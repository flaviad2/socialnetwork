În directorul socialnetwork/images se află câteva imagini cu GUI-ul aplicației. 

În acest proiect, am implementat simplist o rețea de socializare care permite Sign up în rețea, cu introducerea și validarea datelor unei persoane.
Ulterior, utilizatorul creat se poate loga folosind email și parola (parola a fost hash-uită folosind blowfish). 
Rețeaua de socializare permite trimitere de cereri de prietenie, anularea de cereri trimise, acceptarea sau respingerea cererilor primite, respectiv ștergerea unui utilizator din lista actuală de prieteni. 
De asemenea, aplicația permite trimiterea de mesaje către un singur utilizator sau către mai mulți (group chat). 
Utilizatorul logat este notificat la primirea unui mesaj sau a unei cereri de prietenie.
Un mesaj se poate trimite ca mesaj independent sau ca reply la un mesaj primit (atât în conversația între două persoane, cât și în group chat). 
Utilizatorul logat poate vedea ultimele lui conversații ordonate cronologic într-o listă.
El poate alege să continuie o conversație mai veche sau să înceapă una nouă, selectând destinatarul/destinatarii căruia îi adresează mesajele.
Aplicația permite, de asemenea, creare de evenimente și opțiunea de a da follow sau unfollow unui eveniment care urmează să aibă loc. 
Cu 7 zile înaintea unui eveniment, utilizatorul este notificat cu privire la desfășurarea evenimentului în zilele ulterioare. Înainte de a da follow/unfollow unui eveniment, utilizatorul poate vedea detalii despre acest eveniment (nume, descriere, participanți, data, ora).
După ce evenimentul are loc, notificarea dispare.
Aplicația mai permite și crearea unor rapoarte cu privire la prieteniile create/ mesajele primite într-un anumit interval calendaristic, cu exportarea acestui raport într-un PDF care se poate salva pe calculatorul utilizatorului. 
PDF-ul a fost formatat folosind iText PDF.
Persistența aplicației a fost realizată folosind o bază de date și serverul Postgres, respectiv fișiere txt, iar GUI-ul a fost realizat folosind JavaFX si SceneBuilder.