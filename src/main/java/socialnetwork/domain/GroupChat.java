package socialnetwork.domain;

import java.util.List;
import java.util.Objects;

public class GroupChat extends Entity<Long>{

    //id-ul chat-ului
    private List<Utilizator> utilizatori;
    private List<Message> mesaje;

    public GroupChat(List<Utilizator> utilizatori, List<Message> mesaje)
    {
        this.utilizatori=utilizatori;
        this.mesaje=mesaje;
    }


    public List<Utilizator> getUtilizatori() {
        return utilizatori;
    }

    public List<Message> getMesaje() {
        return mesaje;
    }

    public void setUtilizatori(List<Utilizator> utilizatori) {
        this.utilizatori = utilizatori;
    }

    public void setMesaje(List<Message> mesaje) {
        this.mesaje = mesaje;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupChat groupChat = (GroupChat) o;
        return Objects.equals(utilizatori, groupChat.utilizatori) && Objects.equals(mesaje, groupChat.mesaje);
    }

    @Override
    public int hashCode() {
        return Objects.hash(utilizatori, mesaje);
    }

    @Override
    public String toString() {
        return "GroupChat users: " +
                 utilizatori ;
               // ", mesaje=" + mesaje +
               // '}';
    }
}
