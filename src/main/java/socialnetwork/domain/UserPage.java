package socialnetwork.domain;

import socialnetwork.service.*;
import socialnetwork.utils.events.*;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.awt.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class UserPage  implements Observable{

    private UtilizatorService userService;
    private FriendRequestService friendRequestService;
    private PrietenieService friendshipService;
    private MessageService messageService;
    private EventService eventService;
    private Utilizator user;

    List<Observer> observers=new ArrayList<>();

    /**
     * Un obiect de tip UserPage se va crea folosind referinte la toate service-urile si la utilizatorul curent
     * Obiectul de tip UserPage retine informatii importante despre Utilizatorul curent (logat in thread-ul respectiv)
     * a.i. nu mai este nevoie sa se faca apel la service din controller, ci se poate tine in controller doar
     * referinta la UserPage-ul curent
     * @param userService
     * @param friendshipService
     * @param friendRequestService
     * @param messageService
     * @param eventService
     * @param user
     */
    public UserPage(UtilizatorService userService, PrietenieService friendshipService,FriendRequestService friendRequestService, MessageService messageService,EventService eventService,Utilizator user){
        this.userService=userService;
        this.friendshipService=friendshipService;
        this.friendRequestService=friendRequestService;
        this.messageService=messageService;
        this.eventService=eventService;
        this.user=user;

    }


    /**
     * Metoda care returneaza utilizatorul curent logat.
     */
    public Utilizator getUser(){
        return user;
    }

    /**
     * Metoda care returneaza utilizatorul avand Id-ul dat, daca acesta exista
     * @param userID
     * @return
     */
    public Optional<Utilizator> findUser(long userID){
        return userService.getUser(userID);
    }


    /**
     * Metoda care returneaza prietenii unui utilizator cu id dat
     * Paginat (folosit pt afisare in tabel)
     * @param lastID
     * @return
     */
    public List<UtilizatorPrietenieDTO> getUserFriendsPaged(long lastID){
        return userService.getNextFriends(user.getId(),lastID);
    }

    public List<UtilizatorPrietenieDTO> getUserFriendsPagedFilteres(long lastID, String name){
        return userService.getNextFriendsFiltered(user.getId(),lastID, name);
    }



    /**
     * Metoda care returneaza prietenii unui utilizator cu id dat
     * @return
     */
    public List<UtilizatorPrietenieDTO> getUserFriends(){
        return userService.getFriends(user.getId());
    }


    public List<MessageDTO> getUserGroups()
    {


        return StreamSupport.stream(messageService.getAllMessages().spliterator(),false).
                map(x->{ return new MessageDTO(x.getId(), "mesaj tabel");} )
                .collect(Collectors.toList());

    }

    /**
     * Metoda care returneaza utilizatorii cu care s-a imprietenit un user intr-o perioada data
     * @param startDate
     * @param endDate
     * @return
     */
    public List<UtilizatorPrietenieDTO> getUserFriendBetweenDates(LocalDate startDate,LocalDate endDate){
        return userService.getFriendsBetween(user.getId(),startDate,endDate);
    }


    /**
     * Returneaza toate cererile de prietenie PRIMITE de un utilizator care
     * sunt inca in status PENDIND
     * Metoda folosita pt afisarea in tabel a cererilor de prietenie la care
     * mai are de raspuns un user
     * @return
     */
    //primite
    public List<FriendRequest> getFriendsRequest()
    {

        return friendRequestService.getFriendRequests(user.getId());
    }

    //trimise
    public List<FriendRequest> getSentFriendRequest()
    {
        return friendRequestService.getSentFriendRequests(user.getId());
    }

    /**
     * Metoda care returneaza utilizatorii cu care un user nu este inca prieten
     * Metoda folosita cand cautam un utilizator pt a-i trimite cerere de prietenie
     * @param filterString
     * @param lastID
     * @return
     */
    public List<Utilizator> getUserNonFriendsPaged(String filterString,long lastID){
        return userService.getNextNonFriendsFiltered(user.getId(),filterString,lastID);
    }

    public List<Event> getEventsUnfollowedPagedFiltered(String filterString,long lastID){
        System.out.println("da  event1!");

        return eventService.getNextUnfollowedFiltered(user.getId(),filterString,lastID);
    }


    public List<Event> getEventsFollowedPagedFiltered(String filterString,long lastID){
        System.out.println("da  event1!");

        return eventService.getNextFollowedFiltered(user.getId(),filterString,lastID);
    }



    /**
     * Metoda care returneaza cereri de prietenie la care un utilizator mai are de raspuns
     * Paged
     * Folosit pt afisare in tabel, ca sa nu afisam toate inregistrarile din DB
     * @param lastID
     * @return
     */
    public List<Utilizator> getReceivedRequestsPaged(long lastID){
        return userService.getNextUsersRequestReceived(user.getId(),lastID);
    }

    /**
     * Metoda care returneaza cererile de prietenie pe care un utilizator le-a trimis
     * Paged
     * Folosit pt afisare in tabel, ca sa nu afisam toate inregistrarile
     * @param lastID
     * @return
     */
    public List<Utilizator> getSentRequestsPaged(long lastID){
        return userService.getNextUsersRequestSent(user.getId(),lastID);
    }


    /**
     * Metoda care returneaza ultimul mesaj trimis intre doi utilizatori
     * @param otherID
     * @return
     */
    public Message getLastMessageConversation(long otherID){
        return messageService.getLastMessage(user.getId(),otherID);
    }


    /**
     * Metoda care returneaza toate mesajele dintre doi utilizatori dintr-un interval calendaristic
     * @param otherID
     * @param startDate
     * @param endDate
     * @return
     */
    public List<Message> getConversationBetweenDates(long otherID,LocalDate startDate,LocalDate endDate){
        return messageService.getConversationBetweenDates(user.getId(),otherID,startDate,endDate);
    }


    /**
     * Metoda care returneaza toate mesajele dintr-o conversatie, ordonate dupa data
     * @param otherID
     * @param lastID
     * @return
     */
    public List<Message> getConversationPaged(long otherID,long lastID){
        return messageService.getNextMessages(user.getId(),otherID,lastID);
    }

    public List<Message> getGroupConversationPaged(List<Long> ids,long lastID, Long firstMessage){
        return messageService.getNextGroupMessages(ids,lastID,  firstMessage);
    }


    /**
     * Metoda care returneaza evenimentele la care un utilizator a dat unfollow
     * Paged
     * Folosit pt afisare in tabel ca sa nu afisam toate inregistrarile
     * @param lastID
     * @return
     */
    public List<Event> getUnfollowedEventsPaged(long lastID){
        return eventService.getNextUnfollowed(user.getId(),lastID);
    }

    public List<EventDTO> getUnfollowedEventsPagedDTO(long lastID)
    {
        List<EventDTO> list_events=new ArrayList<>();
        for(Event ev: getUnfollowedEventsPaged(lastID))
        {
            EventDTO eventDTO=new EventDTO(ev.getId(), ev.getOwner(), ev.getStartDate(), ev.getEndDate(), ev.getName(), ev.getDescription(), new Button(), new Button());
            list_events.add(eventDTO);
        }
        return list_events;

    }
    /**
     *Metoda care returneaza evenimentele la care userul logat a dat follow
     * Paged
     * Folosit pr afisare in tabel
     * @param lastID id-ul ultimului eveniment afisat
     * @return
     */
    public List<Event> getFollowedEventsPaged(long lastID){
        return eventService.getNextFollowed(user.getId(),lastID);
    }

    public List<Event> getFollowedEventsPaged(String filter, long lastID){
        return eventService.getNextFollowed(user.getId(),lastID);
    }

    /**
     * Metoda care returneaza toate evenimentele la care userul logat a dat follow
     * Se va executa mai incet pt ca trebuie sa aduca toate datele
     * @return
     */
    public List<Event> getFollowedEvents(){
        return eventService.getAllFollowedEvents(user.getId());
    }

    /**
     * Metoda care returneaza toti participantii la un anumit eveniment
     * @param eventID
     * @return
     */
    public List<Utilizator> getEventParticipants(long eventID){
        return eventService.getAllParticipantsFromEvent(eventID);
    }
    //////////////////////////////////////////////////////////////////////////////////////////


    //PAGE SIZE GETTERS & SETTERS//////////////////////////////////////////////////////

    //USER FRIENDS
    public void setPageFriendsSize(int newSizeFriends){
        userService.setPageFriendsSize(newSizeFriends);
    }
    public int getPageFriendsSize() {
        return userService.getPageFriendsSize();
    }

    //USER NON FRIENDS
    public void setPageNonFriendsSize(int newSizeNonFriends){
        userService.setPageNonFriendsSize(newSizeNonFriends);
    }
    public int getPageNonFriendsSize(){
        return userService.getPageNonFriendsSize();
    }

    //USER RECEIVED REQUESTS
    public void setPageUsersRequestReceived(int newSizeUsersRequestReceived){
        userService.setPageUsersRequestReceived(newSizeUsersRequestReceived);
    }
    public int getPageUsersRequestReceivedSize(){
        return userService.getPageUsersRequestReceivedSize();
    }

    //USER SENT REQUESTS
    public void setPageUsersRequestSent(int newSizeUsersRequestSent){
        userService.setPageUsersRequestSent(newSizeUsersRequestSent);
    }
    public int getPageUsersRequestSentSize(){
        return userService.getPageUsersRequestSentSize();
    }

    //USER CONVERSATION
    public void setPageMessages(int newSizeMessages){
        messageService.setPageMessages(newSizeMessages);
    }

    //UNFOLLOWED EVENTS
    public void setPageUnfollowed(int newSizeUnfollowed){
        eventService.setPageUnfollowed(newSizeUnfollowed);
    }

    public int getPageUnfollowed(){
        return eventService.getPageUnfollowed();
    }

    //FOLLOWED EVENTS
    public void setPageFollowed(int newSizeFollowed){
        eventService.setPageFollowed(newSizeFollowed);
    }

    public int getPageFollowed(){
        return eventService.getPageFollowed();
    }
    //////////////////////////////////////////////////////////////////////////////////////////////


    //USEFUL FRIENDSHIP WRITERS METHODS///////////////////////////////////////////////////////////


    /**
     * Metoda care trimite o cerere de prietenie prin apelarea functiei
     * de adaugare cerere prietenie in service
     * Metoda folosita pt add friend
     * @param otherID
     */
    public void sendFriendRequest(long otherID){

        FriendRequest fr = new FriendRequest(findUser(user.getId()).get(), findUser(otherID).get(), LocalDate.now().getYear(), LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth());
        friendRequestService.addFriendRequest(fr);

    }

    /**
     * Metoda care sterge un prieten al utilizatorului logat
     * Metoda folosita pt a sterge un prieten din tabelul cu prieteni ai userului curent
     * @param otherID
     */
    public void removeFriend(long otherID){
        friendshipService.removePrieten(user.getId(),otherID);
    }


    /**
     * Metoda care sterge o cerere de prietenie prin apelarea functieri de stergere cerere
     * din service-ul de cereri de prietenie
     * Metodo folosita pentru retragerea unei cereri de prietenie
     * @param otherID
     */
    public void removeRequest(long otherID)
    {
        Tuple<Long, Long> prietenieId= new Tuple<>(user.getId(), otherID);
        friendRequestService.deleteFriendRequest(prietenieId);
    }

    /**
     * Metoda folosita pentru a raspunde unei cereri de prietenie
     * schimband statusul in APPROVED
     * Dupa asta, cei doi useri vor fi prieteni si cererea nu va mai aparea in tabel
     * @param otherID
     */
    public void acceptRequest(long otherID){
        Tuple<Long, Long> prietenieId= new Tuple<>(otherID, user.getId());
        FriendRequest cerere= friendRequestService.getFriendRequest(prietenieId);
        friendRequestService.respond(cerere, Status.APPROVED.name().toString());
    }

    /**
     * Metoda folosita pentru a raspunde unei cereri de prietenie
     * schimband statusul in REJECTED
     * Dupa asta, cererea se poate trimite iar
     * @param otherID
     */
    public void rejectRequest(long otherID)
    {
        Tuple<Long, Long> prietenieId= new Tuple<>(otherID, user.getId());
        FriendRequest cerere= friendRequestService.getFriendRequest(prietenieId);
        friendRequestService.respond(cerere, Status.REJECTED.name().toString());
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////


    //USEFUL MESSAGE WRITERS METHODS//////////////////////////////////////////////////////////////
    public void sendMessage(Message message){
        messageService.addMessage(message);
    }


    /**
     * Metoda care returneaza un obiect de tip mesaj avand id-ul dat
     * @param idM
     * @return
     */

    public Message findMessage(Long idM)
    {
        return messageService.getMessage(idM);
    }

    /**
     * Metoda prin care se raspunde unui mesaj dintr-un chat intre 2 pers
     * @param idM
     * @param email
     * @param text
     */
    public void replyMessage(Long idM, String email, String text){

        messageService.replyToMessage(idM, email, text);
    }

    /**
     * toate mesajele care au fost primite sau trimise de acest user
     * @return
     */
    public List<Message> findAllMessagesForUser()
    {
        return messageService.findAllMessagesForUser(getUser().getId());
    }

    public Message findMessageForThisUser(long idM)
    {
        for(Message m: findAllMessagesForUser())
            if(m.getId()==idM)
                return m;
            return null;
    }
    /**
     * Metoda prin care se raspunde unui mesaj dintr-un chat cu mai multe persoane
     *
     * @param idM
     * @param email
     * @param text
     */
    public void replyToAllMessage(Long idM, String email, String text){
        messageService.replyToAll(idM, email, text);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////


    //USEFUL EVENT WRITERS METHODS////////////////////////////////////////////////////////////////

    /**
     * Metoda care creeaza un eveniment (aici se fac si validari)
     * @param event
     */
    public void createEvent(Event event){
        eventService.createEvent(event);
    }

    /**
     * Metoda prin care se sterge un eveniment cu id dat
     * @param eventID
     */
    public void removeEvent(Long eventID){
        eventService.removeEvent(eventID);
    }


    /**
     * Metoda prin care se da follow unui eveniment cu id dat
     * @param eventID
     */
    public void followEvent(Long eventID){
        eventService.followEvent(user.getId(),eventID);
    }

    /**
     * Metoda prin care se da unfollow unui eveniment cu id dat
     * @param eventID
     */
    public void unfollowEvent(Long eventID){
        eventService.unfollowEvent(user.getId(),eventID);
    }


    //////////////////////////////////////////////////////////////////////////////////////////////

    //OBSERVERS METHODS////////////////////////////////////////////////////////////////////////////

    /**
     * Metoda prin care se adauga un observer la service-ul de prietenii care
     * este Observable
     * @param friendshipEvent
     */
    public void addFriendshipObserver(Observer<FriendshipsChangeEvent> friendshipEvent) {
        friendshipService.addObserver(friendshipEvent);
    }

    /**
     * Metoda prin care se adauga un observer la service-ul de cereri de prietenie
     * care este Observable
     * @param friendRequestsEvent
     */
    public void addFriendRequestObserver(Observer<FriendRequestsEvent> friendRequestsEvent)
    {
        friendRequestService.addObserver(friendRequestsEvent);
    }

    /**
     * Metoda prin care se adauga un observer la service-ul de cereri de prietenie
     * @param messageEvent
     */
    public void addMessageObserver(Observer<MessageSentEvent> messageEvent){
        messageService.addObserver(messageEvent);
    }

    /**
     * Metoda prin care se adauga un observer la service-ul de evenimente
     * @param eventEvent
     */
    public void addEventObserver(Observer<EventChangeEvent> eventEvent){
        eventService.addObserver(eventEvent);
    }

    @Override
    /**
     * Metoda prin care se adauga un observer la obiectul curent
     * de tip Observable
     */
    public void addObserver(Observer e) {
        observers.add(e);
    }

    @Override
    /**
     * Metoda prin care se sterge un observer la obiectul curent
     * de tip Observable
     */
    public void removeObserver(Observer e) {
        observers.remove(e);
    }

    @Override
    /**
     * Metoda prin care se da notify tuturor observerilor inscrisi
     * pt a observa obiectul curent de tip UserPage
     */
    public void notifyObservers(EventObserver t) {
        observers.forEach(x->{x.update(t);});
    }



    ///////////////////////////////////////////////////////////////////////////////////////////////



}
