package socialnetwork.domain;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import socialnetwork.utils.Constants;

import java.awt.*;
import java.time.LocalDate;

public class UtilizatorPrietenieDTO extends Entity<Long> {
    private String email;
    private String firstName;
    private String lastName;
    private LocalDate date;
    private String check;



    public UtilizatorPrietenieDTO(String email,String firstName,String lastName,LocalDate date){
        this.email=email;
        this.firstName=firstName;
        this.lastName=lastName;
        this.date=date;
        this.check="v";
    }

    public String getEmail(){
        return email;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public String getCheck(){return check; }

    public LocalDate getDate(){
        return date;
    }

    @Override
    public String toString(){
        return firstName+" "+lastName+"\n         Friendship created on: "+ date.format(Constants.DATE_TIME_FORMATTER_DAY);
    }

}
