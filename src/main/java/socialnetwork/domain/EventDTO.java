package socialnetwork.domain;

import javafx.fxml.FXML;
import socialnetwork.utils.Constants;

import java.awt.*;
import java.time.LocalDateTime;

public class EventDTO  {
    private Utilizator owner;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String name;
    private String description;
    private Long id;

    @FXML
    private Button button1;

    @FXML
    private Button button2;

    public EventDTO(Long id, Utilizator owner, LocalDateTime startDate, LocalDateTime endDate, String name, String description, Button but1, Button but2){
        this.owner=owner;
        this.id=id;
        this.startDate=startDate;
        this.endDate=endDate;
        this.name = name;
        this.description=description;
        this.button1=but1;
        this.button2=but2;
    }

    public Utilizator getOwner(){
        return owner;
    }

    public Long getId()
    {
        return id;
    }

    public void setOwner(Utilizator owner) {
        this.owner = owner;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description){
        this.description=description;
    }

    @Override
    public String toString(){
        return "\tEvent: "+ name +"\n\tStarting on "+startDate.format(Constants.DATE_TIME_FORMATTER_HM)+"\n\tOwner: "+owner.getFirstName()+" "+owner.getLastName()+"\n\n";
    }

    public String toStringDAY_OF_WEEK(){
        return "\tEvent: "+ name +"\n\tStarting on "+startDate.getDayOfWeek()+" "+startDate.getMonth()+" "+ startDate.getYear()+"\n\tOwner: "+owner.getFirstName()+" "+owner.getLastName()+"\n\n";
    }

    public Button getButton1() {
        return button1;
    }

    public Button getButton2() {
        return button2;
    }
}
