package socialnetwork.ui;

import socialnetwork.domain.*;
import socialnetwork.comunity.*;
import socialnetwork.domain.exceptions.PrietenieValidationException;
import socialnetwork.domain.exceptions.ServiceException;
import socialnetwork.domain.exceptions.UserValidationException;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.service.FriendRequestService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;


public class UI {
    UtilizatorService serv;
    PrietenieService serv_pr;
    MessageService servM;
    FriendRequestService serv_fr;

    private static Long idU;
    private static Long idM;

    public UI(UtilizatorService serv,PrietenieService serv_pr, MessageService servM, FriendRequestService serv_fr) {
        this.serv=serv;
        this.serv_pr=serv_pr;
        this.servM=  servM;
        this.serv_fr=serv_fr;
        idU=serv.getLastID();
    }
    static void afisareUtilizatori(UtilizatorService serv){
        for(Utilizator u:serv.getAll()){
            System.out.println(u.getId()+" "+u.getFirstName()+" "+u.getLastName());
        }
        System.out.println();
    }
    static void afisarePrieteniiAll(PrietenieService servP, UtilizatorService servU){
        for(Prietenie p:servP.getAll()){
            System.out.println("(ID:"+servU.getUser(p.getId().getLeft()).get().getId()+" & "+
                    servU.getUser(p.getId().getRight()).get().getId()+ ") "+
                    servU.getUser(p.getId().getLeft()).get().getFirstName()+" "+
                    servU.getUser(p.getId().getLeft()).get().getLastName()+" si "+
                    servU.getUser(p.getId().getRight()).get().getFirstName()+" "+
                    servU.getUser(p.getId().getRight()).get().getLastName()+" sunt prieteni"
                    +" din "+p.getDate()) ;
        }
        System.out.println();
    }
     /* static void afisarePrieteni(Scanner myObj, PrietenieService servP, UtilizatorService servU) {
        System.out.print("ID Utilizator: ");
        Long id=Long.parseLong(myObj.nextLine());
        if(servU.getUser(id)==null){
            System.out.println("Acest utilizator nu exista!");
        }
        else{
            for (UserFriendshipDTO p : servP.userFriendships(id).get()) {

                System.out.println(p);
            }
            System.out.println();}
    }

    static void afiseazaMesajePentruUtilizator(Scanner myObj, MessageService servM, UtilizatorService servU){
        System.out.print("ID Utilizator: ");
        Long id=Long.parseLong(myObj.nextLine());
        if(servU.getUser(id)==null){
            System.out.println("Acest utilizator nu exista!");
        }
        else{
            Iterable<Message> messages = servM.get(id);
            if (messages.equals(0)){
                System.out.println("Acest utilizator nu are mesaje!");
            }
            for (Message m : messages){
                System.out.println(m);
            }
        }
    }*/

    static void afiseazaToateMesajele(Scanner myObj, MessageService servM){
        Iterable<Message> messages = servM.getAllMessages();
        if (messages.equals(0)) {
            System.out.println("Nu exista niciun mesaj!");
        }
        else {
            for (Message m : messages){
                System.out.println(m);
            }
        }
    }

    static void addMessageUI(Scanner myObj, MessageService servM, UtilizatorService servU) {

        System.out.print("FROM utilizator : ");
        Long fromId = Long.parseLong(myObj.nextLine());
        if (servU.getUser(fromId).get() == null){
            System.out.println("Nu exista acest utilizator!");
        }

        System.out.print("TO: ");
        List<Utilizator> to = new ArrayList<>();
        Long toId = Long.parseLong(myObj.nextLine());
        while (toId != 0) {
            if (servU.getUser(toId).get() == null) {
                System.out.println("Acest destinatar nu exista!");
                break;
            }
            to.add(servU.getUser(toId).get());
            toId = Long.parseLong(myObj.nextLine());
        }

        LocalDateTime date = LocalDateTime.now();
        System.out.println("Textul mesajului de trimis: ");
        String text = myObj.nextLine();

        System.out.println("Raspunde la mesajul (0 daca mesajul nu e reply): ");
        Long originalMessage = Long.parseLong(myObj.next());
        Message message1 = new Message(servU.getUser(fromId).get(), date, to, text);
        message1.setOriginalMessage(originalMessage);
        servM.addMessage(message1);
        System.out.println("Mesajul a fost adaugat cu succes!");

    }

    static void replyMessageUI(Scanner myObj, MessageService servM, UtilizatorService serv){

        System.out.println("Log-in cu email-ul dumneavoastra: ");
        String email = String.valueOf(myObj.nextLine());

        if(serv.getUtilizatorByEmail(email)!=null) {
            System.out.println("ID MESAJ INITIAL la care faceti reply: ");
            Long idMesaj = Long.parseLong(myObj.nextLine());
            if (servM.getMessage(idMesaj) != null) {
                System.out.println("Introduceti reply-ul: ");
                String text = myObj.nextLine();
                servM.replyToMessage(idMesaj, email, text);
            } else System.out.println("Nu exista mesajul la care incercati sa faceti reply!!");
        }
        else System.out.println("Utilizator inexistent!");


    }

    /*
    static void getConversationsUI(Scanner myObj, MessageService servM, UtilizatorService serv) {
        System.out.println("ID user1: ");
        Long id1 = Long.parseLong(myObj.nextLine());
        System.out.println("ID user2: ");
        Long id2 = Long.parseLong(myObj.nextLine());
        Iterable<Message> list = servM.getConversations(id1, id2);
        for (Message m :list){
            System.out.print(m.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + " "+m.getFrom().getFirstName() + " " + m.getFrom().getLastName() + ": ");
            System.out.println(m.getMessage());
        }
    }*/

    static void addConversationUI(Scanner myObj, MessageService servM, UtilizatorService serv){
        System.out.println("Adaugati o conversatie: ");
        System.out.println("User 1: ");
        Long id1 = Long.parseLong(myObj.next());
        System.out.println("User2: ");
        Long id2 = Long.parseLong(myObj.next());
        Utilizator user1 = serv.getUser(id1).get();
        Utilizator user2 = serv.getUser(id2).get();
        System.out.println(user1.getFirstName() + " " + user1.getLastName() + ": ");
        String text1 = myObj.nextLine();
        List<Utilizator> toUser1 = new ArrayList<>();
        toUser1.add(user1);
        List<Utilizator> toUser2 = new ArrayList<>();
        toUser2.add(user2);
        Message messageFromUser1 = new Message(user1, LocalDateTime.now(), toUser2, text1);
        messageFromUser1.setOriginalMessage(0L);
        servM.addMessage(messageFromUser1);
        Message messageFromUser2 = null;
        String text2;
        do {
            System.out.println(user2.getFirstName() + " " + user2.getLastName() + ": ");
            text2 = myObj.nextLine();
            messageFromUser2 = new Message(user2, LocalDateTime.now(), toUser1, text2);
            //messageFromUser2.setOriginalMessage(messageFromUser1.getId());
            messageFromUser2.setOriginalMessage(id1);
            servM.addMessage(messageFromUser2);
            System.out.println(user1.getFirstName() + " " + user1.getLastName() + ": ");
            text1 = myObj.nextLine();
            messageFromUser1 = new Message(user1, LocalDateTime.now(), toUser2, text1);
            //messageFromUser1.setOriginalMessage(messageFromUser2.getId());
            messageFromUser1.setOriginalMessage(id2);
            servM.addMessage(messageFromUser1);
        }
        while (!text1.equals("bye"));

        System.out.println("END OF CONVERSATION");


    }

    static void optiuni(){
        System.out.println("1-------->ADD utilizator");
        System.out.println("2-------->DELETE utilizator");
        System.out.println("3-------->PRINT toti utilizatorii");

        System.out.println("4-------->ADD prietenie");
        System.out.println("5-------->REMOVE prietenie");
        System.out.println("6-------->PRINT toate prieteniile");
        System.out.println("7-------->PRINT prietenii unui utilizator (LAMBDA)");


        System.out.println("8-------->Numar de comunitati");
        System.out.println("9-------->Cea mai sociabila comunitate");

        System.out.println("10------->UPDATE utilizator");
        System.out.println("11------->UPDATE o prietenie");
        System.out.println("12------->CAUTA un utilizator");
        System.out.println("13------->CAUTA o prietenie");
        System.out.println("14------->PRINT prieteniile unui utilizator dintr-o luna data (LAMBDA)");

        System.out.println("15------->PRINT toate mesajele pentru Utilizator");
        System.out.println("16------->PRINT toate mesajele");
        System.out.println("17------->SEND mesaj");
        System.out.println("18------->REPLY mesaj");
        System.out.println("19------->PRINT conversatie a doi Useri");
        System.out.println("20------->ADD conversatie");

        System.out.println("21------->Trimite o cerere de prietenie");
        System.out.println("22------->Raspunde la o cerere de prietenie");




        System.out.println("0.Exit");
    }
    /*
    static void addUtilizatorUI(Scanner myObj,UtilizatorService serv){

        try {
            System.out.print("Nume: ");
            String nume = myObj.nextLine();
            System.out.print("Prenume: ");
            String prenume = myObj.nextLine();
            Utilizator u1 = new Utilizator(nume, prenume);
            idU++;
            u1.setId(idU);
            serv.addUtilizator(u1);
            System.out.println("Utilizator adaugat \n");
        }catch(ServiceException e) {System.out.println(e.getMessage()+"\n");}
    }*/


    static void addPrietenieUI(Scanner myObj,PrietenieService serv_pr){
        try {
            System.out.print("ID1: ");Long id1=myObj.nextLong();
            System.out.print("ID2: ");Long id2=myObj.nextLong();
            System.out.print("Anul: ");int an=myObj.nextInt();
            System.out.print("Luna: ");int luna=myObj.nextInt();
            System.out.print("Ziua: ");int zi=myObj.nextInt();
            serv_pr.addPrieten(id1, id2,an,luna,zi);
            System.out.println("Prietenie adaugata \n");
        }catch(ServiceException e) {System.out.println(e.getMessage() +"\n");}


    }
    static void removeUserUI(Scanner myObj,UtilizatorService serv,Repository<Tuple<Long, Long>, Prietenie> repoP){
        System.out.print("ID: ");
        Long id = myObj.nextLong();
        try{
            serv.removeUtilizator(id);
            System.out.println("Utilizator sters \n");}
        catch(ServiceException e){
            System.out.println( e.getMessage());
        }

    }

    static void updateUserUI(Scanner myObj,UtilizatorService serv){
        System.out.print("ID: ");
        Long id = myObj.nextLong();
        myObj.nextLine();


        Utilizator user = serv.getUser(id).get();
        if(user!=null) {
            System.out.println("Se poate face modificarea...");

            System.out.print("Nume (nou sau vechi) : ");
            String nume = myObj.nextLine();
            System.out.print("Prenume (nou sau vechi) : ");
            String prenume = myObj.nextLine();
            user.setFirstName(nume);
            user.setLastName(prenume);

            try {
                serv.getRepo().update(user);
                System.out.println("Utilizator modificat \n");
            } catch (UserValidationException e) {
                System.out.println(e.getMessage());
            }
        }
        else System.out.println("Nu se poate face modifc pt ca utilizatorul nu exista!");


    }


    static void updatePrietenieUI(Scanner myObj, PrietenieService servP){
        System.out.print("ID1: ");
        Long id1 = myObj.nextLong();
        myObj.nextLine();

        System.out.print("ID2: ");
        Long id2 = myObj.nextLong();
        myObj.nextLine();

        Prietenie p= servP.getPrietenie(id1, id2);
        if(p!=null)
        {
            System.out.println("Se poate face modificarea...");

            System.out.print("Noua data: ");
            String date=myObj.nextLine();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

            LocalDate localDate = LocalDate.parse(date, formatter);

            Prietenie pr=servP.getPrietenie(id1, id2);
            pr.setDate(localDate);

            try {
                servP.getRepo().update(pr);
                System.out.println("Prietenie modificata \n");
            } catch (PrietenieValidationException e) {
                System.out.println(e.getMessage());
            }
        }
        else System.out.println("Nu se poate face modifc pt ca utilizatorul nu exista!");


    }
    static void removePrietenieUI(Scanner myObj,PrietenieService serv_pr) {

        System.out.print("ID1: ");
        Long id1 = myObj.nextLong();
        System.out.print("ID2: ");
        Long id2 = myObj.nextLong();
        try {
            serv_pr.removePrieten(id1, id2);
            System.out.println("Relatie de prietenie stearsa \n");
        }
        catch(ServiceException e){
            System.out.println(e.getMessage());
        }
    }

    static void afisarePrieteniLambdaLunaData(Scanner myObj, PrietenieService servP, UtilizatorService servU){
        System.out.print("ID Utilizator: ");
        Long id=Long.parseLong(myObj.nextLine());
        System.out.println("Luna in care s-a legat prietenia: ");
        try {
            if(servU.getUser(id)==null){
                System.out.println("Acest utilizator nu exista!");
            }
            else{
                Month month = Month.of(Integer.parseInt(myObj.nextLine()));
                List<UserFriendshipDTO> list = servP.userFriendshipsMonth(id, month);
                for (UserFriendshipDTO userFriendshipDTO : list) {
                    System.out.println(userFriendshipDTO);
                }
                System.out.println();
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }

    }


    public void sendFriendRequest(Scanner myObj, FriendRequestService serv, UtilizatorService servU, PrietenieService serv_pr){
        System.out.print("Log-in cu ID-ul dumneavoastra: ");
        Long id1 = myObj.nextLong();
        System.out.print("ID-ul utilizatorului cu care vreti sa va imprieteniti: ");
        Long id2 = myObj.nextLong();
        System.out.print("Anul: ");int an=myObj.nextInt();
        System.out.print("Luna: ");int luna=myObj.nextInt();
        System.out.print("Ziua: ");int zi=myObj.nextInt();
        if(servU.getUser(id1)==null || servU.getUser(id2)==null){
            System.out.println("Acesti utilizatori nu exista!");
        }
        else {
            int ok = 1;
            if (serv_pr.suntPrieteni(id1, id2))
                ok = 0;
            if (ok == 1) {
                FriendRequest fr = new FriendRequest(servU.getUser(id1).get(), servU.getUser(id2).get(),an,luna,zi);
                if(serv.getFriendRequest(fr.getId())!=null&&serv.getFriendRequest(fr.getId()).getStatus().equals(Status.PENDING)){
                    System.out.println("Cererea de prietenie e deja in asteptare!\n");
                }
                else {
                    serv.addFriendRequest(fr);
                    System.out.println("Cerere de prietenie trimisa!");
                }
            } else
                System.out.println("Sunteti deja prieten cu acest utilizator");
        }

    }

    public void respondToFriendRequest(Scanner myObj, FriendRequestService serv,UtilizatorService servU){
        System.out.print("Log-in cu ID-ul dumneavoastra: ");
        Long id1 = myObj.nextLong();
        if(servU.getUser(id1)==null)
            System.out.println("Acest utilizator nu exista!");
        else {

            int nr = 0;
            List<FriendRequest> cererile_acestui_utilizator = new ArrayList<>();
            for (FriendRequest f : serv.getAll()) {
                if (f.getUser2().getId() == id1 && f.getStatus().equals(Status.PENDING))
                    cererile_acestui_utilizator.add(f);

            }
            System.out.println("");
            if (cererile_acestui_utilizator.size() != 0) {
                System.out.println("Cererile dumneavoastra care sunt PENDING: ");
                System.out.println();
                for (FriendRequest f : cererile_acestui_utilizator) {
                    System.out.println(f.getUser1().getId() + " | " + f.getUser1().getFirstName() + " | " +
                            f.getDate() + " | " + f.getStatus().toString());
                }


                System.out.print("ID-ul utilizatorului care v-a trimis cererea de prietenie: ");
                Long id2 = myObj.nextLong();
                if (servU.getUser(id2) == null) {
                    System.out.println("Acest utilizator nu exista!");
                } else {
                    System.out.println("Pentru a accepta cererea, tastati 1");
                    System.out.println("Pentru a refuza cererea, tastati 0");
                    int ok = myObj.nextInt();
                    FriendRequest fr = new FriendRequest(servU.getUser(id1).get(), servU.getUser(id2).get(), LocalDate.now().getYear(), LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth());
                    if (ok == 0) {
                        try {
                            serv.respond(fr, Status.REJECTED.name());
                            serv.deleteFriendRequest(fr.getId());
                            System.out.println("Am respins prietenia!");
                        } catch (ServiceException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                    if (ok == 1) {
                        try {
                            serv.respond(fr, Status.APPROVED.name());
                            System.out.println("Am acceptat prietenia!");
                        } catch (ServiceException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                }
            } else System.out.println("Nu aveti cereri de prietenie PENDING!"); //
        }
    }


    public void start() {

        while(true){
            optiuni();
            Scanner myObj = new Scanner(System.in);
            System.out.print("Introduceti optiunea: ");//spatiu doar intre nr complex si operator
            int opt = Integer.parseInt(myObj.nextLine());
            switch(opt){
                //case 1:
                  //  addUtilizatorUI(myObj,serv);
                    //break;
                case 2:
                    removeUserUI(myObj,serv,serv_pr.getRepo());
                    break;
                case 3:
                    afisareUtilizatori(serv);
                    break;
                case 4:
                    addPrietenieUI(myObj,serv_pr);
                    break;
                case 5:
                    removePrietenieUI(myObj,serv_pr);
                    break;
                case 6:
                    afisarePrieteniiAll(serv_pr,serv);
                    break;
              //  case 7:
                //    afisarePrieteni(myObj,serv_pr,serv);
                  //  break;
                case 8:
                    System.out.println("Numar comunitati: "+serv.NumarComunitati()+"\n");
                    break;
                case 9:
                    System.out.println("Cea mai sociabila comunitate: ");
                    int maxim=serv.ComunitateSociabila()-1;

                    System.out.println("de lungime:"+maxim);
                    if(maxim==0)
                        System.out.println("Nu exista inca prietenii!! \n");

                    break;
                case 10:
                    updateUserUI(myObj,serv);
                    break;

                case 11:
                    updatePrietenieUI(myObj, serv_pr);
                    break;

                case 12:
                    System.out.print("Introduceti id-ul de cautare:");

                    Long id = myObj.nextLong();
                    Utilizator u=serv.getRepo().findOne(id).get();
                    if(u!=null)
                        System.out.println(u.getId()+"  "+u.getFirstName()+"  "+u.getLastName());
                    else
                        System.out.println("Nu s-a gasit acest utilizator!");
                    break;

                case 13:
                    System.out.println("ID1: ");
                    Long id1=myObj.nextLong();
                    System.out.println("ID2: ");
                    Long id2=myObj.nextLong();
                    Tuple pp = new Tuple((long)id1, (long)id2);
                    Prietenie p=serv_pr.getRepo().findOne(pp);
                    if(p!=null)
                        System.out.println("Utilizatorii cu id1= "+ p.getId().getLeft()+" si id2="+p.getId().getRight()+"  sunt prieteni de la data de "+p.getDate().getYear()+"-"+p.getDate().getMonthValue()+"-"+p.getDate().getDayOfMonth());
                    else
                        System.out.println("Nu exista aceasta prietenie!");
                    break;

                case 14:
                    afisarePrieteniLambdaLunaData(myObj, serv_pr, serv);
                    break;
               // case 15:
                 //   afiseazaMesajePentruUtilizator(myObj, servM, serv);
                   // break;
                case 16:
                    afiseazaToateMesajele(myObj, servM);
                    break;
                case 17:
                    addMessageUI(myObj, servM, serv);
                    break;
                case 18:
                    replyMessageUI(myObj, servM, serv);
                    break;
               // case 19:
                 //   getConversationsUI(myObj, servM, serv);
                   // break;
                case 20:
                    addConversationUI(myObj, servM, serv);
                    break;

                case 21:
                    sendFriendRequest(myObj,serv_fr,serv,serv_pr);
                    break;
                case 22:
                    respondToFriendRequest(myObj, serv_fr, serv);
                    break;

                case 0:
                    System.out.println("Aplicatie inchisa...");
                    return;
            }
        }
    }

}

