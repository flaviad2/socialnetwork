package socialnetwork.service;

import socialnetwork.domain.Entity;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.exceptions.MessageValidationException;
import socialnetwork.domain.exceptions.ServiceException;
import socialnetwork.repository.db.MessageDbRepository;
import socialnetwork.repository.db.PrietenieDbRepository;
import socialnetwork.repository.db.UtilizatorDbRepository;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.MessageSentEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MessageService implements Observable<MessageSentEvent> {
    private PrietenieDbRepository repo;
    private UtilizatorDbRepository repoU;
    private MessageDbRepository repoM;
    private UtilizatorService servU;

    List<Observer<MessageSentEvent>> observers=new ArrayList<>();

    /**
     * Constructorul clasei
     * @param repo - Repo-ul de prietenii
     * @param repoU - Repo-ul de utilizatori
     * @param repoM - Repo-ul de mesaje
     */
    public MessageService(PrietenieDbRepository repo, UtilizatorDbRepository repoU, MessageDbRepository repoM, UtilizatorService servU) {
        this.repo = repo;
        this.repoU = repoU;
        this.repoM = repoM;
        this.servU =servU;

    }


    /**
     * Metoda care returneaza toate mesajele
     * @return toate mesajele din repo
     */
    public Iterable<Message> getAllMessages(){
        return repoM.findAll();
    }


    /**
     * Metoda care returneaza mesajele unui user (trimise/primite)
     * @param email - email-ul utilizatorului dat ca si parametru
     * @return toate mesajele pentru user
     */
    public Iterable<Message> getAllMessagesForAUser(String email){
        Iterable<Message> lista = repoM.findAll();
        List<Message> userMessageList = new ArrayList<>();
        for (Message m : lista) {
            Long idUser = servU.getUtilizatorByEmail(email).getId();
            if (m.getFrom().getId() == idUser)
                userMessageList.add(m);
            for(Utilizator mTo : m.getTo()) {
                if(mTo.getId() == idUser) {
                    userMessageList.add(m);
                }
            }
        }
        return userMessageList;
    }


    /**
     * Metoda care adauga un mesaj
     * @param message - mesajul de adaugat
     * @return null
     */
    public Message addMessage(Message message) {
        try{
            Message message1=repoM.save(message);
            if(message1!=null){
                notifyObservers(new MessageSentEvent(ChangeEventType.MESSAGE,message1));
            }
            return message1;
        }
        catch(MessageValidationException e) {throw new ServiceException(e.getMessage());}
    }


    /**
     * Metoda care raspunde unui mesaj existent
     * @param idMessage - id-ul mesajului la care se va face reply
     * @param email - userului care face reply
     * @param text - textul mesajului
     * @return null
     * @throws ServiceException - daca mesajul nu exista
     */
    public Message replyToMessage( Long idMessage, String email, String text){
        Message message = repoM.findOne(idMessage);
        Message replyMessage = null;
        Utilizator u = servU.getUtilizatorByEmail(email);

        if(message == null) {
            throw new ServiceException("Nu exista acest mesaj!");
        }
        else{
            if (!message.getTo().contains(u)){
                throw new ServiceException("You cannot reply to this message! It doesn't belong to you!");
            }
            else {
                
                List<Utilizator> to = new ArrayList<>();
                to.add(message.getFrom()); // cine face reply
                replyMessage = new Message(u, LocalDateTime.now(), to, text);
                replyMessage.setOriginalMessage(idMessage);
                Message message1=repoM.save(replyMessage);

                if(message1!=null){
                    notifyObservers(new MessageSentEvent(ChangeEventType.MESSAGE,message1));
                }
                else return null;
            }
        }
        return null;

    }


    //se adauga un mesaj creat de mine care initializeaza group chat-ul
    public Message sendToAll(Message message){
        try{
            Message message1=repoM.save(message);
            if(message1!=null){
                notifyObservers(new MessageSentEvent(ChangeEventType.MESSAGE,message1));
            }
            return message1;
        }
        catch(MessageValidationException e) {throw new ServiceException(e.getMessage());}

    }

    //toare reply-urile vor fi pt acel mesaj initial care a creat group chat-ul
    public Message replyToAll(Long idMessage, String email, String text){
        Message message = repoM.findOne(idMessage);
        Message replyAll = null;
        Utilizator u = servU.getUtilizatorByEmail(email);

        if(message == null) {
            throw new ServiceException("Nu exista acest mesaj!");
        }
        else{
            if (!message.getTo().contains(u)){
                throw new ServiceException("You cannot reply to this message! It doesn't belong to you!");
            }
            else{
                List<Utilizator> to = new ArrayList<>();
                for (Utilizator user : message.getTo()){
                    if (!user.equals(u)){
                        to.add(user);
                    }
                }
                to.add(message.getFrom());
                replyAll = new Message(u, LocalDateTime.now(), to, text);
                replyAll.setOriginalMessage(idMessage);
                Message message1= repoM.save(replyAll);
                if(message1!=null){
                    notifyObservers(new MessageSentEvent(ChangeEventType.MESSAGE,message1));
                }
                else return null;

            }

        }
        return null;
    }

    /**
     * Metoda care returneaza conversatiile a doi utilizatori
     * @param user1 - username-ul primului user
     * @param user2 - username-ul celui de-al doilea user
     * @return lista de conversatii
     */
    public List<Message> getConversations(String user1, String user2){

        List<Message> messageList=new ArrayList<>();
        Long id1 = servU.getUtilizatorByEmail(user1).getId();
        Long id2 = servU.getUtilizatorByEmail(user2).getId();
        for(Message m:repoM.findAll()){

            if((m.getTo().contains(repoU.findOne(id1)) && m.getFrom().getId()==id2)
                    ||m.getTo().contains(repoU.findOne(id2)) && m.getFrom().getId()==id1)
                messageList.add(m);
        }

        if(messageList.size()!=0)
        {
            List<Message> ListaNoua= messageList.stream()
                    .sorted(Comparator.comparing(Entity::getId)).collect(Collectors.toList());
            return ListaNoua;
        }
        else
            throw new ServiceException("Nu exista conversatii intre acesti utilizatori!");

    }


    /**
     * Metoda care returneaza un mesajul dupa id
     * @param id1 - id-ul mesajului
     * @return mesaj
     */
    public Message getMessage(Long id1){
        return repoM.findOne(id1);
    }

    public Iterable<Message> getConversation(Long userID_A, Long userID_B){
        return repoM.getConvo(userID_A,userID_B);
    }

    public List<Message> getConversationBetweenDates(Long userID_A, Long userID_B, LocalDate date1, LocalDate date2){
        return repoM.getConvoBetweenDates(userID_A,userID_B,date1,date2);
    }


    public Message getOneMessage(long id){
        return repoM.findOne(id);
    }

    public Message getLastMessage(long id1,long id2){
        return repoM.getLastMessage(id1,id2);
    }


    @Override
    public void addObserver(Observer<MessageSentEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<MessageSentEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(MessageSentEvent t) {
        observers.stream().forEach(x->x.update(t));
    }


    private int pageMessages=-1;
    private int sizeMessages=1;
    public void setPageMessages(int newSizeMessages){
        sizeMessages=newSizeMessages;
    }

    public List<Message> getNextMessages(long userA,long userB,long lastID){
        pageMessages++;
        return getMessages(pageMessages,userA,userB,lastID);

    }

    public List<Message> getNextGroupMessages(List<Long> ids, long lastID, Long firstMessage){
        pageMessages++;
        return getGroupMessages(pageMessages,ids,lastID, firstMessage);

    }


  public List<Message> findAllMessagesForUser(long idUser)
  {

      return StreamSupport.stream(repoM.findAllForUser(idUser).spliterator(),false)
              .collect(Collectors.toList());
  }



    public List<Message> getMessages(int page,long userA,long userB,long lastID){
        pageMessages=page;
        Pageable pageable=new PageableImplementation(pageMessages,sizeMessages);
        return repoM.findAllConversationPaged(pageable,userA,userB,lastID).getContent()
                .collect(Collectors.toList());
    }

    public List<Message> getGroupMessages(int page,List<Long>ids,long lastID, Long firstMessage){
        pageMessages=page;
        Pageable pageable=new PageableImplementation(pageMessages,sizeMessages);
        return repoM.findAllGroupConversationPaged(pageable,ids,lastID,firstMessage).getContent()
                .collect(Collectors.toList());
    }


}
