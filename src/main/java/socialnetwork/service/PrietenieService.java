package socialnetwork.service;


import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.UserFriendshipDTO;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.exceptions.ServiceException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.db.FriendRequestsDbRepository;
import socialnetwork.repository.db.PrietenieDbRepository;
import socialnetwork.repository.db.UtilizatorDbRepository;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.FriendshipsChangeEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

//import sun.nio.ch.Util;

public class PrietenieService  implements Observable<FriendshipsChangeEvent>  {
    private PrietenieDbRepository repo;
    private UtilizatorDbRepository repoU;
    private UtilizatorService serv_u;
    private FriendRequestsDbRepository repoFR;
    private List<Observer<FriendshipsChangeEvent>> observers=new ArrayList<>();


    public PrietenieService(PrietenieDbRepository repo, UtilizatorDbRepository repoU, FriendRequestsDbRepository friendRequestsDbRepository) {
        this.repo = repo;
        this.repoU = repoU;
        //load friends(adaug in listele de prieteni )
        if(this.repo.size()!=0)
            for (Prietenie p : this.repo.findAll()) {
                Long id1 = p.getId().getLeft();
                Long id2 = p.getId().getRight();
                if (repoU.findOne(id1) != null && repoU.findOne(id2) != null)
                { this.repoU.findOne(id1).get().addFriend(this.repoU.findOne(id2).get());
                    this.repoU.findOne(id2).get().addFriend(this.repoU.findOne(id1).get());}
            }
        this.repoFR=friendRequestsDbRepository;
        this.serv_u = serv_u;


    }

    public boolean suntPrieteni(Long id1, Long id2) {
        for (Prietenie p : repo.findAll())
            if ((p.getId().getRight() == id1 && p.getId().getLeft() == id2) || (p.getId().getRight() == id2 && p.getId().getLeft() == id1))
                return true;
        return false;
    }

    public Prietenie getPrietenie(Long id1, Long id2)
    {
        for (Prietenie p : repo.findAll())
            if ((p.getId().getRight() == id1 && p.getId().getLeft() == id2) || (p.getId().getRight() == id2 && p.getId().getLeft() == id1))
                return p;
        return null;
    }

    public List<UserFriendshipDTO> userFriendships(String email) {
        Long id = serv_u.getUtilizatorByEmail(email).getId();
        if (repoU.findOne(id) == null) {
            throw new ServiceException("User does not exist!\n");
        }
        List<UserFriendshipDTO> list = StreamSupport.stream(repo.findAll().spliterator(), false)
                .filter(x -> (x.getId().getLeft() == id || x.getId().getRight() == id))
                .map(x -> {
                    Utilizator user1 = repoU.findOne(x.getId().getLeft()).get();
                    Utilizator user2 = repoU.findOne(x.getId().getRight()).get();
                    UserFriendshipDTO userFriendshipDTO;
                    if (user1.getId().compareTo(id) == 0)
                        userFriendshipDTO = new UserFriendshipDTO(user2.getId(), user2.getEmail(), user2.getFirstName(), user2.getLastName(), x.getDate());
                    else
                        userFriendshipDTO = new UserFriendshipDTO(user1.getId(), user1.getEmail(), user1.getFirstName(), user1.getLastName(), x.getDate());
                    return userFriendshipDTO;
                })
                .collect(Collectors.toList());
        return list;
    }

    public List<UserFriendshipDTO> userFriendshipsMonth(Long id, Month month) {
        if (repoU.findOne(id) == null) {
            throw new ServiceException("User does not exist!\n");
        }
        List<UserFriendshipDTO> list = StreamSupport.stream(repo.findAll().spliterator(), false)
                .filter(x -> ((x.getId().getLeft() == id || x.getId().getRight() == id) && x.getDate().getMonth() == month))
                .map(x -> {
                    Utilizator user1 = repoU.findOne(x.getId().getLeft()).get();
                    Utilizator user2 = repoU.findOne(x.getId().getRight()).get();
                    UserFriendshipDTO userFriendshipDTO;
                    if (user1.getId().compareTo(id) == 0)
                        userFriendshipDTO = new UserFriendshipDTO(user2.getId(), user2.getEmail(), user2.getFirstName(), user2.getLastName(), x.getDate());
                    else
                        userFriendshipDTO = new UserFriendshipDTO(user1.getId(),user1.getEmail(), user1.getFirstName(), user1.getLastName(), x.getDate());
                    return userFriendshipDTO;
                })
                .collect(Collectors.toList());
        return list;
    }

    public Repository<Tuple<Long, Long>, Prietenie> getRepo() {
        return repo;
    }

    /**
     * Adaug o noua prietenie
     *
     * @param id1 id-ul utilizatorului1
     * @param id2 id-ul utilizatorului2
     * @return obiectul de tip prietenie adaugat
     */
    public Prietenie addPrieten(Long id1, Long id2, int an, int luna, int zi) {
        if (repoU.findOne(id1) == null || repoU.findOne(id2) == null)
            throw new ServiceException("Utilizatori invalizi!");
        //Tuple<Long, Long> id = new Tuple<>(id1, id2);
        //Tuple<Long, Long> idd = new Tuple<>(id2, id1);

        if(suntPrieteni(id1,id2) || suntPrieteni(id2, id1)){
            throw new ServiceException("Aceasta prietenie exista deja!");
        }

        try {
            Prietenie pr = new Prietenie(id1, id2, an, luna, zi);
            Prietenie p = repo.save(pr);
            notifyObservers(new FriendshipsChangeEvent(ChangeEventType.ACCEPT, p));
            repoU.findOne(id1).get().addFriend(repoU.findOne(id2).get());//
            repoU.findOne(id2).get().addFriend(repoU.findOne(id1).get());//
            return p;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Sterg o prietenie
     *
     * @param id1 id-ul utilizatorului1
     * @param id2 id-ul utilizatorului2
     * @return obiectul de tip prietenie sters
     */
    public Prietenie removePrieten(Long id1, Long id2) {
        if (repoU.findOne(id1) == null || repoU.findOne(id2) == null)
            throw new ServiceException("Utilizatori invalizi!");
        Tuple<Long, Long> id = new Tuple<>(id1, id2);
        Prietenie pr = repo.findOne(id);
        if (pr != null) {
            Prietenie p = repo.delete(pr.getId());
            notifyObservers(new FriendshipsChangeEvent(ChangeEventType.DELETE, p));
            repoU.findOne(p.getId().getLeft()).get().removeFriend(repoU.findOne(p.getId().getRight()).get());
            repoU.findOne(p.getId().getRight()).get().removeFriend(repoU.findOne(p.getId().getLeft()).get());

            if(repoFR.findOne(id)!=null)
            repoFR.delete(id);
            else
            {
                 id = new Tuple<>(id2, id1);
                repoFR.delete(id);


            }
            return p;
        } else {
            throw new ServiceException("Aceasta prietenie nu exista!");
        }
    }

    /**
     * @return toate el de tip Prietenie
     */
    public Iterable<Prietenie> getAll() {
        return repo.findAll();
    }

    public Prietenie createPrietenie(String email1, String email2, int an , int luna, int zi) {
        Long idUser1 = null;
        Long idUser2 = null;
        Prietenie newPrietenie;

        if (serv_u.existUtilizator(email1)) {
            idUser1 = serv_u.getUtilizatorByEmail(email1).getId();
        }
        if (serv_u.existUtilizator(email2)) {
            idUser2 = serv_u.getUtilizatorByEmail(email2).getId();
        }

        newPrietenie = new Prietenie(idUser1, idUser2, an, luna, zi);
        return newPrietenie;
    }

    public Prietenie createPrietenie(String email1, String email2) {
        Long idUser1 = null;
        Long idUser2 = null;
        Prietenie newPrietenie;

        if (serv_u.existUtilizator(email1)) {
            idUser1 = serv_u.getUtilizatorByEmail(email1).getId();
        }
        if (serv_u.existUtilizator(email2)) {
            idUser2 = serv_u.getUtilizatorByEmail(email2).getId();
        }

        newPrietenie = new Prietenie(idUser1, idUser2, LocalDate.now());
        return newPrietenie;
    }

    @Override
    public void addObserver(Observer<FriendshipsChangeEvent> e) {
        observers.add(e);

    }

    @Override
    public void removeObserver(Observer<FriendshipsChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(FriendshipsChangeEvent t) {
        observers.stream().forEach(x->x.update(t));
    }

    /**
     * Metoda care adauga in lista fiecarui utilizator prietenii pe care ii are din lista de prietenii
     */
    public void loadFriends() {
        for (Prietenie prietenie : repo.findAll()) {
            long ID1 = prietenie.getId().getLeft();
            long ID2 = prietenie.getId().getRight();
            Utilizator user1 = repoU.findOne(ID1).get();
            Utilizator user2 = repoU.findOne(ID2).get();
            if (user1!=null && user2!=null) {
                user1.addFriend(user2);
                user2.addFriend(user1);
            }
        }
    }
}




