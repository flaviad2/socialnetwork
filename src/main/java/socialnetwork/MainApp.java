package socialnetwork;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialnetwork.controller.LogInController;
import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.Message;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.db.*;
import socialnetwork.service.*;

import javax.swing.*;
import javax.swing.text.IconView;
import java.awt.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;

public class MainApp extends Application {
    UtilizatorDbRepository userDatabaseRepository;
    PrietenieDbRepository friendshipDatabaseRepository;
    FriendRequestsDbRepository friendRequestsDbRepository;
    MessageDbRepository messageDatabaseRepository;
    EventDbRepository eventDatabaseRepository;

    UtilizatorService userService;
    PrietenieService friendshipService;
    FriendRequestService friendRequestService;
    MessageService messageService;
    EventService eventService;

    private double xOffset = 0;
    private double yOffset = 0;

    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {

        userDatabaseRepository=new UtilizatorDbRepository("users","jdbc:postgresql://localhost:5432/SocialNetwork", "postgres", "postgres", new UtilizatorValidator());
        friendshipDatabaseRepository=new PrietenieDbRepository("jdbc:postgresql://localhost:5432/SocialNetwork", "postgres", "postgres", new PrietenieValidator());
        friendRequestsDbRepository= new FriendRequestsDbRepository("jdbc:postgresql://localhost:5432/SocialNetwork", "postgres", "postgres", new FriendRequestValidator(), userDatabaseRepository);





        messageDatabaseRepository=new MessageDbRepository("jdbc:postgresql://localhost:5432/SocialNetwork", "postgres", "postgres",new MessageValidator(), userDatabaseRepository);

        eventDatabaseRepository=new EventDbRepository("events","jdbc:postgresql://localhost:5432/SocialNetwork", "postgres", "postgres",new EventValidator(),userDatabaseRepository);
        friendRequestService=new FriendRequestService(friendRequestsDbRepository,friendshipDatabaseRepository);
        userService=new UtilizatorService(userDatabaseRepository, friendshipDatabaseRepository,  friendRequestsDbRepository);
        friendshipService=new PrietenieService(friendshipDatabaseRepository,userDatabaseRepository, friendRequestsDbRepository);
        messageService=new MessageService(friendshipDatabaseRepository, userDatabaseRepository, messageDatabaseRepository,userService);
        eventService=new EventService(eventDatabaseRepository);



        initView(primaryStage);
        primaryStage.setResizable(false);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setWidth(800);
        primaryStage.setTitle("Log in:");
        primaryStage.show();
    }

    private void initView(Stage primaryStage) throws IOException {

        FXMLLoader loginLoader = new FXMLLoader();
        loginLoader.setLocation(getClass().getResource("/views/loginView.fxml"));
        AnchorPane loginLayout = loginLoader.load();
        loginLayout.setOnMousePressed(event->{
            xOffset = event.getSceneX();
            yOffset = event.getSceneY();
        });
        loginLayout.setOnMouseDragged(event -> {
            primaryStage.setX(event.getScreenX() - xOffset);
            primaryStage.setY(event.getScreenY() - yOffset);
        });

        primaryStage.setScene(new Scene(loginLayout));

        LogInController loginController = loginLoader.getController();
        
        loginController.setMultiService(userService,friendshipService,messageService, friendRequestService,eventService);

    }
}