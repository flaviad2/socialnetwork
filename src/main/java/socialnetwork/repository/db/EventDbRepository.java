package socialnetwork.repository.db;

import socialnetwork.domain.Event;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.PageImplementation;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.utils.Constants;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;

public class EventDbRepository extends AbstractDbRepository<Long, Event> {
    private UtilizatorDbRepository repoUsers;
    private String url;
    private String username;
    private String password;


    /**
     * Repo-ul de evenimente are referinta spre repo-ul de useri
     * @param dbName
     * @param url
     * @param username
     * @param password
     * @param validator
     * @param repoUsers
     */
    public EventDbRepository(String dbName, String url, String username, String password, Validator<Event> validator,UtilizatorDbRepository repoUsers) {
        super(dbName, url, username, password, validator);
        this.repoUsers=repoUsers;
        this.url=url;
        this.username=username;
        this.password=password;
    }

    @Override
    public Event extractEntity(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong("id");
        Long ownerID= resultSet.getLong("owner_id");
        Utilizator owner=repoUsers.findOne(ownerID).get();
        String startDateString = resultSet.getString("start_date");
        String endDateString = resultSet.getString("end_date");
        String eventTitle=resultSet.getString("title");
        String eventDescription=resultSet.getString("description");
        LocalDateTime startDate = LocalDateTime.parse(startDateString, Constants.DATE_TIME_FORMATTER);
        LocalDateTime endDate = LocalDateTime.parse(endDateString, Constants.DATE_TIME_FORMATTER);
        Event createdEvent=new Event(owner,startDate,endDate,eventTitle,eventDescription);
        createdEvent.setId(id);
        return createdEvent;
    }

    @Override
    public String createEntityAsQueryInsert(Event entity) {
        String startDate="'"+entity.getStartDate().format(Constants.DATE_TIME_FORMATTER_HM)+"'";
        String endDate="'"+entity.getEndDate().format(Constants.DATE_TIME_FORMATTER_HM)+"'";
        String title="'"+entity.getName()+"'";
        String description="'"+entity.getDescription()+"'";
        return "INSERT INTO events(owner_id, start_date, end_date, title, description)"+
                "VALUES ("+entity.getOwner().getId()+","+startDate+","+endDate+","+title+","+description+");";
    }

    @Override
    public String createEntityAsQueryDelete(Long aLong) {
        return "DELETE FROM events WHERE id="+aLong;
    }

    @Override
    public String createEntityAsQueryFindOne(Long aLong) {
        return "SELECT * FROM EVENTS WHERE id="+aLong;
    }

    public void followEvent(Long user_id,Long eventID) {

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO events_users(event_id,user_id)" +
                     "VALUES(? ,?)"))

        {
            statement.setLong(1, eventID);
            statement.setLong(2,user_id);
            statement.executeUpdate();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void unfollowEvent(Long user_id,Long eventID) {

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM events_users where user_id=? and event_id=?"))
        {
            statement.setLong(1, user_id);
            statement.setLong(2,eventID);
            statement.executeUpdate();
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public Iterable<Utilizator> getAllParticipantsFromEvent(long eventID) {
        ArrayList<Utilizator> list = new ArrayList<Utilizator>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("select * from events_users where event_id="+eventID);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long id = resultSet.getLong("user_id");
                Utilizator participant= repoUsers.findOne(id).get();
                list.add(participant);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }


    /**
     * Se returneaza atat evenimentele la care s-a dat unfollow cat si cele care nu au fost de fapt urmarite niciodata
     * @param userID
     * @param lastID
     * @param pageSize
     * @return
     */
    public Iterable<Event> getUnfollowedEvents(long userID,long lastID,int pageSize){
        ArrayList<Event> list=new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "select * \n" +
                             "from events \n" +
                             "where start_date>current_date and\n" + //=> obtinem toate evenimentele care nu sunt urm de acest user
                             "id not in \n" +                        //se neaga cautarea de mai jos
                             "(\n" +
                             "    select event_id \n" +
                             "    from events_users eu \n" +         //pt un participant, luam toate evenimentele
                             "    inner join events e on eu.event_id  = e.id \n" + //la care participa
                             "    where eu.user_id = "+userID +
                             ") and id>"+lastID+
                             " order by start_date  limit " + pageSize );
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Event event=extractEntity(resultSet);
                list.add(event);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    /**
     * Returneaza toate evenimentele PAGED
     * @param userID
     * @param lastID
     * @param pageSize
     * @return
     */

    public Iterable<Event> getFollowedEvents(long userID,long lastID,int pageSize){
        ArrayList<Event> list=new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "select * \n" +
                             "from events \n" +
                             "where start_date>current_date and\n" +
                             "id in \n" +         // .... id in ... => evenimentul se gaseste printre cele urmarite de acest user
                             "(\n" +
                             "    select event_id \n" +
                             "    from events_users eu \n" +
                             "    inner join events e on eu.event_id  = e.id \n" +
                             "    where eu.user_id = "+userID +

                             ") and id>"+lastID+
                             " order by start_date  limit " + pageSize );

             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Event event=extractEntity(resultSet);
                list.add(event);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }


    public Iterable<Event> getUnfollowedEventsFiltered(long userID,long lastID,int pageSize, String title){
        ArrayList<Event> list=new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "select * \n" +
                             "from events e \n" +
                             " \n order by start_date \n"+

                             "where start_date>current_date and\n" + //=> obtinem toate evenimentele care nu sunt urm de acest user
                             "id not in \n" +                        //se neaga cautarea de mai jos
                             "(\n" +
                             "    select event_id \n" +
                             "    from events_users eu \n" +         //pt un participant, luam toate evenimentele
                             "    inner join events e on eu.event_id  = e.id \n" + //la care participa
                             "    where eu.user_id = "+userID +
                             ") and (LOWER(e.title) like '"+title+ "%'"+" )"+"and id>"+lastID+
                             "   limit " + pageSize );
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Event event=extractEntity(resultSet);
                list.add(event);
            }
            System.out.println(list+ "aici din event search");

            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }


    public Iterable<Event> getFollowedEventsFiltered(long userID,long lastID,int pageSize, String title){
        ArrayList<Event> list=new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "select * \n" +
                             "from events e \n" +
                             " \n order by start_date \n"+

                             "where \n" +
                             "id in \n" +         // .... id in ... => evenimentul se gaseste printre cele urmarite de acest user
                             "(\n" +
                             "    select event_id \n" +
                             "    from events_users eu \n" +
                             "    inner join events e on eu.event_id  = e.id \n" +
                             "    where eu.user_id = "+userID +

                             ") and (LOWER(e.title) like '"+title+ "%'"+")"+"and id>"+lastID+
                             " order by start_date  limit " + pageSize );

             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Event event=extractEntity(resultSet);
                list.add(event);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }






    /**
     * Returneaza toate evenimentele fara a se folosi de paginare
     * @param userID
     * @return
     */
    public Iterable<Event> getAllFollowedEvents(long userID){
        ArrayList<Event> list=new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "select * \n" +
                             "from events \n" +
                             "where start_date>current_date and\n" +
                             "id in \n" +
                             "(\n" +
                             "    select event_id \n" +
                             "    from events_users eu \n" +
                             "    inner join events e on eu.event_id  = e.id \n" +
                             "    where eu.user_id = "+userID +" ) "+
                             " order by start_date");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Event event=extractEntity(resultSet);
                list.add(event);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public Page<Event> findAllUnfollowedEventsPaged(Pageable pageable, long userID, long lastID){
        System.out.println("da  event!4");

        return new PageImplementation<Event>(pageable, StreamSupport.stream(this.getUnfollowedEvents(userID,lastID,pageable.getPageSize()).spliterator(),false));
    }

    public Page<Event> findAllFollowedEventsPaged(Pageable pageable, long userID, long lastID){
        return new PageImplementation<Event>(pageable, StreamSupport.stream(this.getFollowedEvents(userID,lastID,pageable.getPageSize()).spliterator(),false));
    }


    public Page<Event> findAllUnfollowedEventsPagedFiltered(Pageable pageable, long userID,String name,long lastID){
        System.out.println("da events 10");
        return new PageImplementation<Event>(pageable, StreamSupport.stream(this.getUnfollowedEventsFiltered(userID,lastID,pageable.getPageSize(), name).spliterator(),false));
    }

    public Page<Event> findAllFollowedEventsPagedFiltered(Pageable pageable, long userID,String name,long lastID){
        return new PageImplementation<Event>(pageable, StreamSupport.stream(this.getFollowedEventsFiltered(userID,lastID,pageable.getPageSize(), name).spliterator(),false));
    }

}
