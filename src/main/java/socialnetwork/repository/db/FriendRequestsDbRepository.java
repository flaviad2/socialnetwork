package socialnetwork.repository.db;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.PageImplementation;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.utils.Constants;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.StreamSupport;

public class FriendRequestsDbRepository implements Repository<Tuple<Long, Long>, FriendRequest> {

    /**
     * folosite cand creez repo db
     */
    private String url;
    private String username;
    private String password;
    private Validator<FriendRequest> validator;
    private UtilizatorDbRepository repoU;

    /**
     * @param url       db postgres
     * @param username  db postgres
     * @param password  db postgres
     * @param validator validatorul entitatii validator
     */
    public FriendRequestsDbRepository(String url, String username, String password, Validator<FriendRequest> validator,UtilizatorDbRepository repoU ) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        this.repoU=repoU;
    }


    /**
     * @return all entities
     */

    @Override
    public Iterable<FriendRequest> findAll() {

        Set<FriendRequest> requests = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friend_requests");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {

                Long id1 = resultSet.getLong("requester_id");
                Long id2 = resultSet.getLong("addressee_id");
                String status = resultSet.getString("status");
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String date = resultSet.getString("date");
                LocalDate localDate = LocalDate.parse(date, formatter);

                Utilizator u1=repoU.findOne(id1).get();
                Utilizator u2=repoU.findOne(id2).get();


                FriendRequest request= new FriendRequest(u1, u2, localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth());
                request.setStatus(Status.valueOf(status));

                requests.add(request);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return requests;
    }



    @Override
    public FriendRequest findOne(Tuple<Long, Long> id) {


        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friend_requests ");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {

                Long id1 = resultSet.getLong("requester_id");
                Long id2 = resultSet.getLong("addressee_id");

                if(id1==id.getLeft() && id2==id.getRight() || id1==id.getRight() && id2==id.getLeft() ) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

                    String date = resultSet.getString("date");
                    LocalDate localDate = LocalDate.parse(date, formatter);


                    Utilizator user1=repoU.findOne(id1).get();
                    Utilizator user2=repoU.findOne(id2).get();

                    FriendRequest friendRequest=new FriendRequest(user1, user2,  localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth() );

                    return friendRequest;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;


    }



    @Override
    public FriendRequest save(FriendRequest entity) {

        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);
        int no_row=0;

        String sql = "insert into friend_requests (requester_id, addressee_id, status, date) values (?, ?, ?,CAST(? as date))";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, entity.getUser1().getId());
            ps.setLong(2, entity.getUser2().getId());

            ps.setString(3, entity.getStatus().toString());
            ps.setString(4,entity.getDate().toString());


            no_row=ps.executeUpdate();
            if(no_row!=0)
                return entity;


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendRequest delete(Tuple<Long, Long> aLong) {

        if (aLong==null)
            throw new IllegalArgumentException("entity must be not null");

        FriendRequest p=findOne(aLong);
        int row_count=0;


        String sql = "delete from friend_requests where (requester_id = ? and addressee_id=? or requester_id=? and addressee_id=?)";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, aLong.getLeft());
            ps.setLong(2, aLong.getRight());
            ps.setLong(3, aLong.getRight());
            ps.setLong(4,aLong.getLeft());

            row_count=ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(row_count==0)
            return null;
        return p;

    }

    @Override
    public FriendRequest update(FriendRequest entity) {

        validator.validate(entity);

        String sql = "update friend_requests SET status=? where (requester_id=? and addressee_id=? OR requester_id=? and addressee_id=?)  ";
        int row_count = 0;


        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {


            ps.setString(1, entity.getStatus().toString());
            ps.setLong(2, entity.getId().getLeft());
            ps.setLong(3, entity.getId().getRight());
            ps.setLong(4, entity.getId().getRight());
            ps.setLong(5, entity.getId().getLeft());

            row_count=ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        if(row_count==0)
            return null;
        return entity;
    }




    @Override
    public int size()
    {
        String sql = " SELECT COUNT(*) AS total \n" +
                "FROM friend_requests" +
                "  ";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {


                return resultSet.getInt("total");

            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return -1;

    }
    public Iterable<Prietenie> getFriendsUser(long userID, long lastID, int pageSize) {
        ArrayList<Prietenie> list = new ArrayList<Prietenie>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("" +
                     "select requester_id+addressee_id-"+userID+" as friend_id,date,status " +
                     "from friend_requests\n" +
                     "where (addressee_id=" + userID + " or requester_id=" + userID + ") and status='APPROVED'"+
                     "and requester_id+addressee_id-"+userID+">"+lastID+" order by requester_id+addressee_id-"+userID+" limit "+pageSize);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long requesterID = userID;
                Long addresseeID = resultSet.getLong("friend_id");
                String createdDateString = resultSet.getString("date");
                String status = resultSet.getString("status");
                LocalDateTime createdDate = LocalDateTime.parse(createdDateString, Constants.DATE_TIME_FORMATTER_DAY);

                Utilizator u1=repoU.findOne(requesterID).get();
                Utilizator u2= repoU.findOne(addresseeID).get();
                Prietenie friendship =new Prietenie(requesterID, addresseeID, createdDate.getYear(),  createdDate.getMonthValue(), createdDate.getDayOfMonth());
                list.add(friendship);
            }

            System.out.println(list +"din db getfriends");
            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(list +"din db getfriends");

        System.out.println(list);

        return list;
    }


    //toate cererile de prietenie adresate acestui user
    //cereri de prietenie Pending
    //pentru ca folosim functia ca sa afisam cererile la care nu s-a rasp
    public List<FriendRequest> getFriendRequestsUser(long userID) {
        ArrayList<FriendRequest> list = new ArrayList<FriendRequest>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("" +
                     "select requester_id, addressee_id , date, status " +
                     "from friend_requests\n" +
                     "where addressee_id=" + userID +" and status='PENDING'");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long addresseeID = userID;
                Long requesterID = resultSet.getLong("requester_id");
                String status = resultSet.getString("status");
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

                String date = resultSet.getString("date");
                LocalDate createdDate = LocalDate.parse(date, formatter);


                Utilizator u1=repoU.findOne(requesterID).get();
                Utilizator u2= repoU.findOne(addresseeID).get();
                FriendRequest friendship =new FriendRequest(u1, u2, createdDate.getYear(),  createdDate.getMonthValue(), createdDate.getDayOfMonth());
                list.add(friendship);
            }

            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }


        return list;
    }

    //toate cererile de prietenie trimise de acest user
    //cereri de prietenie Pending
    public List<FriendRequest> getSentFriendRequestsUser(long userID) {
        ArrayList<FriendRequest> list = new ArrayList<FriendRequest>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("" +
                     "select requester_id, addressee_id , date, status " +
                     "from friend_requests\n" +
                     "where requester_id=" + userID +" and status='PENDING'");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long requesterID = userID;
                Long addresseeID  = resultSet.getLong("addressee_id");
                String status = resultSet.getString("status");
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

                String date = resultSet.getString("date");
                LocalDate createdDate = LocalDate.parse(date, formatter);


                Utilizator u1=repoU.findOne(requesterID).get();
                Utilizator u2= repoU.findOne(addresseeID).get();
                FriendRequest friendship =new FriendRequest(u1, u2, createdDate.getYear(),  createdDate.getMonthValue(), createdDate.getDayOfMonth());
                list.add(friendship);
            }

            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }


        return list;
    }
    public Iterable<Prietenie> getFirstNFriendsUser(long userID,long n) {
        ArrayList<Prietenie> list = new ArrayList<Prietenie>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT * from friend_requests\n" +
                             "where (addressee_id=" + userID + " or requester_id=" + userID + ") and status!='APPROVED' order by requester_id+addressee_id-"+userID+" LIMIT "+n);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {

                Long requesterID = resultSet.getLong("requester_id");
                Long addresseeID = resultSet.getLong("addressee_id");
                String createdDateString = resultSet.getString("date");
                String status = resultSet.getString("status");
                LocalDateTime createdDate = LocalDateTime.parse(createdDateString, Constants.DATE_TIME_FORMATTER_DAY);
                Prietenie friendship = new Prietenie(requesterID, addresseeID, createdDate.getYear(), createdDate.getMonthValue(), createdDate.getDayOfMonth());
                list.add(friendship);
            }
            System.out.println(list +"din db getNNfriends");

            System.out.println(list);

            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(list +"din db getNNfriends");

        System.out.println(list);

        return list;
    }

    public Page<Prietenie> findAllFriendsPaged(Pageable pageable, long userID, long lastID){
        return new PageImplementation<Prietenie>(pageable, StreamSupport.stream(this.getFriendsUser(userID,lastID,pageable.getPageSize()).spliterator(),false));
    }



}