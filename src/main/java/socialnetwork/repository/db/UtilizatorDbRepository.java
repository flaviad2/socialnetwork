package socialnetwork.repository.db;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.PageImplementation;
import socialnetwork.repository.paging.Pageable;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

public class UtilizatorDbRepository extends AbstractDbRepository<Long, Utilizator> {


    private static int apel=0;

    private String url;
    private String username;
    private String password;
    public UtilizatorDbRepository(String dbName,String url, String username, String password, Validator<Utilizator> validator) {
        super(dbName,url,username,password,validator);
        this.url=url;
        this.username=username;
        this.password=password;
    }

    /**
     * Se cauta un utilizator dupa email si parola
     * @param email
     * @param password
     * @return utilizatorul, daca exista
     */

    public Optional<Utilizator> checkBypass(String email,String password){
        if(email.length()==0 || password.length()==0)
            return Optional.empty();
        String query="SELECT * FROM users WHERE '"+email+"'=email AND password=crypt('"+password+"',password)";
        try (Connection connection = DriverManager.getConnection(url, username, this.password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                return Optional.of(extractEntity(resultSet));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return Optional.empty();
    }



    public Utilizator findOneByEmail(String aEmail) {

        System.out.println("din email"+ aEmail);
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from users where users.email = ?")){
            statement.setString(1,aEmail);

            ResultSet resultSet = statement.executeQuery() ;

            if (resultSet.next()) {



                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("firstname");
                String lastName = resultSet.getString("lastname");
                String password=resultSet.getString("password");
                Utilizator user = new Utilizator(firstName, lastName,aEmail,password);
                System.out.println("Din cauta dupa email: "+ user);
                user.setId(id);
                return user;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


    public boolean checkExistingEmail(String email){
        String query="SELECT * FROM users WHERE '"+email +"'=email";
        try (Connection connection = DriverManager.getConnection(url, username, this.password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    @Override
    public Utilizator extractEntity(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong("id");
        String firstName = resultSet.getString("firstname");
        String lastName = resultSet.getString("lastname");
        String email=resultSet.getString("email");
        String password=resultSet.getString("password");
        Utilizator user = new Utilizator(firstName, lastName,email,password);
        user.setId(id);
        return user;
    }


    @Override
    public String createEntityAsQueryDelete(Long userID) {
        return "DELETE FROM users WHERE id="+userID;
    }

    /**
     *   salt used:blowfish
     *   (xor, sum... in algoritm)
     * @param user
     * @return query de introduce in Db a unui Utilizator, avand datele lui
     * parola este criptata cu blowfish si apare in Db criptata folosind functia crypt din SQL
     */

    @Override
    public String createEntityAsQueryInsert(Utilizator user) {
        Long id=user.getId();
        String firstName="'"+user.getFirstName()+"'";
        String lastName="'"+user.getLastName()+"'";
        String email="'"+user.getEmail()+"'";
        String password="'"+user.getPassword()+"'";
        return "INSERT INTO users (id, firstname, lastname,email,password) VALUES " +
                "("+id+","+firstName+","+lastName+","+email+",crypt("+password+",gen_salt('bf',8)));";
    }

    public String createEntityAsQueryFindOne(Long userID){
        return "SELECT * FROM users WHERE id="+userID;
    }


    /**
     * Metoda folosita cand se face cautarea in tabelul de useri pt a trimite o cerere de prietenie
     * @param userID
     * @param name
     * @param lastID
     * @param pageSize
     * @return
     */
    public List<Utilizator> getNonFriendsUserFilteredName(Long userID,String name,Long lastID,int pageSize){

        ArrayList<Utilizator> list = new ArrayList<Utilizator>();
        pageSize=1;
        if(apel==0)
        {pageSize=2;
            apel=apel+1;
          }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT u2.id,u2.firstname,u2.lastname,u2.email,u2.password\n"+
                             "FROM users u1,users u2\n"+
                             "WHERE NOT EXISTS(SELECT 1 FROM prietenii p\n"+
                             "WHERE (\n" +
                             "(p.id1 = u1.id AND p.id2 = u2.id) or\n"+
                             "(p.id1 = u2.id AND p.id2 = u1.id)))\n"+
                             " AND NOT EXISTS(SELECT 1 FROM friend_requests fr\n"+
                             "WHERE (\n" +
                             "(fr.requester_id = u1.id AND fr.addressee_id = u2.id) or\n"+
                             "(fr.requester_id = u2.id AND fr.addressee_id = u1.id)))\n"+
                             "AND u1.id != u2.id AND u1.id = "+userID +"\n" +
                             "AND (LOWER(CONCAT(u2.firstname, ' ', u2.lastname, ' ', u2.email)) like '"+name+"%'"+
                             "OR LOWER(u2.firstname) like '"+name+"%'"+
                             "OR LOWER(u2.lastname) like '"+name+"%'"+
                             "OR LOWER(u2.email) like '"+name+"%')"+

                             "AND u2.id>"+lastID+" limit "+pageSize
             );
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Utilizator user = extractEntity(resultSet);
                list.add(user);

            }
            System.out.println("PAGE SIZE DIN DB E "+ pageSize);
            System.out.println("LAST ID DIN DB E: "+ lastID);
            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public Iterable<Utilizator> getReceivedRequestsUser(long userID,long lastID,int pageSize) {
        ArrayList<Utilizator> list = new ArrayList<Utilizator>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("select * from friend_requests where\n" +
                     "addressee_id="+userID+" and status='PENDING'\n"+
                     "and requester_id>"+lastID+" order by requester_id limit "+pageSize);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long id = resultSet.getLong("requester_id");
                Utilizator u= this.findOne(id).get();
                list.add(u);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public Iterable<Utilizator> getSentRequestsUser(long userID,long lastID, int pageSize) {
        ArrayList<Utilizator> list = new ArrayList<Utilizator>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("select * from friend_requests where\n" +
                     "requester_id="+userID+" and status='PENDING'\n"+
                     "and addressee_id>"+lastID+" order by addressee_id limit "+pageSize);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long id = resultSet.getLong("addressee_id");
                Utilizator u= this.findOne(id).get();
                list.add(u);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }



    public Page<Utilizator> findAllNonFriendsPaged(Pageable pageable, long userID,String name,long lastID){
        return new PageImplementation<Utilizator>(pageable, StreamSupport.stream(this.getNonFriendsUserFilteredName(userID,name,lastID,pageable.getPageSize()).spliterator(),false));
    }

    public Page<Utilizator> findAllReceivedRequestsPaged(Pageable pageable, long userID,long lastID){
        return new PageImplementation<Utilizator>(pageable, StreamSupport.stream(this.getReceivedRequestsUser(userID,lastID,pageable.getPageSize()).spliterator(),false));
    }

    public Page<Utilizator> findAllSentRequestsPaged(Pageable pageable, long userID,long lastID){
        return new PageImplementation<Utilizator>(pageable, StreamSupport.stream(this.getSentRequestsUser(userID,lastID, pageable.getPageSize()).spliterator(),false));
    }
}

