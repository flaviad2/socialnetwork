package socialnetwork.repository.db;

import socialnetwork.domain.GroupChat;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GroupChatDbRepository implements Repository<Long, GroupChat> {

    private String url;
    private String username;
    private String password;
    //private Validator
    private UtilizatorDbRepository repoU;
    private MessageDbRepository repoM;

    public GroupChatDbRepository(String url, String username, String password, UtilizatorDbRepository repoU, MessageDbRepository repoM) {
        this.url = url;
        this.username = username;
        this.password = password;

        this.repoU = repoU;
        this.repoM = repoM;
    }

    @Override
    public GroupChat findOne(Long aLong) {
        GroupChat groupChat = null;

        String sql = "select * from groups where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, aLong);

            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                Long idG = resultSet.getLong("id");
                Long idM = resultSet.getLong("id_m");

                List<Message> messages = new ArrayList<>();
                List<Utilizator> users = new ArrayList<>();
                Message aux = null;

                String sql2 = "select * from groups \n" +
                        "    left join group_messages as g_m on g_m.id = groups.id" +
                        "    left join message on group_messages.id_mess= message.id_m \n";
                PreparedStatement statement1 = connection.prepareStatement(sql2);


                ResultSet resultSet1 = statement1.executeQuery();

                while (resultSet1.next()) {
                    Long idMes = resultSet1.getLong("id_mess");


                    aux = repoM.findOne(idMes);
                    messages.add(aux);

                }

                if (messages != null) {
                    Utilizator emitator = messages.get(0).getFrom();
                    users.add(emitator);
                    for (Utilizator u : messages.get(0).getTo())
                        users.add(u);

                }


                groupChat = new GroupChat(users, messages);
                ps.execute();

            }

            return groupChat;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Din findOne in DB:" + groupChat);
        return groupChat;
    }

    @Override
    public Iterable<GroupChat> findAll() {
        return null;
    }

    @Override
    public GroupChat save(GroupChat entity) {
        return null;
    }

    @Override
    public GroupChat delete(Long aLong) {
        return null;
    }

    @Override
    public GroupChat update(GroupChat entity) {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }


}