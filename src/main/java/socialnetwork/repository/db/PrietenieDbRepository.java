package socialnetwork.repository.db;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.PageImplementation;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.utils.Constants;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.StreamSupport;

public class PrietenieDbRepository implements Repository<Tuple<Long, Long>, Prietenie> {

    /**
     * folosite cand creez repo db
     */
    private String url;
    private String username;
    private String password;
    private Validator<Prietenie> validator;

    /**
     * @param url       db postgres
     * @param username  db postgres
     * @param password  db postgres
     * @param validator validatorul entitatii validator
     */
    public PrietenieDbRepository(String url, String username, String password, Validator<Prietenie> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }


    /**
     *
     * @return all entities
     */

    @Override
    public Iterable<Prietenie> findAll() {
        Set<Prietenie> prietenii = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "\n" +
                             "select f.id1 as id1, f.id2 as id2, u1.firstname as fN1, u2.firstname as fN2, f.date as date \n" +
                             "from prietenii f \n" +
                             "inner join users u1 on u1.id = f.id1 \n" +
                             "inner join users u2 on u2.id = f.id2"
             );
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");


                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String date = resultSet.getString("date");

                LocalDate localDate = LocalDate.parse(date, formatter);

                Prietenie prietenie = new Prietenie(id1, id2, localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth());
                prietenii.add(prietenie);
            }

            return prietenii;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return the entity with the specified id
     *          or null - if there is no entity with the given id
     * @throws IllegalArgumentException
     *                  if id is null.
     */
    @Override

    public Prietenie findOne(Tuple<Long, Long> id) {


        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from prietenii ");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {

                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");

                if(id1==id.getLeft() && id2==id.getRight() || id1==id.getRight() && id2==id.getLeft() ) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

                    String date = resultSet.getString("date");
                    LocalDate localDate = LocalDate.parse(date, formatter);

                    Prietenie prietenie = new Prietenie(id1, id2, localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth());
                    return prietenie;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Prietenie save(Prietenie entity) {

        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);

        int row_count=0;

        String sql2=
                "insert into prietenii ( id1, id2, date) values (?, ?, CAST(? as date))  ";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql2)) {

            ps.setInt(1, entity.getId().getLeft().intValue());
            ps.setInt(2, entity.getId().getRight().intValue());
            ps.setString(3,entity.getDate().toString());

            row_count=ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(row_count==0)
            return entity;
        return null;
    }

    @Override
    public Prietenie delete(Tuple<Long, Long> aLong) {

        if (aLong==null)
            throw new IllegalArgumentException("entity must be not null");

        Prietenie p=findOne(aLong);
        int row_count=0;

        String sql = "delete from prietenii where (id1 = ? and id2=? or id1=? and id2=?)";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, aLong.getLeft());
            ps.setLong(2, aLong.getRight());
            ps.setLong(3, aLong.getRight());
            ps.setLong(4,aLong.getLeft());

            row_count=ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(row_count==0)
            return null;
        return p;

    }

    @Override
    public Prietenie update(Prietenie entity) {

        validator.validate(entity);

        String sql = "update prietenii SET date=CAST(? as date) where (id1=? and id2=? OR id1=? and id2=?)  ";
        int row_count = 0;


        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getDate().toString());
            ps.setLong(2, entity.getId().getLeft());
            ps.setLong(3, entity.getId().getRight());
            ps.setLong(4, entity.getId().getRight());
            ps.setLong(5, entity.getId().getLeft());

            row_count=ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(row_count==0)
            return null;
        return entity;
    }


    @Override
    public int size()
    {
        String sql = " SELECT COUNT(*) AS total \n" +
                "FROM prietenii" +
                "  ";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                return resultSet.getInt("total");
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return -1;
    }


    public Iterable<Prietenie> getFriendsUser(long userID,long lastID,int pageSize) {
        ArrayList<Prietenie> list = new ArrayList<Prietenie>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("" +
                     "select id1+id2-"+userID+" as friend_id,date " +
                     "from prietenii \n" +
                     "where (id1=" + userID + " or id2=" + userID + ")"+
                     "and id1+id2-"+userID+">"+lastID+" order by id1+id2-"+userID+" limit "+pageSize);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long requesterID = userID;
                Long addresseeID = resultSet.getLong("friend_id");
                String createdDateString = resultSet.getString("date");


                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

                String date = resultSet.getString("date");
                LocalDate createdDate = LocalDate.parse(date, formatter);
                //LocalDate createdDate = LocalDate.parse(createdDateString, Constants.DATE_TIME_FORMATTER_DAY);
                Prietenie friendship = new Prietenie(requesterID, addresseeID, createdDate);

                list.add(friendship);
            }
            System.out.println(list +"din db getfriends ppp");

            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(list +"din db getfriends ppp");

        return list;
    }



    public Iterable<Prietenie> getFriendsUserFiltered(long userID,long lastID,int pageSize, String name) {
        ArrayList<Prietenie> list = new ArrayList<Prietenie>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("" +
                     "select p.id1+p.id2-"+userID+" as friend_id,date " +
                     "from prietenii p left join users u on (p.id1=u.id  or p.id2=u.id) \n " +
                     "where ( (p.id1=" + userID + " or p.id2=" + userID + ") and (LOWER(u.firstname) like '" + name + "%' or " +
                     "LOWER(u.lastname) like'"+ name+"%' or " +
                     "LOWER(u.email) like '"+name+"%'))"+
                     "and p.id1+p.id2-"+userID+">"+lastID+" order by p.id1+p.id2-"+userID+" limit "+pageSize);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long requesterID = userID;
                Long addresseeID = resultSet.getLong("friend_id");
                String createdDateString = resultSet.getString("date");


                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

                String date = resultSet.getString("date");
                LocalDate createdDate = LocalDate.parse(date, formatter);
                //LocalDate createdDate = LocalDate.parse(createdDateString, Constants.DATE_TIME_FORMATTER_DAY);
                Prietenie friendship = new Prietenie(requesterID, addresseeID, createdDate);

                list.add(friendship);
            }
            System.out.println(list +"din db getfriends ppp");

            return list;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(list +"din db getfriends ppp");

        return list;
    }





    public Page<Prietenie> findAllFriendsPaged(Pageable pageable, long userID, long lastID){
        return new PageImplementation<Prietenie>(pageable, StreamSupport.stream(this.getFriendsUser(userID,lastID,pageable.getPageSize()).spliterator(),false));
    }


    public Page<Prietenie> findAllFriendsPagedFiltered(Pageable pageable, long userID, long lastID, String name){
        return new PageImplementation<Prietenie>(pageable, StreamSupport.stream(this.getFriendsUserFiltered(userID,lastID,pageable.getPageSize(), name).spliterator(),false));
    }
}