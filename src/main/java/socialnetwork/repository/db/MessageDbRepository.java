package socialnetwork.repository.db;


import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.PageImplementation;
import socialnetwork.repository.paging.Pageable;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.StreamSupport;

public class MessageDbRepository implements Repository<Long, Message> {
    private String url;
    private String username;
    private String password;
    private Validator<Message> validator;
    private UtilizatorDbRepository repoU;


    /**
     * Constructorul clasei
     * @param url - db postgres
     * @param username - db postgres
     * @param password - db postgres
     * @param validator - validatorul entitatii
     * @param repoU - Repo-ul de utilizatori
     */
    public MessageDbRepository(String url, String username, String password, Validator<Message> validator, UtilizatorDbRepository repoU) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        this.repoU = repoU;
    }


    /**
     * @param aLong - id-ul dupa care se cauta entitatea
     * @return - entitatea
     */
    @Override
    public Message findOne(Long aLong) {
        Message message = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        String sql = "select * from message where id_m = ?";
        try (Connection connection = DriverManager.getConnection(url, username,password);
             PreparedStatement ps =connection.prepareStatement(sql)){
            ps.setLong(1, aLong);

            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                Long idM = resultSet.getLong("id_m");
                Long idFrom = resultSet.getLong("from");
                Date date = resultSet.getDate("date");
                String text = resultSet.getString("text");
                Long originalM = resultSet.getLong("original_message");
                Utilizator from = repoU.findOne(idFrom).get();
                List<Utilizator> to = new ArrayList<>();

                String sql2 = "select * from message\n" +
                        "    left join receivers as r on message.id_m = r.id_message\n" +
                        "    left join users as u on u.id = r.id_receiver\n" +
                        " where  message.id_m=?";
                PreparedStatement statement1 = connection.prepareStatement(sql2);

                statement1.setLong(1, idM);
                ResultSet resultSet1 = statement1.executeQuery();
                String emailReceiver;
                String firstNameReceiver;
                String lastNameReceiver;
                String passwordReceiver;
                while(resultSet1.next()) {
                    Long idTo = resultSet1.getLong("id_receiver");
                    emailReceiver = resultSet1.getString("email");
                    passwordReceiver=resultSet1.getString("password");
                    firstNameReceiver = resultSet1.getString("firstname");
                    lastNameReceiver = resultSet1.getString("lastname");
                    Utilizator receiver = new Utilizator(firstNameReceiver, lastNameReceiver, emailReceiver, passwordReceiver);
                    receiver.setId(idTo);
                    to.add(receiver);
                }

                message = new Message(from, convertToLocalDateTime(date), to, text);
                message.setOriginalMessage(originalM);
                message.setId(idM);
                ps.execute();

            }

            return message;
        }catch(SQLException e){
           e.printStackTrace();
        }
        return message;
    }


    /**
     * @return toate entitatile de tip Message
     */
    @Override
    public Iterable<Message> findAll() {
        Set<Message> messages = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT * from message m inner join users as u on u.id = m.from");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id_m");
                Long idFrom = resultSet.getLong("from");
                Timestamp date = resultSet.getTimestamp("date");

                String text = resultSet.getString("text");
                String email = resultSet.getString("email");
                String firstName = resultSet.getString("firstname");
                String lastName = resultSet.getString("lastname");
                String password2= resultSet.getString("password");
                Long originalMessage =resultSet.getLong("original_message");

                List<Utilizator> to = new ArrayList<>();
                try (Connection connection1 = DriverManager.getConnection(url, username, password);
                     PreparedStatement statement1 = connection1.prepareStatement(
                             "select * from message\n" +
                                     "    left join receivers as r on message.id_m = r.id_message\n" +
                                     "    left join users as u on u.id = r.id_receiver\n" +
                                     " where  message.id_m=?"))
                {
                    statement1.setLong(1, id);
                    ResultSet resultSet1 = statement1.executeQuery();
                    String emailReceiver;
                    String firstNameReceiver;
                    String lastNameReceiver;
                    String passwordReceiver;
                    while(resultSet1.next()) {

                        Long idTo = resultSet1.getLong("id_receiver");
                        emailReceiver =resultSet1.getString("email");
                        firstNameReceiver = resultSet1.getString("firstname");
                        lastNameReceiver = resultSet1.getString("lastname");
                        passwordReceiver = resultSet1.getString("password");
                        Utilizator receiver = new Utilizator( firstNameReceiver, lastNameReceiver, emailReceiver,passwordReceiver );
                        receiver.setId(idTo);
                        to.add(receiver);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                Utilizator from = new Utilizator(firstName, lastName,email, password2 );
                from.setId(idFrom);
                Message message = new Message(from, date.toLocalDateTime(), to, text);
                //System.out.println(convertToLocalDateTime(date));
                message.setOriginalMessage(originalMessage);
                message.setId(id);
                messages.add(message);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messages;
    }


    public LocalDateTime convertToLocalDateTime(Date dateToConvert) {
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }


    /**
     * metoda ce salveaza entitatile in repo
     * @param entity entity must be not null
     * @return entitatea daca se salveaza, altfel null
     */
    @Override
    public Message save(Message entity) {
        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);
        Long id_message = null;
        int row_count=0;
        String sql = "insert into message (\"from\", date, text, original_message) values (?, ?, ?, ?)";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, entity.getFrom().getId());
            ps.setTimestamp(2, Timestamp.valueOf(entity.getDate()));
            ps.setString(3, entity.getMessage());

            if(entity.getOriginalMessage()!=null)
                ps.setLong(4, entity.getOriginalMessage());
            else ps.setLong(4,0);
            row_count=ps.executeUpdate();



            String sql1 = "select max(id_m) from message";
            PreparedStatement ps1 = connection.prepareStatement(sql1);
            ResultSet resultSet1 = ps1.executeQuery();
            while(resultSet1.next()) {
                id_message = resultSet1.getLong("max");
            }

            ps1.execute();

            String sql2 = "insert into receivers (id_message, id_receiver) values (?, ?)";
            PreparedStatement ps2 = connection.prepareStatement(sql2);
            for (Utilizator user : entity.getTo() ){
                ps2.setLong(1, id_message);
                ps2.setLong(2, user.getId());
                ps2.execute();
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if(row_count!=0)
            return entity;
        return null;

    }

    @Override
    public Message delete(Long aLong) {
        return null;
    }

    @Override
    public Message update(Message entity) {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }


    public Message getLastMessage(long id_a,long id_b) {
        ArrayList<Message> list = new ArrayList<Message>();
        Message message = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("select * from message inner join receivers on message.id_m = receivers.id_message \n " +
                     "where ( \"from\"= "+ id_b +"and id_receiver=" +id_a+  " or   \"from\"= " + id_a +"and id_receiver=" +id_b +")"+

                     "order by date desc\n"+
                     "limit 1");

             ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                Long idM = resultSet.getLong("id_m");
                System.out.println(idM);
                Long idFrom = resultSet.getLong("from");
                System.out.println(idFrom);
                Date date = resultSet.getDate("date");
                System.out.println(date);
                String text = resultSet.getString("text");
                System.out.println(text);
                Long originalM = resultSet.getLong("original_message");
                System.out.println(originalM);
                Utilizator from = repoU.findOne(idFrom).get();
                System.out.println(from);
                List<Utilizator> to = new ArrayList<>();

                String sql2 = "select * from message\n" +
                        "    left join receivers as r on message.id_m = r.id_message\n" +
                        "    left join users as u on u.id = r.id_receiver\n" +
                        " where  message.id_m="+idM;
                PreparedStatement statement1 = connection.prepareStatement(sql2);

                ResultSet resultSet1 = statement1.executeQuery();
                String emailReceiver;
                String firstNameReceiver;
                String lastNameReceiver;
                String passwordReceiver;
                while(resultSet1.next()) {
                    Long idTo = resultSet1.getLong("id_receiver");
                    emailReceiver = resultSet1.getString("email");
                    passwordReceiver=resultSet1.getString("password");
                    firstNameReceiver = resultSet1.getString("firstname");
                    lastNameReceiver = resultSet1.getString("lastname");
                    Utilizator receiver = new Utilizator(firstNameReceiver, lastNameReceiver, emailReceiver, passwordReceiver);
                    receiver.setId(idTo);
                    to.add(receiver);
                }
                message = new Message(from, convertToLocalDateTime(date), to, text);
                message.setOriginalMessage(originalM);

                message.setId(idM);
                statement.execute();

            }
            return message;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public Iterable<Message> getConvo(long id_a,long id_b) {

        ArrayList<Message> messages = new ArrayList<Message>();

        Message message=null;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "    select * from message inner join receivers on message.id_m = receivers.id_message \n " +
                             "where ( \"from\"= "+ id_b +"and id_receiver=" +id_a+  " or   \"from\"= " + id_a +"and id_receiver=" +id_b +")"+

                             "order by date desc\n");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id_m");
                Long idFrom = resultSet.getLong("from");
                Timestamp date = resultSet.getTimestamp("date");
                Long originalMessage =resultSet.getLong("original_message");
                String text = resultSet.getString("text");

                Utilizator emitator=repoU.findOne(idFrom).get();
                String email = emitator.getEmail();
                String firstName = emitator.getFirstName();
                String lastName = emitator.getLastName();
                String password2= emitator.getPassword();

                List<Utilizator> to = new ArrayList<>();
                try (Connection connection1 = DriverManager.getConnection(url, username, password);
                     PreparedStatement statement1 = connection1.prepareStatement(
                             "select * from message\n" +
                                     "    left join receivers as r on message.id_m = r.id_message\n" +
                                     "    left join users as u on u.id = r.id_receiver\n" +
                                     " where  message.id_m="+id))
                {
                    ResultSet resultSet1 = statement1.executeQuery();
                    String emailReceiver;
                    String firstNameReceiver;
                    String lastNameReceiver;
                    String passwordReceiver;
                    while(resultSet1.next()) {

                        Long idTo = resultSet1.getLong("id_receiver");
                        emailReceiver =resultSet1.getString("email");
                        firstNameReceiver = resultSet1.getString("firstname");
                        lastNameReceiver = resultSet1.getString("lastname");
                        passwordReceiver = resultSet1.getString("password");
                        Utilizator receiver = new Utilizator( firstNameReceiver, lastNameReceiver, emailReceiver,passwordReceiver );
                        receiver.setId(idTo);
                        to.add(receiver);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                Utilizator from = new Utilizator(firstName, lastName,email, password2 );
                from.setId(idFrom);
                message = new Message(from, date.toLocalDateTime(), to, text);
                message.setOriginalMessage(originalMessage);
                message.setId(id);
                messages.add(message);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messages;
    }


    public Iterable<Message> findAllForUser(long idU) {
        Set<Message> messages = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT * from message m inner join users as u on u.id = m.from ");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id_m");
                Long idFrom = resultSet.getLong("from");
                Timestamp date = resultSet.getTimestamp("date");

                String text = resultSet.getString("text");
                String email = resultSet.getString("email");
                String firstName = resultSet.getString("firstname");
                String lastName = resultSet.getString("lastname");
                String password2= resultSet.getString("password");
                Long originalMessage =resultSet.getLong("original_message");

                List<Utilizator> to = new ArrayList<>();
                try (Connection connection1 = DriverManager.getConnection(url, username, password);
                     PreparedStatement statement1 = connection1.prepareStatement(
                             "select * from message\n" +
                                     "    left join receivers as r on message.id_m = r.id_message\n" +
                                     "    left join users as u on u.id = r.id_receiver\n" +
                                     " where  message.id_m=?"))
                {
                    statement1.setLong(1, id);
                    ResultSet resultSet1 = statement1.executeQuery();
                    String emailReceiver;
                    String firstNameReceiver;
                    String lastNameReceiver;
                    String passwordReceiver;
                    while(resultSet1.next()) {

                        Long idTo = resultSet1.getLong("id_receiver");
                        emailReceiver =resultSet1.getString("email");
                        firstNameReceiver = resultSet1.getString("firstname");
                        lastNameReceiver = resultSet1.getString("lastname");
                        passwordReceiver = resultSet1.getString("password");
                        Utilizator receiver = new Utilizator( firstNameReceiver, lastNameReceiver, emailReceiver,passwordReceiver );
                        receiver.setId(idTo);
                        to.add(receiver);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                Utilizator from = new Utilizator(firstName, lastName,email, password2 );
                from.setId(idFrom);
                Message message = new Message(from, date.toLocalDateTime(), to, text);
                message.setOriginalMessage(originalMessage);
                message.setId(id);

                if(from.getId()==idU || to.contains(repoU.findOne(idU)) )
                messages.add(message);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messages;
    }

    public Iterable<Message> getConvoPaged(long id_a,long id_b,long lastID,int pageSize) {
        ArrayList<Message> messages = new ArrayList<Message>();

        Message message=null;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "    select * from message inner join receivers on message.id_m = receivers.id_message \n " +
                             "where (( \"from\"= "+ id_a +"and id_receiver=" +id_b+  " or   \"from\"= " + id_b +"and id_receiver=" +id_a +")"+
                             "and id_m<" + lastID +" ) \n" +
                             " order by date desc limit " + pageSize);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id_m");

                Long idFrom = resultSet.getLong("from");

                Timestamp date = resultSet.getTimestamp("date");

                String text = resultSet.getString("text");


                Utilizator emitator=repoU.findOne(idFrom).get();



                Long originalMessage =resultSet.getLong("original_message");


                List<Utilizator> to = new ArrayList<>();
                try (Connection connection1 = DriverManager.getConnection(url, username, password);
                     PreparedStatement statement1 = connection1.prepareStatement(
                             "select * from message\n" +
                                     "    left join receivers as r on message.id_m = r.id_message\n" +
                                     "    left join users as u on u.id = r.id_receiver\n" +
                                     " where  message.id_m=?"))
                {
                    statement1.setLong(1, id);
                    ResultSet resultSet1 = statement1.executeQuery();
                    String emailReceiver;
                    String firstNameReceiver;
                    String lastNameReceiver;
                    String passwordReceiver;
                    while(resultSet1.next()) {

                        Long idTo = resultSet1.getLong("id_receiver");
                        emailReceiver =resultSet1.getString("email");
                        firstNameReceiver = resultSet1.getString("firstname");
                        lastNameReceiver = resultSet1.getString("lastname");
                        passwordReceiver = resultSet1.getString("password");
                        Utilizator receiver = new Utilizator( firstNameReceiver, lastNameReceiver, emailReceiver,passwordReceiver );
                        receiver.setId(idTo);
                        to.add(receiver);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }




                message = new Message(emitator, date.toLocalDateTime(), to, text);
                message.setOriginalMessage(originalMessage);

                message.setId(id);

                messages.add(message);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return messages;

    }


    public Iterable<Message> getGroupChatPaged(List<Long> ids,long lastID,int pageSize, Long firstMessage) {
        ArrayList<Message> messages = new ArrayList<Message>();

        Message message=null;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "    select * from message inner join receivers on message.id_m = receivers.id_message \n " +
                             "where message.original_message=" + firstMessage +
                             "and id_m<" + lastID +"  \n" +
                             " order by date desc limit " + pageSize );
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id_m");

                Long idFrom = resultSet.getLong("from");

                Timestamp date = resultSet.getTimestamp("date");

                String text = resultSet.getString("text");


                Utilizator emitator=repoU.findOne(idFrom).get();



                Long originalMessage =resultSet.getLong("original_message");


                List<Utilizator> to = new ArrayList<>();
                try (Connection connection1 = DriverManager.getConnection(url, username, password);
                     PreparedStatement statement1 = connection1.prepareStatement(
                             "select * from message\n" +
                                     "    left join receivers as r on message.id_m = r.id_message\n" +
                                     "    left join users as u on u.id = r.id_receiver\n" +
                                     " where  message.id_m=?"))
                {
                    statement1.setLong(1, id);
                    ResultSet resultSet1 = statement1.executeQuery();
                    String emailReceiver;
                    String firstNameReceiver;
                    String lastNameReceiver;
                    String passwordReceiver;
                    while(resultSet1.next()) {

                        Long idTo = resultSet1.getLong("id_receiver");
                        emailReceiver =resultSet1.getString("email");
                        firstNameReceiver = resultSet1.getString("firstname");
                        lastNameReceiver = resultSet1.getString("lastname");
                        passwordReceiver = resultSet1.getString("password");
                        Utilizator receiver = new Utilizator( firstNameReceiver, lastNameReceiver, emailReceiver,passwordReceiver );
                        receiver.setId(idTo);
                        to.add(receiver);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }




                message = new Message(emitator, date.toLocalDateTime(), to, text);
                message.setOriginalMessage(originalMessage);

                message.setId(id);

                List<Long> aux=new ArrayList<>();
                for(Utilizator u: to)
                    aux.add(u.getId());
                aux.add(idFrom);

                boolean ok=true;
                for(Long idL: ids)
                    if(!aux.contains(idL)) ok=false;

                    for(Long idL:aux)
                        if(!ids.contains(idL) ) ok=false;

                if(ok==true)
                messages.add(message);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return messages;

    }


    public List<Message> getConvoBetweenDates(long id_a, long id_b, LocalDate date1, LocalDate date2) {
        String date1String="'"+date1.toString()+" 00:00:00"+"'";
        String date2String="'"+date2.toString()+" 23:59:59"+"'";
        ArrayList<Message> messages = new ArrayList<Message>();

        Message message=null;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "    select * from message inner join receivers on message.id_m = receivers.id_message \n " +
                             "where ( \"from\"= "+ id_b +"and id_receiver=" +id_a+  " )"+
                             "and (date>="+date1String+" and date<="+date2String+")"+
                             "order by date"
             );
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id_m");
                Long idFrom = resultSet.getLong("from");
                Timestamp date = resultSet.getTimestamp("date");

                String text = resultSet.getString("text");

                Long originalMessage =resultSet.getLong("original_message");

                Utilizator emitator = repoU.findOne(idFrom).get();

                List<Utilizator> to = new ArrayList<>();
                try (Connection connection1 = DriverManager.getConnection(url, username, password);
                     PreparedStatement statement1 = connection1.prepareStatement(
                             "select * from message\n" +
                                     "    left join receivers as r on message.id_m = r.id_message\n" +
                                     "    left join users as u on u.id = r.id_receiver\n" +
                                     " where  message.id_m=?"))
                {
                    statement1.setLong(1, id);
                    ResultSet resultSet1 = statement1.executeQuery();
                    String emailReceiver;
                    String firstNameReceiver;
                    String lastNameReceiver;
                    String passwordReceiver;
                    while(resultSet1.next()) {

                        Long idTo = resultSet1.getLong("id_receiver");
                        emailReceiver =resultSet1.getString("email");
                        firstNameReceiver = resultSet1.getString("firstname");
                        lastNameReceiver = resultSet1.getString("lastname");
                        passwordReceiver = resultSet1.getString("password");
                        Utilizator receiver = new Utilizator( firstNameReceiver, lastNameReceiver, emailReceiver,passwordReceiver );
                        receiver.setId(idTo);
                        to.add(receiver);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }


                message = new Message(emitator, date.toLocalDateTime(), to, text);
                message.setOriginalMessage(originalMessage);
                message.setId(id);
                messages.add(message);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return messages;
    }

    public Page<Message> findAllConversationPaged(Pageable pageable, long userA, long userB, long lastID){
        return new PageImplementation<Message>(pageable, StreamSupport.stream(this.getConvoPaged(userA,userB,lastID,pageable.getPageSize()).spliterator(),false));
    }

    public Page<Message> findAllGroupConversationPaged(Pageable pageable, List<Long> ids, long lastID, Long firstMessage){
        return new PageImplementation<Message>(pageable, StreamSupport.stream(this.getGroupChatPaged(ids,lastID,pageable.getPageSize(), firstMessage).spliterator(),false));
    }

}