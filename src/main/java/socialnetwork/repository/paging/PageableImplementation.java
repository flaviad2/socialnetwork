package socialnetwork.repository.paging;

/**
 * This class adds restrictions that will be used when searching for a page
 --->(In loc de PageRequest object)
 This is an implementation of Pageable interface
 Pass the PageRequest object as an argument to the repo method we intend to use
 We can create a PageRequest object by passing in the requested page number and size
 */
public class PageableImplementation implements Pageable {


    private int pageNumber;
    private int pageSize;

    public PageableImplementation(int pageNumber, int pageSize) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }

    @Override
    public int getPageNumber() {
        return this.pageNumber;
    }

    @Override
    public int getPageSize() {
        return this.pageSize;
    }
}

