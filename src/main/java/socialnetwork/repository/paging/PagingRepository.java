package socialnetwork.repository.paging;

import socialnetwork.domain.Entity;
import socialnetwork.repository.Repository;

public interface PagingRepository<ID , E extends Entity<ID>> extends Repository<ID, E> {

    /**
     *
     * @param pageable
     * @return a Page of entities meeting the paging restriction provided in the Pageable object.
     *
     */
    Page<E> findAll(Pageable pageable);


    /**
     *
     * @param pageable
     * @return a Page of entities meeting the paging restriction provided in the Pageable object.
     * Users will be searched by using userId
     */
    Page<E> findAll(Pageable pageable,long userID);
}