package socialnetwork.repository.paging;


//metadata
//if we'll want to obtain the metadata, we'll make methods return a Page object
import java.util.stream.Stream;

public interface Page<E> {
    Pageable getPageable();

    Pageable nextPageable();

    Stream<E> getContent();


}
