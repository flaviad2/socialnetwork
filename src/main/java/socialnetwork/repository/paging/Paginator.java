package socialnetwork.repository.paging;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Paginator<E> {
    private Pageable pageable;
    private Iterable<E> elements;

    /**
     *
     * @param pageable =restrictii
     * @param elements =colectia de elemente pe care o vom pagina
     */
    public Paginator(Pageable pageable, Iterable<E> elements) {
        this.pageable = pageable;
        this.elements = elements;
    }

    /**
     * functia de paginare care pagineaza colectia de elemente data in constructor
     * @return rezultatul paginarii elementelor
     */
    public Page<E> paginate() {
        Stream<E> result = StreamSupport.stream(elements.spliterator(), false)
                .skip(pageable.getPageNumber()  * pageable.getPageSize())
                .limit(pageable.getPageSize());
        return new PageImplementation<>(pageable, result);
    }
}

