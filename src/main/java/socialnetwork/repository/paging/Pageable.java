package socialnetwork.repository.paging;


/**
 * This interface adds restrictions that will be used when searching for a page
 */
public interface Pageable {


    int getPageNumber();
    int getPageSize();
}
