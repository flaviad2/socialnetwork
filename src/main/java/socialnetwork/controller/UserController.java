package socialnetwork.controller;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import javafx.util.converter.LocalTimeStringConverter;
import socialnetwork.domain.Event;
import socialnetwork.domain.*;
import socialnetwork.domain.exceptions.ServiceException;
import socialnetwork.service.FriendRequestService;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.Constants;
import socialnetwork.utils.PDFCreator.PDFGenerator;
import socialnetwork.utils.events.*;
import socialnetwork.utils.observer.Observer;

import javax.swing.text.html.ImageView;
import java.awt.*;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TimerTask;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.sun.tools.doclint.Entity.image;

public class UserController implements Observer {
    UserPage userPage;



    private UtilizatorService serviceU;
    private PrietenieService serviceP;
    private FriendRequestService serviceC;
    private MessageService serviceM;
    private String  username;
    private Long userId;


    ObservableList<UtilizatorPrietenieDTO> modelFriends = FXCollections.observableArrayList();
    ObservableList<Utilizator> modelUsers= FXCollections.observableArrayList();
    ObservableList<MessageDTO> modelMessages=FXCollections.observableArrayList();
    ObservableList<MessageDTO> modelMessagesG=FXCollections.observableArrayList();

    ObservableList<Utilizator> modelReceivedRequests=FXCollections.observableArrayList();
    ObservableList<Utilizator> modelSentRequests=FXCollections.observableArrayList();
    ObservableList<Event> modelUnfollowedEvents=FXCollections.observableArrayList();

  

    ObservableList<Event> modelFollowedEvents=FXCollections.observableArrayList();
    ObservableList<Event> modelEvents = FXCollections.observableArrayList();

    private List<UtilizatorPrietenieDTO> selectedUsersToChat;
    private List<Utilizator> selecteUsersToChatG;
    private MessageDTO selectedGroupChat;
    private Stage stage;


    private final javafx.scene.image.Image IMAGE_JAN = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/1.jpg");
    private final javafx.scene.image.Image IMAGE_FEB  = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/2.jpg");
    private final javafx.scene.image.Image IMAGE_MAR  = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/3.jpg");
    private final javafx.scene.image.Image IMAGE_APR = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/4.jpg");
    private final javafx.scene.image.Image IMAGE_MAY  = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/5.jpg");
    private final javafx.scene.image.Image IMAGE_JUNE  = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/6.jpg");
    private final javafx.scene.image.Image IMAGE_JULY  = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/7.jpg");
    private final javafx.scene.image.Image IMAGE_AUG = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/8.jpg");
    private final javafx.scene.image.Image IMAGE_SEP  = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/9.jpg");
    private final javafx.scene.image.Image IMAGE_OCT  = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/10.jpg");
    private final javafx.scene.image.Image IMAGE_NOV = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/11.jpg");
    private  final javafx.scene.image.Image IMAGE_DEC  = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/12.jpg");


    @FXML
    private Label label_startDate;

    @FXML
    private javafx.scene.image.ImageView imagClopotel;

    @FXML
    private Label label_endDate;

    @FXML
    private Button buttonGoTo;



    @FXML
    private Label label_eventTitle;

    @FXML
    private Label label_Description;

    @FXML
    private javafx.scene.image.ImageView imageCalendar;

    @FXML
    private Button buttonLogout;

    @FXML
    private ImageView photo;

    @FXML
    private Button buttonSendMessage;

    @FXML
    private Button buttonCreateEv;

    @FXML
    private Button showConvo;

    @FXML
    private Button buttonDeleteFriend;

    @FXML
    private Button buttonAddFriendAction;

    @FXML
    private Label label_remove;
    @FXML
    private Button buttonChangeRequests;

    @FXML
    private Button buttonRequestAccept;

    @FXML
    private Button buttonRequestReject;

    @FXML
    private Button buttonRequestCancel;

    @FXML
    private Button buttonSaveFile;


    @FXML
    private Button buttonChangeEvents;



    //-----------------------TABELA USERS----------------------------------------------
    @FXML
    private TableView<Utilizator> tableViewUsers;

    @FXML
    private TableColumn<Utilizator, String> tableUsersColumnX;

    @FXML
    private TableColumn<Utilizator, String> tableUsersColumnEmail;

    @FXML
    private TableColumn<Utilizator, String> tableUsersColumnFirstName;

    @FXML
    private TableColumn<Utilizator, String> tableUsersColumnLastName;
    //-----------------------------------------------------------------------------------

    //-----------------------TABELA RECEIVED REQUESTS----------------------------------------------
    @FXML
    private TableView<Utilizator> tableViewReceivedRequests;

    @FXML
    private TableColumn<Utilizator, String> tableReceivedColumnEmail;

    @FXML
    private TableColumn<Utilizator, String> tableReceivedColumnFirstName;

    @FXML
    private TableColumn<Utilizator, String> tableReceivedColumnLastName;

    @FXML
    private TableColumn<Utilizator, Button> tableColumnButtonA;



    //-----------------------------------------------------------------------------------

    //-----------------------TABELA SENT REQUESTS----------------------------------------------
    @FXML
    private TableView<Utilizator> tableViewSentRequests;

    @FXML
    private TableColumn<Utilizator, String> tableSentColumnEmail;

    @FXML
    private TableColumn<Utilizator, String> tableSentColumnFirstName;

    @FXML
    private TableColumn<Utilizator, String> tableSentColumnLastName;

    private Event selectedEvent;
    //-----------------------------------------------------------------------------------

    //-----------------------TABELA FRIENDS----------------------------------------------
    @FXML
    private TableView<UtilizatorPrietenieDTO> tableViewFriends;

    @FXML
    private TableColumn<UtilizatorPrietenieDTO,String> tableFriendsColumnEmail;

    @FXML
    private TableColumn<UtilizatorPrietenieDTO,String> tableFriendsColumnFirstName;

    @FXML
    private TableColumn<UtilizatorPrietenieDTO,String> tableFriendsColumnLastName;

    @FXML
    private TableColumn<UtilizatorPrietenieDTO, String> tableFriendsColumnCheck;
    //------------------------------------------------------------------------------------



    //----------------------TABELA UNFOLLOWED EVENTS--------------------------------------
    @FXML
    private TableView<Event> tableViewUnfollowedEvents;

    @FXML
    private TableColumn<Event,String> tableUnfollowedEventsName;


    @FXML
    private AnchorPane anchorPaneRequestInfo;
    @FXML
    private TableColumn<Event, Event> ColB;

    @FXML
    private TableColumn<Event, LocalDateTime> tableUnfollowedEventsStart;
    //------------------------------------------------------------------------------------





    //----------------------TABELA FOLLOWED EVENTS--------------------------------------
    @FXML
    private TableView<Event> tableViewFollowedEvents;

    @FXML
    private TableColumn<Event,String> tableFollowedEventsName;

    @FXML
    private TableColumn<Event, LocalDateTime> tableFollowedEventsStart;
    //------------------------------------------------------------------------------------


    private Long firstMessage;


    @FXML
    private TabPane tabPane;

    @FXML
    private TabPane tabPaneTables;

    @FXML
    private TextField textFieldSearchUser;


    @FXML
    private TextField textFieldSearchFriend;

    @FXML
    private TextField textFieldSearchEvent1;

    @FXML
    private TextField textFieldSearchEvent2;



    @FXML
    private TextField textFieldTypeMessage;

    @FXML
    private TextField textFieldPDFName;

    @FXML
    private TextField textFieldFileOutput;

    @FXML
    private TextField textFieldEventName;

    @FXML
    private TextArea textAreaEventDescription;


    @FXML
    private ListView<MessageDTO> listViewChat;

    @FXML
    private ListView<MessageDTO> listViewChatG;



    @FXML
    private ListView<String> listViewReport;

    @FXML
    private ListView<String> listViewEvent;

    @FXML
    private ListView<String> listViewEvent1;



    @FXML
    private ListView<String> listViewUpcoming;



    @FXML
    private ListViewWithImages listViewUp;


    @FXML
    private DatePicker datePicker1;

    @FXML
    private DatePicker datePicker2;

    @FXML
    private DatePicker datePickerEventStart;

    @FXML
    private DatePicker datePickerEventEnd;

    @FXML
    private ComboBox<String> comboBoxReport;


    @FXML
    private AnchorPane anchorSaveFile;

    @FXML
    private AnchorPane anchorInfo;

    @FXML
    private AnchorPane anchorAddEvent;


    @FXML
    private Button buttonCreateGroupChat;

    @FXML
    private Button buttonShowNotificationReq;

    @FXML
    Spinner<LocalTime> startSpinner;

    @FXML
    Spinner<LocalTime> endSpinner;

    @FXML
    Label nameLabel;

    @FXML
    private final Button buttonParticipantsEv=new Button();

    @FXML
    private final Button buttonInfoEv=new Button();

    @FXML
    Label label_chat;

    @FXML
    Label dateLabel;

    Timeline clock;


    @FXML
    private Message last_message=null;

    @FXML
    private  javafx.scene.image.ImageView imag2;

    @FXML
    private javafx.scene.image.ImageView img1;

    @FXML
    private javafx.scene.image.ImageView img3;

    @FXML
    private javafx.scene.image.ImageView imgEvents;

    @FXML
    private Image Image;

    @FXML
    private final javafx.scene.image.Image IMAGE_RUBY  = new javafx.scene.image.Image("https://upload.wikimedia.org/wikipedia/commons/f/f1/Ruby_logo_64x64.png");

    @FXML
    private final javafx.scene.image.Image  IMAGE_APPLE  = new javafx.scene.image.Image("http://findicons.com/files/icons/832/social_and_web/64/apple.png");
    @FXML
    private final javafx.scene.image.Image  IMAGE_VISTA  = new javafx.scene.image.Image("http://antaki.ca/bloom/img/windows_64x64.png");
    @FXML
    private final javafx.scene.image.Image  IMAGE_TWITTER = new javafx.scene.image.Image("http://files.softicons.com/download/social-media-icons/fresh-social-media-icons-by-creative-nerds/png/64x64/twitter-bird.png");
    @FXML
    private javafx.scene.image.Image[] listOfImages = {IMAGE_RUBY, IMAGE_APPLE, IMAGE_VISTA, IMAGE_TWITTER};

    @FXML
    private AnchorPane anchorPaneDetailsEvent;

    @FXML
    private AnchorPane anchorPaneDetailsEvent1;

    @FXML
    private javafx.scene.image.Image  IMAGE_INFO  = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/images%20(6).png");
    @FXML
    private javafx.scene.image.Image  IMAGE_Participants  = new javafx.scene.image.Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/collection-clip-art-people-8.png");





    private void initClock(Timeline clock){
        clock=new Timeline(new KeyFrame(Duration.ZERO, e -> {
            dateLabel.setText(LocalDateTime.now().format(Constants.DATE_TIME_FORMATTER_NICE));
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

    private void initTimeSpinner(Spinner<LocalTime> spinner){
        SpinnerValueFactory<LocalTime> value = new SpinnerValueFactory<LocalTime>() {
            {
                setConverter(new LocalTimeStringConverter(FormatStyle.SHORT));
                setValue(LocalTime.now());
            }
            @Override
            public void decrement(int steps) {
                LocalTime time = getValue();
                setValue(time.minusMinutes(steps));
            }

            @Override
            public void increment(int steps) {
                LocalTime time =  getValue();
                setValue(time.plusMinutes(steps));
            }
        };

        spinner.setValueFactory(value);
        spinner.setEditable(false);
    }



    void initTables(){
        if(userPage.getReceivedRequestsPaged(0).size()!=0)
            imag2.setVisible(true);


        tableFriendsColumnEmail.setCellValueFactory(new PropertyValueFactory<UtilizatorPrietenieDTO,String>("email"));
        tableFriendsColumnFirstName.setCellValueFactory(new PropertyValueFactory<UtilizatorPrietenieDTO,String>("firstName"));
        tableFriendsColumnLastName.setCellValueFactory(new PropertyValueFactory<UtilizatorPrietenieDTO,String>("lastName"));
        TableColumn<UtilizatorPrietenieDTO, UtilizatorPrietenieDTO> tableColumnDelete = new TableColumn<>("Remove friend");
        tableColumnDelete.setCellValueFactory(
                param -> new ReadOnlyObjectWrapper<>(param.getValue())

        );
        tableColumnDelete.setCellFactory(param -> new TableCell<UtilizatorPrietenieDTO, UtilizatorPrietenieDTO>() {

            private final Button ButtonDelete = new Button();


            @Override
            protected void updateItem(UtilizatorPrietenieDTO user, boolean empty) {
                super.updateItem(user, empty);

                if (user==null) {

                    setGraphic(null);
                    return;

                }

                javafx.scene.image.ImageView imageView = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/recycle_bin.png");
                imageView.setPreserveRatio(true);
                ButtonDelete.setPrefWidth(110.0);
               setGraphic(ButtonDelete);


               ButtonDelete.setGraphic(imageView);


                System.out.println("ok");
                ButtonDelete.setOnAction(
                        //ev -> getTableView().getItems().remove(user)
                        ev->handleDeleteFriend(user)
                );



            }
        });

        tableColumnDelete.setPrefWidth(171);
        tableViewFriends.getColumns().add(tableColumnDelete);
        tableUsersColumnEmail.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("email"));
        tableUsersColumnFirstName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("firstName"));
        tableUsersColumnLastName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("lastName"));

        tableReceivedColumnEmail.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("email"));
        tableReceivedColumnFirstName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("firstName"));
        tableReceivedColumnLastName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("lastName"));
        



        tableSentColumnEmail.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("email"));
        tableSentColumnFirstName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("firstName"));
        tableSentColumnLastName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("lastName"));


        tableUnfollowedEventsName.setCellValueFactory(new PropertyValueFactory<Event,String>("name"));
        tableUnfollowedEventsStart.setCellValueFactory(new PropertyValueFactory<Event,LocalDateTime>("startDate"));
        tableUnfollowedEventsStart.setCellFactory(column-> new TableCell<Event, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    setText(null);
                } else {
                    setText(Constants.DATE_TIME_FORMATTER.format(item));
                }
            }
        });


        TableColumn<Event, Event> tableColumnPlus = new TableColumn<>("More info");
        tableColumnPlus.setCellValueFactory(
                param -> new ReadOnlyObjectWrapper<>(param.getValue())

        );
        tableColumnPlus.setCellFactory(param -> new TableCell<Event, Event>() {

            private final Button ButtonPlus = new Button();


            @Override
            protected void updateItem(Event event, boolean empty) {
                super.updateItem(event, empty);

                if (event==null) {

                    setGraphic(null);
                    return;

                }

                javafx.scene.image.ImageView imageView = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/pict--info-cloud-clipart-vector-stencils-library.png");
                imageView.setPreserveRatio(true);
                ButtonPlus.setPrefWidth(100);
                setGraphic(ButtonPlus);


                ButtonPlus.setGraphic(imageView);


                System.out.println("ok");
                ButtonPlus.setOnAction(

                        ev->ShowAnchorPaneRequestInfo(event)
                );



            }
        });

        tableColumnPlus.setPrefWidth(130);
       
        tableViewUnfollowedEvents.getColumns().add( tableColumnPlus);



        TableColumn<Event, Event> tableColumnFollow = new TableColumn<>("Follow");
        tableColumnFollow.setCellValueFactory(
                param -> new ReadOnlyObjectWrapper<>(param.getValue())

        );
        tableColumnFollow.setCellFactory(param -> new TableCell<Event, Event>() {

            private final Button ButtonPlus = new Button();


            @Override
            protected void updateItem(Event event, boolean empty) {
                super.updateItem(event, empty);

                if (event==null) {

                    setGraphic(null);
                    return;

                }

                javafx.scene.image.ImageView imageView = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/Following-in-the-social-media-clipart.png");
                imageView.setPreserveRatio(true);
                ButtonPlus.setPrefWidth(100);
                setGraphic(ButtonPlus);


                ButtonPlus.setGraphic(imageView);


                System.out.println("ok");
                ButtonPlus.setOnAction(

                        ev->handleFollowEvent(event)
                );



            }
        });
        tableColumnFollow.setPrefWidth(130);
        tableViewUnfollowedEvents.getColumns().add(tableColumnFollow);

        TableColumn<Event, Event> tableColumnMonth = new TableColumn<>("Month");
        tableColumnMonth.setCellValueFactory(
                param -> new ReadOnlyObjectWrapper<>(param.getValue())

        );
        tableColumnMonth.setCellFactory(param -> new TableCell<Event, Event>() {

            private final Button ButtonPlus = new Button();


            @Override
            protected void updateItem(Event event, boolean empty) {
                super.updateItem(event, empty);

                if (event==null) {

                    setGraphic(null);
                    return;

                }

              
                ButtonPlus.setPrefWidth(100);
                setGraphic(ButtonPlus);


                final javafx.scene.image.ImageView IMAGE_JAN = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/1.jpg");
                final javafx.scene.image.ImageView IMAGE_FEB  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/2.jpg");
                final javafx.scene.image.ImageView IMAGE_MAR  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/3.jpg");
                final javafx.scene.image.ImageView IMAGE_APR = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/4.jpg");
                final javafx.scene.image.ImageView IMAGE_MAY  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/5.jpg");
                final javafx.scene.image.ImageView IMAGE_JUNE  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/6.jpg");
                final javafx.scene.image.ImageView IMAGE_JULY  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/7.jpg");
                final javafx.scene.image.ImageView IMAGE_AUG = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/8.jpg");
                final javafx.scene.image.ImageView IMAGE_SEP  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/9.jpg");
                final javafx.scene.image.ImageView IMAGE_OCT  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/10.jpg");
                final javafx.scene.image.ImageView IMAGE_NOV = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/11.jpg");
                final javafx.scene.image.ImageView IMAGE_DEC  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/12.jpg");

                ButtonPlus.setPrefWidth(110);
                setGraphic(ButtonPlus);

                if(event.getStartDate().getMonthValue()==1)
                    ButtonPlus.setGraphic(IMAGE_JAN);
                if(event.getStartDate().getMonthValue()==2)
                    ButtonPlus.setGraphic(IMAGE_FEB);
                if(event.getStartDate().getMonthValue()==3)
                    ButtonPlus.setGraphic(IMAGE_MAR);
                if(event.getStartDate().getMonthValue()==4)
                    ButtonPlus.setGraphic(IMAGE_APR);
                if(event.getStartDate().getMonthValue()==5)
                    ButtonPlus.setGraphic(IMAGE_MAY);
                if(event.getStartDate().getMonthValue()==6)
                    ButtonPlus.setGraphic(IMAGE_JUNE);
                if(event.getStartDate().getMonthValue()==7)
                    ButtonPlus.setGraphic(IMAGE_JULY);
                if(event.getStartDate().getMonthValue()==8)
                    ButtonPlus.setGraphic(IMAGE_AUG);
                if(event.getStartDate().getMonthValue()==9)
                    ButtonPlus.setGraphic(IMAGE_SEP);
                if(event.getStartDate().getMonthValue()==10)
                    ButtonPlus.setGraphic(IMAGE_OCT);
                if(event.getStartDate().getMonthValue()==11)
                    ButtonPlus.setGraphic(IMAGE_NOV);
                if(event.getStartDate().getMonthValue()==11)
                    ButtonPlus.setGraphic(IMAGE_DEC);



                System.out.println("ok");
                ButtonPlus.setOnAction(

                        ev->ShowAnchorPaneRequestInfo(event)
                );



            }
        });

        tableColumnMonth.setPrefWidth(100);
        tableViewUnfollowedEvents.getColumns().add(tableColumnMonth);


        tableViewFriends.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        tableFollowedEventsName.setCellValueFactory(new PropertyValueFactory<Event,String>("name"));
        tableFollowedEventsStart.setCellValueFactory(new PropertyValueFactory<Event,LocalDateTime>("startDate"));
        tableFollowedEventsStart.setCellFactory(column-> new TableCell<Event, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    setText(null);
                } else {
                    setText(Constants.DATE_TIME_FORMATTER.format(item));
                }
            }
        });

        TableColumn<Event, Event> tableColumnPlus2 = new TableColumn<>("Learn more");
        tableColumnPlus2.setCellValueFactory(
                param -> new ReadOnlyObjectWrapper<>(param.getValue())

        );
        tableColumnPlus2.setCellFactory(param -> new TableCell<Event, Event>() {

            private final Button ButtonPlus2 = new Button();


            @Override
            protected void updateItem(Event event, boolean empty) {
                super.updateItem(event, empty);

                if (event==null) {

                    setGraphic(null);
                    return;

                }

                javafx.scene.image.ImageView imageView = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/pict--info-cloud-clipart-vector-stencils-library.png");
                imageView.setPreserveRatio(true);
                ButtonPlus2.setPrefWidth(110.0);
                setGraphic(ButtonPlus2);


                ButtonPlus2.setGraphic(imageView);


                System.out.println("ok");
                ButtonPlus2.setOnAction(

                        ev->ShowAnchorPaneRequestInfo(event)
                );



            }
        });

        tableColumnPlus2.setPrefWidth(140);
        tableViewFollowedEvents.getColumns().add(tableColumnPlus2);

        TableColumn<Event, Event> tableColumnUnfollow = new TableColumn<>("Unfollow");
        tableColumnUnfollow.setCellValueFactory(
                param -> new ReadOnlyObjectWrapper<>(param.getValue())

        );
        tableColumnUnfollow.setCellFactory(param -> new TableCell<Event, Event>() {

            private final Button ButtonPlus = new Button();


            @Override
            protected void updateItem(Event event, boolean empty) {
                super.updateItem(event, empty);

                if (event==null) {

                    setGraphic(null);
                    return;

                }

                javafx.scene.image.ImageView imageView = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/Instagram-Unfollow-Least-Engaged-1.jpg");
                imageView.setPreserveRatio(true);
                ButtonPlus.setPrefWidth(110);
                setGraphic(ButtonPlus);


                ButtonPlus.setGraphic(imageView);


                System.out.println("ok");
                ButtonPlus.setOnAction(

                        ev->handleUnfollowEvent(event)
                );



            }
        });
        tableColumnUnfollow.setPrefWidth(140);
        tableViewFollowedEvents.getColumns().add(tableColumnUnfollow);

        TableColumn<Event, Event> tableColumnMonth2 = new TableColumn<>("Month");
        tableColumnMonth2.setCellValueFactory(
                param -> new ReadOnlyObjectWrapper<>(param.getValue())

        );
        tableColumnMonth2.setCellFactory(param -> new TableCell<Event, Event>() {

            private final Button ButtonPlus = new Button();


            @Override
            protected void updateItem(Event event, boolean empty) {
                super.updateItem(event, empty);

                if (event==null) {

                    setGraphic(null);
                    return;

                }

                 final javafx.scene.image.ImageView IMAGE_JAN = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/1.jpg");
                final javafx.scene.image.ImageView IMAGE_FEB  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/2.jpg");
                final javafx.scene.image.ImageView IMAGE_MAR  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/3.jpg");
                final javafx.scene.image.ImageView IMAGE_APR = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/4.jpg");
                 final javafx.scene.image.ImageView IMAGE_MAY  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/5.jpg");
                 final javafx.scene.image.ImageView IMAGE_JUNE  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/6.jpg");
                 final javafx.scene.image.ImageView IMAGE_JULY  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/7.jpg");
                final javafx.scene.image.ImageView IMAGE_AUG = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/8.jpg");
                 final javafx.scene.image.ImageView IMAGE_SEP  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/9.jpg");
                 final javafx.scene.image.ImageView IMAGE_OCT  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/10.jpg");
                 final javafx.scene.image.ImageView IMAGE_NOV = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/11.jpg");
                  final javafx.scene.image.ImageView IMAGE_DEC  = new javafx.scene.image.ImageView("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/12.jpg");

                ButtonPlus.setPrefWidth(110);
                setGraphic(ButtonPlus);

                if(event.getStartDate().getMonthValue()==1)
                    ButtonPlus.setGraphic(IMAGE_JAN);
                if(event.getStartDate().getMonthValue()==2)
                    ButtonPlus.setGraphic(IMAGE_FEB);
                if(event.getStartDate().getMonthValue()==3)
                    ButtonPlus.setGraphic(IMAGE_MAR);
                if(event.getStartDate().getMonthValue()==4)
                    ButtonPlus.setGraphic(IMAGE_APR);
                if(event.getStartDate().getMonthValue()==5)
                    ButtonPlus.setGraphic(IMAGE_MAY);
                if(event.getStartDate().getMonthValue()==6)
                    ButtonPlus.setGraphic(IMAGE_JUNE);
                if(event.getStartDate().getMonthValue()==7)
                    ButtonPlus.setGraphic(IMAGE_JULY);
                if(event.getStartDate().getMonthValue()==8)
                    ButtonPlus.setGraphic(IMAGE_AUG);
                if(event.getStartDate().getMonthValue()==9)
                    ButtonPlus.setGraphic(IMAGE_SEP);
                if(event.getStartDate().getMonthValue()==10)
                    ButtonPlus.setGraphic(IMAGE_OCT);
                if(event.getStartDate().getMonthValue()==11)
                    ButtonPlus.setGraphic(IMAGE_NOV);
                if(event.getStartDate().getMonthValue()==11)
                    ButtonPlus.setGraphic(IMAGE_DEC);


                ButtonPlus.setOnAction(

                        ev->ShowAnchorPaneRequestInfo(event)
                );



            }
        });

        tableColumnMonth2.setPrefWidth(140);
        tableViewFollowedEvents.getColumns().add(tableColumnMonth2);


////////////////////////
        listViewUpcoming.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new EventsXCell();
            }
        });

        tabPaneTables.setVisible(true);
        buttonCreateGroupChat.setVisible(false);





    


        if(img1.isVisible()||imag2.isVisible()) {
            buttonGoTo.setVisible(true);
            imagClopotel.setVisible(true);

        }



    }


    ////////////////SCROLL HANDLERS///////////////////////////////////////////
    EventHandler<ScrollEvent> handlerScrollFriends=event -> {


        if(tableViewFriends.getItems().size()>0) {
            userPage.setPageFriendsSize(2);
            modelFriends.addAll(userPage.getUserFriendsPaged(tableViewFriends.getItems().get(tableViewFriends.getItems().size() - 1).getId()));
            List<UtilizatorPrietenieDTO> friendsList = new ArrayList<>(modelFriends);
            modelFriends.setAll(friendsList);
        }
    };

    EventHandler<ScrollEvent> handlerScrollSearchNonFriends=event-> {


        if(tableViewUsers.getItems().size()>0) {
            userPage.setPageNonFriendsSize(2);
            modelUsers.addAll(userPage.getUserNonFriendsPaged(textFieldSearchUser.getText(), tableViewUsers.getItems().get(tableViewUsers.getItems().size() - 1).getId()));
            List<Utilizator> usersList = new ArrayList<>(modelUsers);

            modelUsers.setAll(usersList);
        }
    };




    EventHandler<ScrollEvent> handlerScrollRequestsReceived=event-> {
        if(tableViewReceivedRequests.getItems().size()>0) {
            userPage.setPageUsersRequestReceived(2);
            modelReceivedRequests.addAll(userPage.getReceivedRequestsPaged(tableViewReceivedRequests.getItems().get(tableViewReceivedRequests.getItems().size() - 1).getId()));
            List<Utilizator> usersList = new ArrayList<>(modelReceivedRequests);
            modelReceivedRequests.setAll(usersList);
        }
    };

    EventHandler<ScrollEvent> handlerScrollRequestsSent=event-> {
        if(tableViewSentRequests.getItems().size()>0) {
            userPage.setPageUsersRequestSent(2);
            modelSentRequests.addAll(userPage.getSentRequestsPaged(tableViewSentRequests.getItems().get(tableViewSentRequests.getItems().size() - 1).getId()));
            List<Utilizator> usersList = new ArrayList<>(modelSentRequests);
            modelSentRequests.setAll(usersList);
        }
    };

    EventHandler<ScrollEvent> handlerScrollMessages=event->{
        if(listViewChat.getItems().size()>0) {

            if (selectedGroupChat == null) {
                userPage.setPageMessages(1);

                List<MessageDTO> messageList = new ArrayList<>(modelMessages);
                //Daca e un chat in care e implicat utilizatorul curent, atunci el e in from sau in to pt fiecare mesaj

                List<Utilizator> utilizatori_implicati1 = new ArrayList<>();
                List<Utilizator> utilizatori_implicati2 = new ArrayList<>();

                List<Message> lista_aux = new ArrayList<>();
                Utilizator u1 = serviceU.getUser(userId).get();
                utilizatori_implicati1.add(u1);
                for (UtilizatorPrietenieDTO u2 : selectedUsersToChat) {
                    lista_aux.addAll(userPage.getConversationPaged(userId, u2.getId())); //in query e pus in 2 parti (a,b) (b,a)
                    utilizatori_implicati1.add(serviceU.getUser(u2.getId()).get());

                }
                for (Message m : lista_aux) {
                    utilizatori_implicati2 = new ArrayList<Utilizator>();
                    utilizatori_implicati2.add(m.getFrom());
                    for (Utilizator destinatar : m.getTo())
                        utilizatori_implicati2.add(destinatar);

                    int ok = 1;
                    for (Utilizator a1 : utilizatori_implicati1)
                        if (!utilizatori_implicati2.contains(a1))
                            ok = 0;
                    for (Utilizator a2 : utilizatori_implicati2)
                        if (!utilizatori_implicati1.contains(a2))
                            ok = 0;
                    if (ok == 0)
                        lista_aux.remove(m);
                }
               
                List<MessageDTO> lista_finala = new ArrayList<>();
                for (Message m : lista_aux) {
                    MessageDTO msg;
                    if (m.getOriginalMessage() == 0) {

                        msg = new MessageDTO(m.getId(),
                                m.getDate().format(Constants.DATE_TIME_FORMATTER_NICE) + "\n" + userPage.findUser(m.getFrom().getId()).get().getLastName() + " " + userPage.findUser(m.getFrom().getId()).get().getFirstName() + ": " + m.getMessage() + "\n\n");
                    } else {

                        msg = new MessageDTO(m.getId(), "Replied at: " + userPage.findMessage(m.getOriginalMessage()).getMessage() + "     From: " + userPage.findUser(m.getFrom().getId()).get().getLastName() + " " + userPage.findUser(m.getFrom().getId()).get().getFirstName() + "  " +
                                m.getDate().format(Constants.DATE_TIME_FORMATTER_NICE) + "\n" + userPage.findUser(m.getFrom().getId()).get().getLastName() + " " + userPage.findUser(m.getFrom().getId()).get().getFirstName() + ": " + m.getMessage() + "\n\n");
                    }

                    lista_finala.add(msg);

                }


                messageList.addAll(lista_finala);
                modelMessages.setAll(messageList);


            }
        }
        else //altfel fac scroll in group chat
        {
            userPage.setPageMessages(1);

            List<MessageDTO> messageList = new ArrayList<>(modelMessages);

            List<Utilizator> utilizatori_implicati1 = new ArrayList<>();
            List<Utilizator> utilizatori_implicati2 = new ArrayList<>();

            List<Message> lista_aux = new ArrayList<>();
            Utilizator u1 = serviceU.getUser(userId).get();
            utilizatori_implicati1.add(u1);
            for (Utilizator u2 : userPage.findMessage(firstMessage).getTo()) {
                lista_aux.addAll(userPage.getConversationPaged(userId, u2.getId())); //in query e pus in 2 parti (a,b) (b,a)
                utilizatori_implicati1.add(u2);

            }
            utilizatori_implicati1.add(userPage.findMessage(firstMessage).getFrom());
            for (Message m : lista_aux) {
                //fac lista group chat pt acest mesaj m
                utilizatori_implicati2 = new ArrayList<Utilizator>();
                utilizatori_implicati2.add(m.getFrom());
                for (Utilizator destinatar : m.getTo())
                    utilizatori_implicati2.add(destinatar);

                //daca continutul lui utilizatori implicati1 nu este acelasi cu al lui utilizatori implicati 2, mesajul m se elimina din lista de candidati
                int ok = 1;
                for (Utilizator a1 : utilizatori_implicati1)
                    if (!utilizatori_implicati2.contains(a1))
                        ok = 0;
                for (Utilizator a2 : utilizatori_implicati2)
                    if (!utilizatori_implicati1.contains(a2))
                        ok = 0;
                if (ok == 0)
                    lista_aux.remove(m);
            }
            //acum am:
            //in lista_aux toate mesajele 
            List<MessageDTO> lista_finala = new ArrayList<>();
            for (Message m : lista_aux) {
                MessageDTO msg;
                if (m.getOriginalMessage() == firstMessage) {

                    msg = new MessageDTO(m.getId(),
                            m.getDate().format(Constants.DATE_TIME_FORMATTER_NICE) + "\n" + userPage.findUser(m.getFrom().getId()).get().getLastName() + " " + userPage.findUser(m.getFrom().getId()).get().getFirstName() + ": " + m.getMessage() + "\n\n");


                    lista_finala.add(msg);

                }

            }
            messageList.addAll(lista_finala);
            modelMessages.setAll(messageList);

        }
    };



    EventHandler<ScrollEvent> handlerScrollEvents=event->{
        if(listViewUpcoming.getItems().size()>0) {
            modelEvents.addAll(userPage.getFollowedEvents());
            List<Event> eventsList = new ArrayList<>(modelEvents);
            modelEvents.setAll(eventsList);


        }
    };

   EventHandler<ScrollEvent> handlerScrollUnfollowed=event->{
        if(tableViewUnfollowedEvents.getItems().size()>0){
            userPage.setPageUnfollowed(5);
            modelUnfollowedEvents.addAll(userPage.getUnfollowedEventsPaged(tableViewUnfollowedEvents.getItems().get(tableViewUnfollowedEvents.getItems().size()-1).getId()));
            List<Event> eventList=new ArrayList<>(modelUnfollowedEvents);
            modelUnfollowedEvents.setAll(eventList);
        }
    };






    EventHandler<ScrollEvent> handlerScrollFollowed=event->{
        if(tableViewFollowedEvents.getItems().size()>0){
            userPage.setPageFollowed(5);
            modelFollowedEvents.addAll(userPage.getFollowedEventsPaged(tableViewFollowedEvents.getItems().get(tableViewFollowedEvents.getItems().size()-1).getId()));
            List<Event> eventList=new ArrayList<>(modelFollowedEvents);
            modelFollowedEvents.setAll(eventList);
        }
    };

    public void initScrollDeploy(){
        deployLoadNonFriendsPage();
        deployLoadFriendsPage();
        deployLoadUsersSentRequests();
        deployLoadUsersReceivedRequests();
        deployLoadMessages();

        deployLoadFollowedEvents();
        deployLoadUnfollowedEvents();

        deployLoadEvents();
    }

    ////////////////SCROLL HANDLERS///////////////////////////////////////////



    public void setMultiService(UserPage userPage, MessageService serviceM, FriendRequestService serviceFR) {
        this.serviceM=serviceM;
        this.serviceC=serviceFR;
        this.userPage=userPage;
        serviceC.addObserver(this);
        userPage.addObserver(this);
        tabPane.getSelectionModel().select(1);
        tabPaneTables.setVisible(false);




        userPage.addFriendshipObserver(friendshipChangeEvent -> {
            if(friendshipChangeEvent.getType()== ChangeEventType.DELETE) {
                refreshModelFriends(tableViewFriends.getItems().size());
                refreshModelReceivedRequests(tableViewReceivedRequests.getItems().size());
            }
            else if(friendshipChangeEvent.getType()==ChangeEventType.ADD) {
                refreshModelFriends(tableViewFriends.getItems().size() + 1);
                refreshModelReceivedRequests(tableViewReceivedRequests.getItems().size()+1);
            }

            refreshModelNonFriends();
            refreshModelSentRequests();
        });

        userPage.addFriendRequestObserver(friendRequestsEvent -> {
            if(friendRequestsEvent.getType()== ChangeEventType.DELETE) {
                refreshModelFriends(tableViewFriends.getItems().size());
                refreshModelReceivedRequests(tableViewReceivedRequests.getItems().size());
            }
            else if(friendRequestsEvent.getType()==ChangeEventType.ADD) {
                refreshModelFriends(tableViewFriends.getItems().size() + 1);
                refreshModelReceivedRequests(tableViewReceivedRequests.getItems().size()+1);
            }
            refreshModelNonFriends();
            refreshModelSentRequests();
            initModelUsersReceivedRequests();
            tableViewReceivedRequests.setItems(modelReceivedRequests);


        });


        userPage.addMessageObserver(messageChangeEvent -> {
            refreshConversation();
        });
        userPage.addEventObserver(eventChangeEvent -> {
            if(eventChangeEvent.getType()==ChangeEventType.ADD) {
                refreshModelUnfollowedEvents(tableViewUnfollowedEvents.getItems().size() + 1);
                refreshModelFollowedEvents(tableViewUnfollowedEvents.getItems().size() );

                sendNotification();
            }
            else if(eventChangeEvent.getType()==ChangeEventType.UPDATE_TYPE_1){
                refreshModelFollowedEvents(tableViewFollowedEvents.getItems().size()+1);
                refreshModelUnfollowedEvents(tableViewUnfollowedEvents.getItems().size());

                sendNotification();
            }
            else if(eventChangeEvent.getType()==ChangeEventType.UPDATE_TYPE_2){
                refreshModelFollowedEvents(tableViewFollowedEvents.getItems().size());
                refreshModelUnfollowedEvents(tableViewUnfollowedEvents.getItems().size()+1);

                sendNotification();
            }
        });

        tabPaneTables.setVisible(true);
        tabPaneTables.getSelectionModel().select(0);
        initModelFriends();
        tableViewFriends.setItems(modelFriends);
        AnchorPane.setTopAnchor(tabPaneTables,0.0);
        AnchorPane.setBottomAnchor(tabPaneTables,82.0);
        initTables();
        initScrollDeploy();

        textFieldSearchUser.textProperty().addListener(x->handleFilterUsers());

        textFieldSearchEvent1.textProperty().addListener(x->handleFilterEventsUnfollowed());

        textFieldSearchEvent2.textProperty().addListener(x->handleFilterEventsFollowed());

        textFieldSearchFriend.textProperty().addListener(x->handleFilterFriends());



        initTimeSpinner(startSpinner);
        initTimeSpinner(endSpinner);
        initClock(clock);


        nameLabel.setText("Hello there, "+userPage.getUser().getFirstName()+" "+userPage.getUser().getLastName()+"!");
        anchorInfo.setVisible(true);

        sendNotification();


     

        Message m=userPage.getLastMessageConversation(userPage.getUser().getId());
        if(last_message==null && m!=null && m.getFrom()!=userPage.getUser())
            img1.setVisible(true);
        last_message=m;

        buttonParticipantsEv.setStyle("-fx-background-color: #36393FFF; ");
        buttonInfoEv.setStyle("-fx-background-color: #36393FFF; ");

        anchorPaneRequestInfo.setVisible(false);

        if(!img1.isVisible() && !imag2.isVisible())
        {
            imagClopotel.setVisible(false);
            buttonGoTo.setVisible(false);
        }


    }


    //------------------------------------------INIT MODELS--------------------------------------------//
    public void goToNotification()
    {
        if(imag2.isVisible())
        {
            imagClopotel.setVisible(false);
            buttonGoTo.setVisible(false);
          
            listViewUpcoming.setVisible(false);

            tabPane.getSelectionModel().select(3);
            imag2.setVisible(false);
            buttonRequestCancel.setVisible(false);
            buttonRequestAccept.setVisible(true);
            buttonRequestReject.setVisible(true);
            AnchorPane.setBottomAnchor(tabPaneTables,155.0);
            AnchorPane.setTopAnchor(tabPaneTables,0.0);
            tabPaneTables.getSelectionModel().select(2);
            tabPaneTables.setVisible(true);
            buttonChangeRequests.setText("Switch to sent requests");

            initModelUsersReceivedRequests();
            tableViewReceivedRequests.setItems(modelReceivedRequests);
            anchorInfo.setVisible(true);
            imgEvents.setVisible(false);
            listViewUpcoming.setVisible(false);


        }
        if(img1.isVisible())
        {
            imagClopotel.setVisible(false);
            buttonGoTo.setVisible(false);
            img1.setVisible(false);

            tableViewFriends.setItems(modelFriends);

            label_remove.setText("Send a message to a friend of yours or go to group chats!");


            refreshModelFriends(modelFriends.size()+1);
            tabPaneTables.setVisible(true);
            tabPaneTables.getSelectionModel().select(0);
            tableViewFriends.setItems(modelFriends);
            AnchorPane.setTopAnchor(tabPaneTables,0.0);
            AnchorPane.setBottomAnchor(tabPaneTables,0.0);
            tabPane.getSelectionModel().select(4);
            anchorInfo.setVisible(false);



        }
        imagClopotel.setVisible(false);
        buttonGoTo.setVisible(false);
    }
    private void initModelFriends() {
        userPage.setPageFriendsSize(2);
        modelFriends.clear();
        modelFriends.setAll(userPage.getUserFriendsPaged(0));

    }

    private void refreshModelFriends(int numberOfLoadings){
        int lastValue=userPage.getPageFriendsSize();
        userPage.setPageFriendsSize(numberOfLoadings);
        modelFriends.setAll(userPage.getUserFriendsPaged(0));
        userPage.setPageFriendsSize(lastValue);
    }

    private void deployLoadFriendsPage(){
        tableViewFriends.addEventFilter(ScrollEvent.ANY,handlerScrollFriends);
        tableViewFriends.addEventHandler(ScrollEvent.ANY,handlerScrollFriends);


    }


    private void initModelNonFriends() {
        userPage.setPageNonFriendsSize(5);
    }



    private void refreshModelNonFriends(){
        int lastValue=userPage.getPageNonFriendsSize();
        userPage.setPageNonFriendsSize(tableViewUsers.getItems().size());
        modelUsers.setAll(userPage.getUserNonFriendsPaged(textFieldSearchUser.getText(),modelUsers.get(modelUsers.size()).getId()));
        userPage.setPageNonFriendsSize(lastValue);
    }
    private void deployLoadNonFriendsPage(){
        tableViewUsers.addEventFilter(ScrollEvent.ANY,handlerScrollSearchNonFriends);
    }



    private void initModelUsersReceivedRequests(){
        userPage.setPageUsersRequestReceived(6);
        modelReceivedRequests.setAll(userPage.getReceivedRequestsPaged(0));

    }
    public void refreshModelReceivedRequests(int numberOfLoadings){
        int lastValue=userPage.getPageUsersRequestReceivedSize();
        userPage.setPageUsersRequestReceived(numberOfLoadings);
        modelReceivedRequests.setAll(userPage.getReceivedRequestsPaged(0));
        userPage.setPageUsersRequestReceived(lastValue);

    }



    private void deployLoadUsersReceivedRequests() {
        tableViewReceivedRequests.addEventFilter(ScrollEvent.ANY, handlerScrollRequestsReceived);


    }


    private void initModelUsersSentRequests(){
        userPage.setPageUsersRequestSent(16);
        modelSentRequests.setAll(userPage.getSentRequestsPaged(0));
    }

    private void refreshModelSentRequests(){
        int lastValue=userPage.getPageUsersRequestSentSize();
        userPage.setPageUsersRequestSent(tableViewSentRequests.getItems().size());
        modelSentRequests.setAll(userPage.getSentRequestsPaged(0));
        userPage.setPageUsersRequestSent(lastValue);
    }

    private void deployLoadUsersSentRequests() {
        tableViewSentRequests.addEventFilter(ScrollEvent.ANY, handlerScrollRequestsSent);
    }



    private void initModelChat(){
   

        userPage.setPageMessages(10);
        modelMessages.clear();
        List<Message> messages=userPage.getConversationPaged(selectedUsersToChat.get(0).getId(),Long.MAX_VALUE);
     

        for(Message mess:messages)
            if(mess.getTo().size()==1)
            {
                MessageDTO msg;
                if(mess.getOriginalMessage()==0){

                    msg=new MessageDTO(mess.getId(),
                            mess.getDate().format(Constants.DATE_TIME_FORMATTER_NICE)+ "\n"+ userPage.findUser(mess.getFrom().getId()).get().getLastName()+" "+userPage.findUser(mess.getFrom().getId()).get().getFirstName()+": "+mess.getMessage() +"\n\n");}

                else{

                    msg=new MessageDTO(mess.getId(),"Replied at: "+ userPage.findMessage(mess.getOriginalMessage()).getMessage()+ "     From: "+ userPage.findUser(mess.getFrom().getId()).get().getLastName()+ " "+userPage.findUser(mess.getFrom().getId()).get().getFirstName() + "  "+
                            mess.getDate().format(Constants.DATE_TIME_FORMATTER_NICE)+ "\n"+ userPage.findUser(mess.getFrom().getId()).get().getLastName()+" "+userPage.findUser(mess.getFrom().getId()).get().getFirstName()+": "+mess.getMessage() +"\n\n");}


                modelMessages.add(0,msg);
            }


            listViewChat.scrollTo(modelMessages.size());
    }





   
    //chat intre mai multi
    private void initModelChat3G(Long firstMessage) {
        List<Long> allUsersInChat2 = new ArrayList<>();

        for (Utilizator u : userPage.findMessage(firstMessage).getTo())
            allUsersInChat2.add(userPage.findUser(u.getId()).get().getId());

        Message m=userPage.findMessage(firstMessage);
        allUsersInChat2.add(m.getFrom().getId());

        userPage.setPageMessages(10);


        modelMessages.clear();
        List<Message> messages = userPage.getGroupConversationPaged(allUsersInChat2, Long.MAX_VALUE, firstMessage);
        if (messages.size() != 0) {

            for (Message mess : messages)
                if (mess.getTo().size() != 1) {
                    MessageDTO msg;
                    if (mess.getOriginalMessage() == firstMessage) {

                        msg = new MessageDTO(mess.getId(),
                                mess.getDate().format(Constants.DATE_TIME_FORMATTER_NICE) + "\n" + userPage.findUser(mess.getFrom().getId()).get().getLastName() + " " + userPage.findUser(mess.getFrom().getId()).get().getFirstName() + ": " + mess.getMessage() + "\n\n");

                        modelMessages.add(0, msg);
                    }
                }


            listViewChat.scrollTo(modelMessages.size());

        }
    }




    private void deployLoadMessages(){
        listViewChat.addEventFilter(ScrollEvent.ANY,handlerScrollMessages);
    }

    private void deployLoadEvents()
    {
        listViewUpcoming.addEventFilter(ScrollEvent.ANY, handlerScrollEvents);


    }

    private void refreshConversation(){
        img1.setVisible(false);
        for(UtilizatorPrietenieDTO other: userPage.getUserFriends())
        {
            Message m = userPage.getLastMessageConversation(other.getId());
            if (m != last_message && m!=null && m.getFrom() != userPage.getUser()) {
                img1.setVisible(true);
                last_message = m;
            }



        }
      
        selectedUsersToChat=tableViewFriends.getSelectionModel().getSelectedItems();

        if(selectedGroupChat!=null && listViewChatG.getSelectionModel().getSelectedItems().size()!=0)
        selectedGroupChat=listViewChatG.getSelectionModel().getSelectedItems().get(0);


        if( selectedUsersToChat!=null && selectedUsersToChat.size()==1) {

            selectedGroupChat=null;
            listViewChatG.getSelectionModel().getSelectedItems().clear();


            initModelChat();
            //chat A <-> B
        }

        else
        if(selectedGroupChat!=null && listViewChatG.getSelectionModel().getSelectedItems().size()!=0)
        {

            tableViewFriends.getSelectionModel().getSelectedItems().clear();

            firstMessage=selectedGroupChat.getId();
            initModelChat3G(selectedGroupChat.getId());

        }





    }

    @FXML
    private void handleCreateGroupChat()
    {
        selectedUsersToChat=tableViewFriends.getSelectionModel().getSelectedItems();

        List<Utilizator> utilizatori_implicati=new ArrayList<>();
        for(UtilizatorPrietenieDTO u: selectedUsersToChat)
            utilizatori_implicati.add(userPage.findUser(u.getId()).get());
        Message m= new Message(userPage.getUser(), LocalDateTime.now(),utilizatori_implicati,"");
        userPage.sendMessage(m);


        List<MessageDTO> messageList = new ArrayList<>(modelMessagesG);

        messageList.add(0,new MessageDTO(m.getId(),"mesaj tabel"));
        System.out.println("aici e aux: "+messageList);
        modelMessagesG.clear();
        modelMessagesG.setAll(messageList);
        System.out.println("aici e modelG"+ modelMessagesG);
        listViewChatG.setItems(modelMessagesG);
    }


    private void initModelUnfollowedEvents(){
        userPage.setPageUnfollowed(5);
        modelUnfollowedEvents.setAll(userPage.getUnfollowedEventsPaged(0));
    }



    private void refreshModelUnfollowedEvents(int numberOfLoadings){
        int lastValue=userPage.getPageUnfollowed();
        userPage.setPageUnfollowed(numberOfLoadings);

        modelUnfollowedEvents.setAll(userPage.getUnfollowedEventsPaged(0));
        userPage.setPageUnfollowed(lastValue);
    }


    private void deployLoadUnfollowedEvents(){
        tableViewUnfollowedEvents.addEventFilter(ScrollEvent.ANY,handlerScrollUnfollowed);
    }





    private void initModelFollowedEvents(){
        userPage.setPageFollowed(4);
        modelFollowedEvents.setAll(userPage.getFollowedEventsPaged(0));


    }

    private void refreshModelFollowedEvents(int numberOfLoadings){
        int lastValue=userPage.getPageFollowed();
        userPage.setPageFollowed(numberOfLoadings);
        modelFollowedEvents.setAll(userPage.getFollowedEventsPaged(0));
        userPage.setPageFollowed(lastValue);
    }

    private void deployLoadFollowedEvents(){
        tableViewFollowedEvents.addEventFilter(ScrollEvent.ANY,handlerScrollFollowed);
    }

    //------------------------------------------INIT MODELS--------------------------------------------//
    @FXML
    void handleLogout(ActionEvent event) {
        stage.close();
    }

    @FXML
    void handleShowFriends(ActionEvent event) {

        buttonCreateGroupChat.setVisible(false);

        imagClopotel.setVisible(false);
        buttonGoTo.setVisible(false);
        tabPaneTables.setVisible(true);
        imgEvents.setVisible(true);
        tableViewFriends.getColumns().get(3).setVisible(true);

        tabPaneTables.getSelectionModel().select(0);
        initModelFriends();
        
        tableViewFriends.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE);
        tableViewFriends.setItems(modelFriends);
        tabPane.getSelectionModel().select(1);
        AnchorPane.setTopAnchor(tabPaneTables,0.0);
        AnchorPane.setBottomAnchor(tabPaneTables,82.0);
        anchorInfo.setVisible(true);
        imgEvents.setVisible(true);
        listViewUpcoming.setVisible(true);


    }

    @FXML
    void handleShowUsers(ActionEvent event) {
        buttonCreateGroupChat.setVisible(false);

        initModelNonFriends();
        imagClopotel.setVisible(false);
        buttonGoTo.setVisible(false);
        tableViewUsers.setItems(modelUsers);
        modelUsers.clear();
        anchorInfo.setVisible(true);
        AnchorPane.setBottomAnchor(tabPaneTables,0.0);
        AnchorPane.setTopAnchor(tabPaneTables,36.0);
        tabPaneTables.getSelectionModel().select(1);
        tabPaneTables.setVisible(true);
        buttonAddFriendAction.setVisible(false);
        textFieldSearchUser.clear();
        textFieldSearchUser.setVisible(true);
        tabPane.getSelectionModel().select(2);
        listViewUpcoming.setVisible(false);
        imgEvents.setVisible(false);



    }

    @FXML
    void handleDeleteFriend(UtilizatorPrietenieDTO deletedFriend){
        if(deletedFriend!=null) {
            userPage.removeFriend(deletedFriend.getId());
            MessageAlert.showMessage(stage, Alert.AlertType.INFORMATION, "Friend Deleted", "You are no longer friend with " + deletedFriend.getFirstName() + " " + deletedFriend.getLastName() + "!");
        }
        else
            MessageAlert.showErrorMessage(stage,"Please select a friend");

    }

    @FXML
    void handleFilterUsers() {
        userPage.setPageNonFriendsSize(17);

        if(textFieldSearchUser.getText().isEmpty()) {
            modelUsers.clear();
            AnchorPane.setBottomAnchor(tabPaneTables,0.0);
            buttonAddFriendAction.setVisible(false);
        }
        else {
            List<Utilizator> usersList=userPage.getUserNonFriendsPaged(textFieldSearchUser.getText(),0);
            modelUsers.setAll(usersList);

            if(modelUsers.size()!=0){
                buttonAddFriendAction.setVisible(true);
                AnchorPane.setBottomAnchor(tabPaneTables,82.0);
            }
            else {
                buttonAddFriendAction.setVisible(false);
                AnchorPane.setBottomAnchor(tabPaneTables,0.0);
            }
        }

    }


    @FXML
    void handleFilterFriends() {
        userPage.setPageFriendsSize(20);


        if (!textFieldSearchFriend.getText().isEmpty()) {

            List<UtilizatorPrietenieDTO> prieteniList = userPage.getUserFriendsPagedFilteres( 0, textFieldSearchFriend.getText());
            modelFriends.clear();
            modelFriends.setAll(prieteniList);

        }

    }


    @FXML
    void handleFilterEventsUnfollowed() {
        userPage.setPageUnfollowed(15);

        if(!textFieldSearchEvent1.getText().isEmpty()) {

            List<Event> eventsList=userPage.getEventsUnfollowedPagedFiltered(textFieldSearchEvent1.getText(),0);
            modelUnfollowedEvents.clear();
            modelUnfollowedEvents.setAll(eventsList);


        }

    }


    @FXML
    void handleFilterEventsFollowed() {
        userPage.setPageFollowed(15);

        if(!textFieldSearchEvent2.getText().isEmpty()) {

            List<Event> eventsList=userPage.getEventsFollowedPagedFiltered(textFieldSearchEvent2.getText(),0);
            modelFollowedEvents.clear();
            modelFollowedEvents.setAll(eventsList);


        }

    }

  

    @FXML
    void handleAddFriend(ActionEvent event){
        Utilizator selectedUser = tableViewUsers.getSelectionModel().getSelectedItem();
        textFieldSearchUser.setText("");
        if(selectedUser!=null) {
            try {
                userPage.sendFriendRequest(selectedUser.getId());
                refreshModelReceivedRequests(modelReceivedRequests.size()+1);
                refreshModelFriends(modelSentRequests.size()+1);

                MessageAlert.showMessage(stage, Alert.AlertType.INFORMATION,"Request sent","The request was sent");
            } catch (ServiceException e) {
                MessageAlert.showErrorMessage(stage, e.getMessage());
            }
        }
        else
            MessageAlert.showErrorMessage(stage,"Please select a user");
    }

    @FXML
    void handleShowRequests(ActionEvent event){

        buttonCreateGroupChat.setVisible(false);

        imagClopotel.setVisible(false);
        imagClopotel.setVisible(false);
        //buttonGoTo.setVisible(false);
        listViewUpcoming.setVisible(false);

        tabPane.getSelectionModel().select(3);
        imag2.setVisible(false);
        buttonRequestCancel.setVisible(false);
        buttonRequestAccept.setVisible(true);
        buttonRequestReject.setVisible(true);
        AnchorPane.setBottomAnchor(tabPaneTables,155.0);
        AnchorPane.setTopAnchor(tabPaneTables,0.0);
        tabPaneTables.getSelectionModel().select(2);
        tabPaneTables.setVisible(true);
        buttonChangeRequests.setText("Switch to sent requests");

        initModelUsersReceivedRequests();
        tableViewReceivedRequests.setItems(modelReceivedRequests);
        anchorInfo.setVisible(true);
        imgEvents.setVisible(false);
        listViewUpcoming.setVisible(false);

    }

    @FXML
    void handleChangeRequests(ActionEvent event){

        if(buttonChangeRequests.getText().equals("Switch to sent requests")) {
            buttonRequestAccept.setVisible(false);
            buttonRequestReject.setVisible(false);
            buttonRequestCancel.setVisible(true);
            tabPaneTables.getSelectionModel().select(3);
            tabPaneTables.setVisible(true);
            initModelUsersSentRequests();
            tableViewSentRequests.setItems(modelSentRequests);
            buttonChangeRequests.setText("Switch to received requests");
        }
        else{
            buttonRequestAccept.setVisible(true);
            buttonRequestReject.setVisible(true);
            buttonRequestCancel.setVisible(false);
            tabPaneTables.getSelectionModel().select(2);
            tabPaneTables.setVisible(true);
            initModelUsersReceivedRequests();
            tableViewReceivedRequests.setItems(modelReceivedRequests);
            buttonChangeRequests.setText("Switch to sent requests");
        }
    }

    @FXML
    void handleAcceptRequest(ActionEvent event){
        Utilizator selectedUser = tableViewReceivedRequests.getSelectionModel().getSelectedItem();
        if(selectedUser!=null) {
            try {
                userPage.acceptRequest(selectedUser.getId());
                refreshModelReceivedRequests(tableViewReceivedRequests.getItems().size()+1);
                MessageAlert.showMessage(stage, Alert.AlertType.INFORMATION,"Request Accepted","You are now friends with "+selectedUser.getFirstName()+" "+selectedUser.getLastName()+"!");
                refreshModelFriends(modelFriends.size()+1);
            } catch (ServiceException e) {
                MessageAlert.showErrorMessage(stage, e.getMessage());
            }
        }
        else
            MessageAlert.showErrorMessage(stage,"Please select a user");

    }

    @FXML
    void handleRejectRequest(ActionEvent event){
        Utilizator selectedUser = tableViewReceivedRequests.getSelectionModel().getSelectedItem();
        if(selectedUser!=null) {
            try {
                userPage.rejectRequest(selectedUser.getId());
                refreshModelReceivedRequests(tableViewReceivedRequests.getItems().size()+1);

                MessageAlert.showMessage(stage, Alert.AlertType.INFORMATION,"Request Rejected","You rejected the friend request of "+selectedUser.getFirstName()+" "+selectedUser.getLastName()+"!");
            } catch (ServiceException e) {
                MessageAlert.showErrorMessage(stage, e.getMessage());
            }
        }
        else
            MessageAlert.showErrorMessage(stage,"Please select a user");


    }

    @FXML
    void handleCancelRequest(ActionEvent event){
        Utilizator selectedUser = tableViewSentRequests.getSelectionModel().getSelectedItem();
        if(selectedUser!=null) {
            try {
                userPage.removeRequest(selectedUser.getId());
                refreshModelSentRequests();
                MessageAlert.showMessage(stage, Alert.AlertType.INFORMATION,"Request Canceled","You canceled the friend request sent to "+selectedUser.getFirstName()+" "+selectedUser.getLastName()+"!");
            } catch (ServiceException e) {
                MessageAlert.showErrorMessage(stage, e.getMessage());
            }
        }
        else
            MessageAlert.showErrorMessage(stage,"Please select a user");

    }

    @FXML
    void handleShowFriendsChat(ActionEvent event){

        buttonCreateGroupChat.setVisible(true);

        imagClopotel.setVisible(false);
        buttonGoTo.setVisible(false);
        img1.setVisible(false);

        tableViewFriends.setItems(modelFriends);

        label_remove.setText("Who you want to write to?");


        refreshModelFriends(modelFriends.size()+1);
        tabPaneTables.setVisible(true);
        tabPaneTables.getSelectionModel().select(0);
        tableViewFriends.setItems(modelFriends);

        List<MessageDTO> messageList = new ArrayList<>(modelMessagesG);


        listViewChatG.setItems(modelMessagesG);
        AnchorPane.setTopAnchor(tabPaneTables,0.0);
        AnchorPane.setBottomAnchor(tabPaneTables,0.0);
        tabPane.getSelectionModel().select(4);
        anchorInfo.setVisible(false);

    }

    @FXML
    void handleDisplayChat(MouseEvent event) {


        if(tabPane.getSelectionModel().getSelectedIndex()==4) {


            img1.setVisible(false);
            listViewChat.getItems().clear();

            List<UtilizatorPrietenieDTO> newSelectedUserToChat = tableViewFriends.getSelectionModel().getSelectedItems();
            selectedUsersToChat = newSelectedUserToChat;

            if(listViewChatG.getSelectionModel().getSelectedItems()!=null && listViewChatG.getSelectionModel().getSelectedItems().size()!=0)
            {   
                MessageDTO newSelectedGroupChat=listViewChatG.getSelectionModel().getSelectedItems().get(0);
            selectedGroupChat=newSelectedGroupChat;}

           


            if(selectedUsersToChat.size()==1)
            {
                  label_chat.setText("Chat");

            }
            else
                label_chat.setText("Group chat");

            if (selectedUsersToChat.size() == 1) {
                initModelChat();
                listViewChatG.getSelectionModel().getSelectedItems().clear();
            }
            else
            {
                if(selectedGroupChat!=null)
                initModelChat3G(selectedGroupChat.getId());

            }
            buttonSendMessage.setText("Send");
            listViewChat.setItems(modelMessages); }


    }




    @FXML
    void handleChangeButton(MouseEvent event) {
        if (listViewChat.getSelectionModel().getSelectedItem()==null) {
            buttonSendMessage.setText("Send");
        } else {
            buttonSendMessage.setText("Reply");
        }

        if(selectedUsersToChat!=null && selectedUsersToChat.size()<=1)
           label_chat.setText("Chat");
        else label_chat.setText("Group chat");
    }


  
    @FXML
    void handleSendMessage(ActionEvent event) {



        MessageDTO sel = listViewChat.getSelectionModel().getSelectedItem();


        if (sel == null){ //send simplu


            try {

                List<Utilizator> destinatari=new ArrayList<>();
                for(UtilizatorPrietenieDTO u:selectedUsersToChat)
                    destinatari.add(userPage.findUser(u.getId()).get());
                Message msg = new Message(userPage.getUser(), LocalDateTime.now(),destinatari, textFieldTypeMessage.getText());
                msg.setOriginalMessage(null);

                serviceM.addMessage(msg);


            }

            catch (ServiceException e) {

                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Trimitere esuata!", "Nu puteti trimite acest mesaj!");

            }


        }

        //reply 1 pers

        else if(selectedUsersToChat.size()==1) {
            Message selected=userPage.findMessage(sel.getId());

            try {
                System.out.println("aici e exceptie null ");
                System.out.println(selected.getId());
                System.out.println(userPage.getUser().getEmail());
                System.out.println(textFieldTypeMessage.getText());

                if(userPage!=null)
                    System.out.println("da");
                userPage.replyMessage(selected.getId(), userPage.getUser().getEmail(), textFieldTypeMessage.getText());


            } catch (ServiceException e) {

                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"Trimitere esuata!","Nu puteti raspunde la acest mesaj!");

            }

        }


        else { //reply group
            Message selected=userPage.findMessage(sel.getId());

            try {

                userPage.replyToAllMessage(selected.getId(), userPage.getUser().getEmail(), textFieldTypeMessage.getText());

            } catch (ServiceException e) {

                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Trimitere esuata!", "Nu puteti raspunde la acest mesaj!");

            }

        }


        textFieldTypeMessage.setText("");

        buttonSendMessage.setText("Send");

        refreshConversation();
        img1.setVisible(false);
        

    }








    @FXML
    void handleShowReports(ActionEvent event){
     
        buttonCreateGroupChat.setVisible(false);

        imagClopotel.setVisible(false);
        buttonGoTo.setVisible(false);
        tabPane.getSelectionModel().select(5);
        textFieldFileOutput.setEditable(false);
        label_remove.setText("Here are your friends!");
      
        tabPaneTables.getSelectionModel().select(0);
        textFieldPDFName.clear();
        textFieldFileOutput.clear();
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "Display all your friends created and your received messages from a calendar period",
                        "Display all your received messages sent by a friend from a calendar period"
                );
        AnchorPane.setTopAnchor(tabPaneTables,0.0);
        AnchorPane.setBottomAnchor(tabPaneTables,0.0);
        tableViewFriends.setItems(modelFriends);
        comboBoxReport.setItems(options);
        tabPaneTables.setVisible(false);
        listViewReport.setVisible(false);
        comboBoxReport.getSelectionModel().clearSelection();
        anchorSaveFile.setVisible(false);
        buttonSaveFile.setVisible(false);
        anchorInfo.setVisible(false);

    }

    @FXML
    void handleGenerateReport(ActionEvent event) {
        LocalDate date1=datePicker1.getValue();
        LocalDate date2=datePicker2.getValue();
        listViewReport.getItems().clear();
        String errors="";
        if(comboBoxReport.getSelectionModel().isEmpty())
            errors+="Please select a report type\n";
        if(date1==null || date2==null)
            errors+="Please select both calendar periods";

        if(!errors.isEmpty())
            MessageAlert.showErrorMessage(stage,errors);
        else
        {

            if(comboBoxReport.getSelectionModel().getSelectedIndex()==0) {
                anchorSaveFile.setVisible(true);
                listViewReport.setVisible(true);
                listViewReport.getItems().add("The list of users below represents all the users you have become friends with between\n " + date1 + " and " + date2 + ":\n");
                boolean exists = false;
                for (UtilizatorPrietenieDTO u : userPage.getUserFriendBetweenDates(date1, date2)) {
                    listViewReport.getItems().add("         " + u + "\n");
                    exists = true;
                }
                if (!exists)
                    listViewReport.getItems().add("         There are no new friends created in this period.");

                listViewReport.getItems().add("Below are all the messages received between "+date1+" and "+date2+":\n");
                exists=false;
                for(UtilizatorPrietenieDTO u:userPage.getUserFriends()) {
                    List<Message> messages=userPage.getConversationBetweenDates(u.getId(),date1,date2);
                    if(messages.size()!=0 && messages.get(0)!=null) {

                        exists=true;
                        listViewReport.getItems().add("\nMessages received from " + u.getFirstName() + " " + u.getLastName() + ":");
                        for (Message m : messages) {
                            if(m!=null){
                                String text = "";
                                if (m.getOriginalMessage()==0 || m.getOriginalMessage()==null) {
                                    text = m.getDate().format(Constants.DATE_TIME_FORMATTER_NICE) +
                                            "\n         " + m.getFrom().getFirstName() + " " + m.getFrom().getLastName() + ": " + m.getMessage() + "\n\n";
                                } else {
                                    text = "    Replied at: " +  userPage.findMessage(m.getOriginalMessage()).getMessage() + "     From: " +  serviceM.getMessage(m.getOriginalMessage()).getFrom().getLastName() + " " + serviceM.getMessage(m.getOriginalMessage()).getFrom().getFirstName() + "\n         " +
                                            m.getDate().format(Constants.DATE_TIME_FORMATTER_NICE) +
                                            "\n         " + m.getFrom().getFirstName() + " " + m.getFrom().getLastName() + ": " + m.getMessage() + "\n\n";
                                }
                                listViewReport.getItems().add("         " + text);
                            }}
                    }
                }
                if(!exists)
                    listViewReport.getItems().add("         There are no new received messages in this period.");

            }
            else if(comboBoxReport.getSelectionModel().getSelectedIndex()==1){
                if(tableViewFriends.getSelectionModel().getSelectedItem()==null)
                    MessageAlert.showErrorMessage(stage,"Please select a friend from the table");
                else {
                    anchorSaveFile.setVisible(true);
                    listViewReport.setVisible(true);
                    UtilizatorPrietenieDTO otherUser = tableViewFriends.getSelectionModel().getSelectedItem();
                    listViewReport.getItems().add("Below are all the messages received from " + otherUser.getFirstName() + " " + otherUser.getLastName() + " between\n" + date1 + " and " + date2 + ":\n");
                    boolean exists = false;

                    List<Message> messages = userPage.getConversationBetweenDates(otherUser.getId(), date1, date2);
                    System.out.println(messages + "acestea");
                    if (messages.size()!=0 && messages.get(0)!=null) {
                        exists = true;
                        listViewReport.getItems().add("\nMessages received from " + otherUser.getFirstName() + " " + otherUser.getLastName() + ":");
                        for (Message m : messages) {
                            if(m!=null)
                            {String text = "";
                                if (m.getOriginalMessage()== 0 && m!=null) {
                                    text = m.getDate().format(Constants.DATE_TIME_FORMATTER_NICE) +
                                            "\n         " + m.getFrom().getFirstName() + " " + m.getFrom().getLastName() + ": " + m.getMessage() + "\n\n";
                                } else {
                                    text = "    Replied at: " + userPage.findMessage(m.getOriginalMessage()).getMessage() + "     From: " +  userPage.findMessage(m.getOriginalMessage()).getFrom().getLastName() + " " +  userPage.findMessage(m.getOriginalMessage()).getFrom().getFirstName() + "\n         " +
                                            m.getDate().format(Constants.DATE_TIME_FORMATTER_NICE) +
                                            "\n         " + m.getFrom().getFirstName() + " " + m.getFrom().getLastName() + ": " + m.getMessage() + "\n\n";
                                }
                                listViewReport.getItems().add("         " + text);
                            }}
                    }
                    if (!exists)
                        listViewReport.getItems().add("         There are no new received messages in this period.");
                }

            }
        }
    }

    @FXML
    void handleSaveFile(ActionEvent event){
        LocalDate date1=datePicker1.getValue();
        LocalDate date2=datePicker2.getValue();
        PDFGenerator documentPDF=new PDFGenerator(userPage);
        try {
            //Creez un document si o noua instanta de scris in acesta
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(textFieldFileOutput.getText()));

            //Deschid documentul si adaug prefata cu detalii despre generarea raportului
            document.open();
            documentPDF.addMetaData(document);
            documentPDF.addTitlePage(document, comboBoxReport.getSelectionModel().getSelectedItem());

            //primul tip de raport
            if(comboBoxReport.getSelectionModel().getSelectedIndex()==0)
            {
                documentPDF.addFriendships(document, userPage.getUser().getId(), date1, date2);
                documentPDF.addMessages(document, userPage.getUser().getId(), date1, date2);
                document.close();
            }
            else{
                //al doilea tip de raport
                documentPDF.addMessagesOneUser(document, userPage.getUser().getId(),tableViewFriends.getSelectionModel().getSelectedItem().getId(), date1, date2);
                document.close();
            }

            //dupa ce se generaza PDF, primim si optiunea sa il deschida direct ca sa nu il mai cautam dupa path
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Want to open the created PDF?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
            alert.initOwner(stage);
            alert.showAndWait();
            if (alert.getResult() == ButtonType.YES) {
                if (Desktop.isDesktopSupported()) {
                    File myFile = new File(textFieldFileOutput.getText());
                    Desktop.getDesktop().open(myFile);
                }
            }
            textFieldPDFName.clear();
            textFieldFileOutput.clear();
            buttonSaveFile.setVisible(false);
            anchorSaveFile.setVisible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @FXML
    void handleBrowse(ActionEvent event){
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(stage);

        if(selectedDirectory!=null) {
            if (textFieldPDFName.getText().isEmpty())
                textFieldFileOutput.setText(selectedDirectory.getAbsolutePath() + "\\Report.pdf");
            else
                textFieldFileOutput.setText(selectedDirectory.getAbsolutePath() + "\\" + textFieldPDFName.getText() + ".pdf");
        }
        if(!textFieldFileOutput.getText().isEmpty())
            buttonSaveFile.setVisible(true);


    }

    @FXML
    void handleSwitchReport(ActionEvent event){

        int choice=comboBoxReport.getSelectionModel().getSelectedIndex();

        if(choice==0)
            tabPaneTables.setVisible(false);
        else if(choice==1)
            tabPaneTables.setVisible(true);


        textFieldPDFName.clear();
        textFieldFileOutput.clear();
        buttonSaveFile.setVisible(false);
        anchorSaveFile.setVisible(false);
        listViewReport.getItems().clear();
        listViewReport.setVisible(false);
        datePicker1.getEditor().clear();
        datePicker2.getEditor().clear();
    }


    @FXML
    void handleShowEvents(ActionEvent event){

        buttonCreateGroupChat.setVisible(false);
        imagClopotel.setVisible(false);
        buttonGoTo.setVisible(false);
        initModelUnfollowedEvents();
        tableViewUnfollowedEvents.setItems(modelUnfollowedEvents);
        anchorPaneDetailsEvent.setVisible(false);
        anchorPaneDetailsEvent1.setVisible(false);

        anchorPaneRequestInfo.setVisible(false);
        buttonChangeEvents.setVisible(true);
        buttonCreateEv.setVisible(false);

        textFieldEventName.setVisible(false);
        textAreaEventDescription.setVisible(false);
        datePickerEventStart.setVisible(false);
        datePickerEventEnd.setVisible(false);
        startSpinner.setVisible(false);
        label_startDate.setVisible(false);
        label_endDate.setVisible(false);
        endSpinner.setVisible(false);
        label_eventTitle.setVisible(false);
        label_Description.setVisible(false);

        imageCalendar.setVisible(false);
        tableViewUnfollowedEvents.setItems(modelUnfollowedEvents);
        tabPane.getSelectionModel().select(6);
        tabPaneTables.getSelectionModel().select(5);
        tabPaneTables.setVisible(true);
        AnchorPane.setBottomAnchor(tabPaneTables,155.0);
        AnchorPane.setTopAnchor(tabPaneTables,0.0);

      

        tabPaneTables.setVisible(true);
        buttonChangeEvents.setText("Switch to followed events");
        listViewEvent.setVisible(false);
        anchorInfo.setVisible(false);
        listViewEvent.setVisible(false);
        anchorAddEvent.setVisible(true);
        anchorPaneRequestInfo.setVisible(false);



    }







    @FXML
    void handleAddEvent(ActionEvent event){
        String errors="";
        if(datePickerEventStart.getValue()==null || startSpinner.getValue()==null){
            errors+="Please select a starting date and hour for the event!\n";
        }
        if(datePickerEventEnd.getValue()==null || endSpinner.getValue()==null){
            errors+="Please select an ending date and hour for the event!\n";
        }
        if(textFieldEventName.getText().trim().length()==0){
            errors+="Please enter a title for the event!\n";
        }
        if(textAreaEventDescription.getText().trim().length()==0){
            errors+="Please enter a description for the event!\n";
        }
        if(errors.length()>0){
            MessageAlert.showErrorMessage(stage,errors);
            textFieldEventName.setText("");
            textAreaEventDescription.setText("");
            return;
        }
        LocalDate dateStart=datePickerEventStart.getValue();
        LocalTime timeStart=startSpinner.getValue();
        LocalDateTime dateTimeStart = LocalDateTime.of(dateStart,timeStart);
        LocalDate dateEnd=datePickerEventEnd.getValue();
        LocalTime timeEnd=endSpinner.getValue();
        LocalDateTime dateTimeEnd=LocalDateTime.of(dateEnd,timeEnd);
        String eventTitle=textFieldEventName.getText();
        String eventDescription=textAreaEventDescription.getText();
        Event eventCreated=new Event(userPage.getUser(),dateTimeStart,dateTimeEnd,eventTitle,eventDescription);
        try {
            userPage.createEvent(eventCreated);
            MessageAlert.showMessage(stage, Alert.AlertType.INFORMATION,"Success","Event created with success!");
        }
        catch (ServiceException e){
            MessageAlert.showErrorMessage(stage,e.getMessage());
        }
        textFieldEventName.setText("");
        textAreaEventDescription.setText("");


    }

    public void handleFollowEvent(Event selectedEvent){
        if(selectedEvent==null){
            MessageAlert.showErrorMessage(stage,"Please select an event!\n");
            return;
        }
        userPage.followEvent(selectedEvent.getId());
        MessageAlert.showMessage(stage, Alert.AlertType.INFORMATION,"Success","You are following now the event called '"+selectedEvent.getName()+"!\n");
    }

    public void handleUnfollowEvent(Event selectedEvent){
        if(selectedEvent==null){
            MessageAlert.showErrorMessage(stage,"Please select an event!\n");
            return;
        }
        userPage.unfollowEvent(selectedEvent.getId());
        MessageAlert.showMessage(stage, Alert.AlertType.INFORMATION,"Success","You unfollowed the event called '"+selectedEvent.getName()+"!\n");
    }

    @FXML
    void handleChangeEvents(ActionEvent event){

        if(buttonChangeEvents.getText().equals("Switch to followed events")) {
            
            tabPaneTables.getSelectionModel().select(4);
            tabPaneTables.setVisible(true);
            initModelFollowedEvents();
            tableViewFollowedEvents.setItems(modelFollowedEvents);
            buttonChangeEvents.setText("Switch to unfollowed events");
            listViewEvent.setVisible(false);
            listViewEvent1.setVisible(false);

            anchorPaneDetailsEvent.setVisible(false);
            anchorPaneDetailsEvent1.setVisible(false);
        }
        else{

            tabPaneTables.getSelectionModel().select(5);
            tabPaneTables.setVisible(true);
            initModelUnfollowedEvents();
            tableViewUnfollowedEvents.setItems(modelUnfollowedEvents);
            buttonChangeEvents.setText("Switch to followed events");
            listViewEvent.setVisible(false);
            listViewEvent1.setVisible(false);

            anchorPaneDetailsEvent.setVisible(false);
            anchorPaneDetailsEvent1.setVisible(false);
        }
    }

    @FXML
    public void handleShowParticipants(){
        buttonCreateEv.setVisible(false);
        textFieldEventName.setVisible(false);
        textAreaEventDescription.setVisible(false);
        datePickerEventStart.setVisible(false);
        datePickerEventEnd.setVisible(false);
        startSpinner.setVisible(false);
        label_startDate.setVisible(false);
        label_endDate.setVisible(false);
        endSpinner.setVisible(false);
        label_eventTitle.setVisible(false);
        label_Description.setVisible(false);

        imageCalendar.setVisible(false);


        if(buttonChangeEvents.getText().equals("Switch to followed events")) {
            boolean hasParticipants=false;
            if (selectedEvent == null) {
                MessageAlert.showErrorMessage(stage, "Please select an event!\n");
                return;
            }
           anchorPaneDetailsEvent1.setVisible(true);
            listViewEvent1.setVisible(true);
            listViewEvent1.getItems().clear();
            for (Utilizator participant : userPage.getEventParticipants(selectedEvent.getId())) {
                hasParticipants=true;
                listViewEvent1.getItems().add(participant.toString());
            }
            if(!hasParticipants)
                listViewEvent1.getItems().add("There are no participants for this event at the moment");
        }
        else if(buttonChangeEvents.getText().equals("Switch to unfollowed events")){
            boolean hasParticipants=false;
            if (selectedEvent == null) {
                MessageAlert.showErrorMessage(stage, "Please select an event!\n");
                return;
            }
            anchorPaneDetailsEvent1.setVisible(true);
            listViewEvent1.setVisible(true);
            listViewEvent1.getItems().clear();
            for (Utilizator participant : userPage.getEventParticipants(selectedEvent.getId())) {
                hasParticipants=true;
                listViewEvent1.getItems().add(participant.toString());
            }
            if(!hasParticipants)
                listViewEvent1.getItems().add("There are no participants for this event at the moment");
        }
    }

    @FXML
    public void ShowAnchorPaneRequestInfo(Event event)
    {
        selectedEvent=event;
        anchorPaneRequestInfo.setVisible(false);

        anchorPaneRequestInfo.setVisible(true);

        anchorPaneDetailsEvent.setVisible(false);
        anchorPaneDetailsEvent1.setVisible(false);


    }
    @FXML
    public void handleHideDetails(MouseEvent event)
    {
        anchorPaneDetailsEvent.setVisible(false);
        anchorPaneDetailsEvent1.setVisible(false);


        anchorPaneRequestInfo.setVisible(false);
        buttonCreateEv.setVisible(false);
        textFieldEventName.setVisible(false);
        textAreaEventDescription.setVisible(false);
        datePickerEventStart.setVisible(false);
        datePickerEventEnd.setVisible(false);
        startSpinner.setVisible(false);
        label_startDate.setVisible(false);
        label_endDate.setVisible(false);
        endSpinner.setVisible(false);
        label_eventTitle.setVisible(false);
        label_Description.setVisible(false);

        imageCalendar.setVisible(false);


    }

    @FXML
    void handleShowFields()
    {
        textFieldEventName.setVisible(true);
        textAreaEventDescription.setVisible(true);
        buttonCreateEv.setVisible(true);
        buttonChangeEvents.setVisible(true);
        datePickerEventStart.setVisible(true);
        datePickerEventEnd.setVisible(true);
        startSpinner.setVisible(true);
        label_startDate.setVisible(true);
        label_endDate.setVisible(true);
        endSpinner.setVisible(true);
        label_eventTitle.setVisible(true);
        label_Description.setVisible(true);
        imageCalendar.setVisible(true);
        anchorPaneDetailsEvent.setVisible(false);
        anchorPaneDetailsEvent1.setVisible(false);

        anchorPaneRequestInfo.setVisible(false);

    }

    @FXML
    public void handleShowInfo(){
        if(buttonChangeEvents.getText().equals("Switch to followed events")) {
            if (selectedEvent == null) {
                System.out.println("nu s-a selectat");
                MessageAlert.showErrorMessage(stage, "Please select an event!\n");
                return;
            }
            System.out.println("s-a selectat");

            anchorPaneDetailsEvent.setVisible(true);
            listViewEvent.setVisible(true);
            listViewEvent.getItems().clear();
            listViewEvent.getItems().add("Owner: "+selectedEvent.getOwner()+"\n");
            listViewEvent.getItems().add("Name: "+selectedEvent.getName()+"\n");
            listViewEvent.getItems().add("Starting date: "+selectedEvent.getStartDate().format(Constants.DATE_TIME_FORMATTER_HM)+"\n");
            listViewEvent.getItems().add("Ending date: "+selectedEvent.getEndDate().format(Constants.DATE_TIME_FORMATTER_HM)+"\n");
            listViewEvent.getItems().add("Description: "+selectedEvent.getDescription());


        }
        else if(buttonChangeEvents.getText().equals("Switch to unfollowed events")){
            if (selectedEvent == null) {
                MessageAlert.showErrorMessage(stage, "Please select an event!\n");
                return;
            }
            anchorPaneDetailsEvent.setVisible(true);
            listViewEvent.setVisible(true);

            listViewEvent.getItems().clear();
            listViewEvent.getItems().add("Owner: "+selectedEvent.getOwner()+"\n");
            listViewEvent.getItems().add("Name: "+selectedEvent.getName()+"\n");
            listViewEvent.getItems().add("Starting date: "+selectedEvent.getStartDate().format(Constants.DATE_TIME_FORMATTER_HM)+"\n");
            listViewEvent.getItems().add("Ending date: "+selectedEvent.getEndDate().format(Constants.DATE_TIME_FORMATTER_HM)+"\n");
            listViewEvent.getItems().add("Description: "+selectedEvent.getDescription());

        }
    }

    @FXML
    public void sendNotification(){
        List<String> upcomingEvents=new ArrayList<>();
        upcomingEvents.add(   "UPCOMING EVENTS IN THE NEXT 7 DAYS!\n\n");

        listViewUpcoming.getItems().clear();



        boolean hasEvents=false;
        for(Event event:userPage.getFollowedEvents()) {
            if (LocalDateTime.now().plusDays(7).isAfter(event.getStartDate())) {
                upcomingEvents.add( event.toStringDAY_OF_WEEK() );
                hasEvents=true;
            }
        }
        if(hasEvents) {

            listViewUpcoming.getItems().clear();


            listViewUpcoming.getItems().addAll(upcomingEvents);

            img3.setVisible(true);



        }
        else{
            listViewUpcoming.getItems().clear();
            listViewUpcoming.getItems().add("You don't have any upcoming events yet!\nGo check the Events tab if you are interested in them.");
            img3.setVisible(false);
        }

        listViewUpcoming.scrollTo(modelEvents.size());
    }

    @FXML
    private void handleSwitchAddEvent(){
        anchorAddEvent.setVisible(true);
        listViewEvent.setVisible(false);
    }

    public void handleLearnMore()
    {
        buttonCreateGroupChat.setVisible(false);
        imagClopotel.setVisible(false);
        buttonGoTo.setVisible(false);
        initModelUnfollowedEvents();
        tableViewUnfollowedEvents.setItems(modelUnfollowedEvents);
        anchorPaneDetailsEvent.setVisible(false);
        anchorPaneDetailsEvent1.setVisible(false);

        anchorPaneRequestInfo.setVisible(false);
        buttonChangeEvents.setVisible(true);
        buttonCreateEv.setVisible(false);

        textFieldEventName.setVisible(false);
        textAreaEventDescription.setVisible(false);
        datePickerEventStart.setVisible(false);
        datePickerEventEnd.setVisible(false);
        startSpinner.setVisible(false);
        label_startDate.setVisible(false);
        label_endDate.setVisible(false);
        endSpinner.setVisible(false);
        label_eventTitle.setVisible(false);
        label_Description.setVisible(false);

        imageCalendar.setVisible(false);
        tableViewUnfollowedEvents.setItems(modelUnfollowedEvents);
        tabPane.getSelectionModel().select(6);
        tabPaneTables.getSelectionModel().select(5);
        tabPaneTables.setVisible(true);
        AnchorPane.setBottomAnchor(tabPaneTables,155.0);
        AnchorPane.setTopAnchor(tabPaneTables,0.0);



        tabPaneTables.setVisible(true);
        buttonChangeEvents.setText("Switch to unfollowed events");
        listViewEvent.setVisible(false);
        anchorInfo.setVisible(false);
        listViewEvent.setVisible(false);
        anchorAddEvent.setVisible(true);
        anchorPaneRequestInfo.setVisible(false);

    }

    public void setStage(Stage newStage){
        this.stage=newStage;
    }




    @Override
    public void update(EventObserver event) {

            if(userPage.getFriendsRequest().size()>modelReceivedRequests.size())
                imag2.setVisible(true);

    }


}
