package socialnetwork.controller;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.awt.*;


public class EventsXCell extends ListCell<String> {
        HBox hbox = new HBox();
        Label label = new Label();
        Pane pane = new Pane();
        Button button = new Button("Details");
        private ImageView imageView = new ImageView();

        private final Image IMAGE_MON = new Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/MON.jpg");
        private final Image IMAGE_TUE  = new Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/TUE.jpg");
        private final Image IMAGE_WED  = new Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/WED.jpg");
        private final Image IMAGE_THU = new Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/THU.jpg");
        private final Image IMAGE_FRI  = new Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/FRI.jpg");
        private final Image IMAGE_SAT  = new Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/SAT.jpg");
        private final Image IMAGE_SUN  = new Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/SUN.jpg");

        private final Image IMAGE= new Image("file:///C:/Users/home/Desktop/t/socialnetwork/src/main/resources/images/cale.jpg");


        String lastItem;

        public EventsXCell() {

            super();

            imageView.setImage(IMAGE);
            label.setStyle("-fx-text-fill: #00ff00" );
            hbox.getChildren().addAll(label, pane, imageView);
            HBox.setHgrow(pane, Priority.ALWAYS);
            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                   // System.out.println(lastItem + " : " + event);
                }
            });
            imageView.setImage(IMAGE);
        }

        @Override
        protected void updateItem(String item, boolean empty) {
            if (item != lastItem ) {
                super.updateItem(item, empty);
                setText(null);  // No text in label of super class
                if (empty) {
                    lastItem = null;
                    imageView.setImage(IMAGE);
                    setGraphic(null);
                   // imageView=new ImageView();
                } else {
                    lastItem = item;
                    label.setStyle("-fx-text-fill: #efe9e9");
                    label.setText(item != null ? item : "<null>");
                    if (lastItem != null && lastItem != "" && !item.contains("UPCOMING EVENTS IN THE NEXT 7 DAYS!") && !item.contains("You don't have any upcoming events yet!\\nGo check the Events tab if you are interested in them.")) {

                        if (lastItem.contains("MONDAY"))
                            imageView.setImage(IMAGE_MON);
                        if (lastItem.contains("TUESDAY"))
                            imageView.setImage(IMAGE_TUE);
                        if (lastItem.contains("WEDNESDAY"))
                            imageView.setImage(IMAGE_WED);
                        if (lastItem.contains("THURSDAY")) {

                            imageView.setImage(IMAGE_THU);
                            imageView.setVisible(true);

                        }
                        if (lastItem.contains("FRIDAY"))
                            imageView.setImage(IMAGE_FRI);
                        if (lastItem.contains("SATURDAY"))
                            imageView.setImage(IMAGE_SAT);
                        if (lastItem.contains("SUNDAY"))
                            imageView.setImage(IMAGE_SUN);


                    }

                    setGraphic(hbox);
                }


            }



        }






}