package socialnetwork.controller;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import static com.sun.javafx.scene.control.skin.Utils.getResource;


public class MessageAlert {

    /**
     * Folosit pentru mesaje informative de tipul: cand se trimite cu succes o cerere de prietenie
     * @param owner
     * @param type
     * @param header
     * @param text
     */
    static void showMessage(Stage owner, Alert.AlertType type, String header, String text){
        Alert message=new Alert(type);
        message.setHeaderText(header);
        message.setContentText(text);
        message.initOwner(owner);

        message.showAndWait();

    }

    /**
     * Folosit pentru erori de tipul: nu se poate trimite aceasta cerere, utilizator invalid la logare
     * @param owner
     * @param text
     */
    static void showErrorMessage(Stage owner, String text){
        Alert message=new Alert(Alert.AlertType.ERROR);
        message.initOwner(owner);
        message.setTitle("Error");
        message.setContentText(text);
        message.showAndWait();
    }
}

